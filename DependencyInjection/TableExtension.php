<?php

namespace TableBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class TableExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->buildParameters($container, $config);
    }

    /**
     * @param ContainerBuilder $container
     * @param array $config
     *
     * @return $this
     */
    protected function buildParameters(ContainerBuilder $container, array $config)
    {
        $container->setParameter('table_bundle.paginator.default_limit', $config['paginator']['default_limit']);
        $container->setParameter('table_bundle.paginator.default_possible_limits', $config['paginator']['default_possible_limits']);

        return $this;
    }
}
