<?php

namespace TableBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('table');

        $rootNode
            ->children()
                ->arrayNode('paginator')
                    ->children()
                        ->integerNode('default_limit')
                            ->info('Default limit for paginator')
                            ->defaultValue(20)
                            ->isRequired()
                        ->end()
                        ->arrayNode('default_possible_limits')
                            ->info('Default possible limits for paginator')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->requiresAtLeastOneElement()
                            ->integerPrototype()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
