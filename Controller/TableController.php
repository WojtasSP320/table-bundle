<?php

namespace TableBundle\Controller;

use Danhoss\DanhossLoggerBundle\Services\Logger;
use MainBundle\Controller\ControllerAbstract;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use TableBundle\Entity\TableSetting;
use TableBundle\Service\TableInterface;
use TableBundle\Service\TableSettingsManager;

/**
 * TableController class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableController extends ControllerAbstract
{
    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/save/table/settings/async",
     *     name="table_save_table_settings_async",
     *     options={"expose": true},
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveTableSettingsActions(Request $request): JsonResponse
    {
        $session = $this->get('session');
        $tableSettingsManager = $this->get(TableSettingsManager::class);

        // ~

        $tableName = $request->get('table_name');
        $tableSettings = $session->get(TableInterface::TABLE_SETTINGS_SESSION_KEY, []);

        $settings = TableSetting::createFromRequest($request)->toArray();

        $settings['columns'] = $tableSettings[$tableName]['columns'] ?? [];

        $tableSettings[$tableName] = $settings;

        // ~

        try {
            $tableSettingsManager->saveTableSettingsFromArray($tableName, $tableSettings[$tableName]);
            $session->set(TableInterface::TABLE_SETTINGS_SESSION_KEY, $tableSettings);
        } catch (\Throwable $exception) {
            $this->get(Logger::class)->exceptionLog($exception);

            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse([]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/columns/layout/save/async",
     *     name="table_save_columns_layout_async",
     *     options={"expose"=true},
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function saveColumnsLayoutAsyncAction(Request $request): JsonResponse
    {
        $session = $this->get('session');
        $tableSettingsManager = $this->get(TableSettingsManager::class);

        $required = [
            'columns',
            'tableName',
        ];

        foreach ($required as $item) {
            if (false === $request->request->has($item)) {
                throw new BadRequestHttpException("Missing '$item' request key!");
            }
        }

        // ~

        $tableName = $request->get('tableName');
        $columns = $request->get('columns');

        $tableSettings = $session->get(TableInterface::TABLE_SETTINGS_SESSION_KEY, []);

        $settings = $tableSettings[$tableName] ?? (new TableSetting())->toArray();
        $settings['columns'] = $columns;
        $tableSettings[$tableName] = $settings;

        // ~

        try {
            $tableSettingsManager->saveTableSettingsFromArray($tableName, $settings);
            $session->set(TableInterface::TABLE_SETTINGS_SESSION_KEY, $tableSettings);
        } catch (\Throwable $exception) {
            $this->get(Logger::class)->exceptionLog($exception);

            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse([]);
    }
}
