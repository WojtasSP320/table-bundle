<?php

namespace TableBundle\Entity;

use DanlineBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\Request;

/**
 * TableSetting
 *
 * @ORM\Table(name="table_settings")
 * @ORM\Entity(repositoryClass="TableBundle\Repository\TableSettingRepository")
 */
class TableSetting
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="DanlineBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var int
     * @ORM\Column(name="page", type="integer", nullable=false, options={"default": 1})
     */
    private $page = 1;

    /**
     * @var int
     * @ORM\Column(name="`limit`", type="integer", nullable=false, options={"default": 10})
     */
    private $limit = 10;

    /**
     * @var array
     *
     * @ORM\Column(name="sorting", type="json", nullable=false)
     */
    private $sorting = [];

    /**
     * @var array
     *
     * @ORM\Column(name="criteria", type="json", nullable=false)
     */
    private $criteria = [];

    /**
     * @var array
     *
     * @ORM\Column(name="columns", type="json", nullable=false)
     */
    private $columns = [];

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $modifiedAt;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public static function createFromRequest(Request $request): self
    {
        $setting = new self();

        $pagination = $request->request->get('pagination', []);

        $setting->page = $pagination['page'] ?? null;
        $setting->limit = $pagination['limit'] ?? null;

        $setting->criteria = $request->request->get('filters', []);
        $setting->sorting = $request->request->get('sorting', []);

        $setting->columns = $request->request->get('columns', []);

        return $setting;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'page' => $this->page,
            'limit' => $this->limit,
            'criteria' => $this->criteria,
            'sorting' => $this->sorting,
            'columns' => $this->columns,
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return array
     */
    public function getSorting(): array
    {
        return $this->sorting;
    }

    /**
     * @param array $sorting
     *
     * @return $this
     */
    public function setSorting(array $sorting): self
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     *
     * @return $this
     */
    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     *
     * @return $this
     */
    public function setColumns(array $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getModifiedAt(): ?\DateTime
    {
        return $this->modifiedAt;
    }
}
