<?php

namespace TableBundle\Repository;

use DanlineBundle\Entity\User;
use DanlineBundle\Repository\EntityRepositoryAbstract;
use Doctrine\ORM\NonUniqueResultException;
use TableBundle\Entity\TableSetting;

/**
 * Class TableSettingRepository
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableSettingRepository extends EntityRepositoryAbstract
{
    /**
     * @return string
     */
    protected function getClass(): string
    {
        return TableSetting::class;
    }

    /**
     * @param User $user
     * @param string $tableName
     *
     * @throws NonUniqueResultException
     *
     * @return mixed
     */
    public function findByUserAndTableName(User $user, string $tableName)
    {
        $queryBuilder = $this->createQueryBuilder('ts');

        $queryBuilder
            ->where('ts.user = :user')
            ->andWhere('ts.name = :tableName')
            ->setParameter('user', $user)
            ->setParameter('tableName', $tableName)
        ;

        $queryBuilder->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
