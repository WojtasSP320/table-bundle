<?php

namespace TableBundle\Exception;

/**
 * Class TableBundleException
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableBundleException extends \Exception
{
}
