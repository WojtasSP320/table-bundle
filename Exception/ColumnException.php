<?php

namespace TableBundle\Exception;

/**
 * ColumnException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ColumnException extends TableException
{
}
