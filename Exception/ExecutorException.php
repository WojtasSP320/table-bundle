<?php

namespace TableBundle\Exception;

/**
 * ExecutorException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ExecutorException extends TableBundleException
{
}
