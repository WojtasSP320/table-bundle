<?php

namespace TableBundle\Exception;

/**
 * ExecutorMethodNotImplementedException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ExecutorMethodNotImplementedException extends ExecutorException
{
}
