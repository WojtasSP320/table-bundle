<?php

namespace TableBundle\Exception;

/**
 * ExecutorCallOnNullException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ExecutorCallOnNullException extends ExecutorException
{
}
