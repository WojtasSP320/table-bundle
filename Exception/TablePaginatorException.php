<?php

namespace TableBundle\Exception;

/**
 * TablePaginatorException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TablePaginatorException extends TableBundleException
{
}
