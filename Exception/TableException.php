<?php

namespace TableBundle\Exception;

/**
 * TableException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableException extends TableBundleException
{
}
