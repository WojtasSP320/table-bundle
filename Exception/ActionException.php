<?php

namespace TableBundle\Exception;

/**
 * ActionException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ActionException extends TableException
{
}
