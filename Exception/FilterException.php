<?php

namespace TableBundle\Exception;

/**
 * FilterException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class FilterException extends ColumnException
{
}
