<?php

namespace TableBundle\Exception;

/**
 * ExecutorArrayAccessOnNonArrayException class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ExecutorArrayAccessOnNonArrayException extends ExecutorException
{
}
