<?php

namespace TableBundle\Service;

use Closure;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\ActionException;
use TableBundle\Exception\ColumnException;
use TableBundle\Exception\ExecutorArrayAccessOnNonArrayException;
use TableBundle\Exception\ExecutorMethodNotImplementedException;
use TableBundle\Exception\FilterException;
use TableBundle\Exception\TableException;
use TableBundle\Service\Cell\SimpleField;
use TableBundle\Service\Filter\NoneFilter;
use TableBundle\Service\Model\TableData;

/**
 * TableAbstract class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
abstract class TableAbstract implements TableInterface
{
    use TableTrait, TableDispatcherTrait;

    /** @var ArrayCollection|Column[] */
    private $columns;

    /** @var Action[]|ArrayCollection */
    private $actions;

    /** @var array */
    private $options = [];

    /** @var ArrayCollection|FilterInterface[] */
    private $filters;

    /** @var TablePaginator|null */
    private $tablePaginator;

    /** @var TableSorter|null */
    private $tableSorter;

    /** @var TableChecker|null */
    private $tableChecker;

    /** @var TableData|null */
    private $postData;

    /** @var array */
    private $criteriaMap = [];

    /** @var array */
    private $orderMap = [];

    /** @var Closure|null */
    private $eachRowCallback;

    /** @var Closure|null */
    private $rowIdCallback;

    /** @var bool */
    private $autoInit = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this
            ->setOptionsResolver(new OptionsResolver())
            ->setDefaultOptions($this->getOptionsResolver())
        ;

        $this->columns = new ArrayCollection();
        $this->actions = new ArrayCollection();
        $this->filters = new ArrayCollection();
    }

    /**
     * @return Closure|null
     */
    public function getEachRowCallback(): ?Closure
    {
        return $this->eachRowCallback;
    }

    /**
     * @param Closure|null $eachRowCallback
     *
     * @return $this
     */
    public function setEachRowCallback(Closure $eachRowCallback = null): self
    {
        $this->eachRowCallback = $eachRowCallback;

        return $this;
    }

    /**
     * @return Closure|null
     */
    public function getRowIdCallback(): ?Closure
    {
        return $this->rowIdCallback;
    }

    /**
     * @param Closure|null $rowIdCallback
     *
     * @return $this
     */
    public function setRowIdCallback(Closure $rowIdCallback = null): self
    {
        $this->rowIdCallback = $rowIdCallback;

        return $this;
    }

    /**
     * Proxy method for table checker
     *
     * @param array $checkboxes
     *
     * @return bool
     */
    public function setCheckboxes(array $checkboxes): bool
    {
        if ($this->isCheckingEnabled()) {
            $this->getTableChecker()->setCheckboxes($checkboxes);

            return true;
        }

        return false;
    }

    /**
     * @param Closure $handler
     *
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return bool
     */
    public function setCheckboxHandler(Closure $handler): bool
    {
        if ($this->isCheckingEnabled()) {
            $this->getTableChecker()->setCheckboxHandler($handler);

            return true;
        }

        return false;
    }

    /**
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return bool
     */
    public function setDefaultCheckboxHandler(): bool
    {
        if ($this->isCheckingEnabled()) {
            $this->getTableChecker()->setDefaultCheckboxHandler();

            return true;
        }

        return false;
    }

    /**
     * @param TableData $postData
     *
     * @return $this
     */
    public function setPostData(TableData $postData): self
    {
        $this->postData = $postData;

        return $this;
    }

    /**
     * @return TableData
     */
    public function getPostData(): TableData
    {
        return $this->postData;
    }

    /**
     * @return TableSorter|null
     */
    public function getTableSorter(): ?TableSorter
    {
        return $this->tableSorter;
    }

    /**
     * @param TableSorter $tableSorter
     *
     * @return $this
     */
    public function setTableSorter(TableSorter $tableSorter): self
    {
        $this->tableSorter = $tableSorter;

        return $this;
    }

    /**
     * @return TablePaginator|null
     */
    public function getTablePaginator(): ?TablePaginator
    {
        return $this->tablePaginator;
    }

    /**
     * @param TablePaginator $tablePaginator
     *
     * @return $this
     */
    public function setTablePaginator(TablePaginator $tablePaginator): self
    {
        $this->tablePaginator = $tablePaginator;

        return $this;
    }

    /**
     * @return TableChecker|null
     */
    public function getTableChecker(): ?TableChecker
    {
        return $this->tableChecker;
    }

    /**
     * @param TableChecker $tableChecker
     *
     * @return $this
     */
    public function setTableChecker(TableChecker $tableChecker): self
    {
        $this->tableChecker = $tableChecker;

        return $this;
    }

    /**
     * @return ArrayCollection|FilterInterface[]
     */
    public function getFilters(): ArrayCollection
    {
        return $this->filters;
    }

    /**
     * @param string $key
     *
     * @return FilterInterface|null
     */
    public function getFilter(string $key): ?FilterInterface
    {
        return $this->filters->get($key);
    }

    /**
     * @param FilterInterface $filter
     * @param string|null $oldName
     *
     * @throws TableException
     *
     * @return $this
     */
    public function setFilter(FilterInterface $filter, string $oldName = null): self
    {
        if ($this->isLocked()) {
            throw new TableException('Cannot modify table filters on locked table!');
        }

        $this->filters->set($filter->getName(), $filter);
        $filter->setTable($this);

        return $this;
    }

    /**
     * @param string $filterName
     *
     * @throws TableException
     *
     * @return TableAbstract
     */
    public function removeFilter(string $filterName): self
    {
        if (!$this->isLocked()) {
            throw new TableException('Cannot modify table filters on locked table!');
        }

        if ('' !== $filterName) {
            $this->filters->remove($filterName);
        }

        return $this;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options): self
    {
        $tableName = $this->getName();

        try {
            $options = $this->optionsResolver->resolve($options);
            $this->options = $options;
        } catch (InvalidOptionsException $exception) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            $messageSuffix = \sprintf("\n In \"%s\" table options.", $tableName);

            throw new InvalidOptionsException($message . $messageSuffix, $code, $exception);
        } catch (MissingOptionsException $exception) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            $messageSuffix = \sprintf("\n In \"%s\" table options.", $tableName);

            throw new MissingOptionsException($message . $messageSuffix, $code, $exception);
        }

        // ~

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string $optionName
     *
     * @throws TableException
     *
     * @return mixed
     */
    public function getOption(string $optionName)
    {
        $options = $this->getOptions();

        if (\array_key_exists($optionName, $options)) {
            return $options[$optionName];
        }

        throw new TableException('Unknown option "%s"!', [$optionName]);
    }

    /**
     * @return string
     */
    abstract public function getName(): string;

    /**
     * @param ArrayCollection $actions
     *
     * @return $this
     */
    protected function setActions(ArrayCollection $actions): self
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * @param ArrayCollection $columns
     *
     * @return $this
     */
    protected function setColumns(ArrayCollection $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @param ArrayCollection $filters
     *
     * @return $this
     */
    protected function setFilters(ArrayCollection $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @param array $additionalCriteria
     * @param bool $override
     *
     * @throws TableException
     *
     * @return array
     */
    public function getListCriteria(array $additionalCriteria = [], bool $override = false): array
    {
        $criteriaParser = new CriteriaParser();

        if (!empty($additionalCriteria)) {
            $this->validateAdditionalCriteriaMap($additionalCriteria);
        }

        $filtersData = $this->getFiltersData();

        if ($override) {
            $criteriaData = \array_merge($filtersData, $additionalCriteria);
        } else {
            $criteriaData = \array_merge($additionalCriteria, $filtersData);
        }

        $criteriaMap = $this->getCriteriaMap();

        return $criteriaParser->parse($criteriaMap, $criteriaData);
    }

    /**
     * @return array
     */
    public function getFiltersData(): array
    {
        $filtersData = [];

        /** @var FilterAbstract $filter */
        foreach ($this->getFilters() as $filter) {
            $filterValue = $filter->getModelValue();

            $filtersData[$filter->getName()] = $filterValue;
        }

        /* Remove empty values */
        $filtersData = \array_filter($filtersData, static function ($elem) {
            $notEmptyArray = (true === \is_array($elem) && false === empty($elem));
            $notEmptyString = (false === \is_array($elem) && '' !== $elem);

            return ($notEmptyArray || $notEmptyString);
        });

        return $filtersData;
    }

    // ~

    /**
     * @param string $name
     *
     * @return Column|null
     */
    public function getColumn(string $name): ?Column
    {
        return $this->columns->get($name);
    }

    /**
     * @return ArrayCollection
     */
    public function getColumns(): ArrayCollection
    {
        return $this->columns;
    }

    /**
     * @param string $name
     * @param Column|null $column
     *
     * @throws TableException
     *
     * @return Column
     */
    public function addColumn(string $name, Column $column = null): Column
    {
        if ($this->isLocked()) {
            throw new TableException('Cannot modify table columns on locked table!');
        }

        if (null === $column && null === $this->getColumn($name)) {
            $column = new Column($this);

            $column->setName($name);
            $this->columns->set($name, $column);
        }

        return $this->columns->get($name);
    }

    /**
     * @param string $class
     * @param string $name
     * @param array $attributes
     *
     * @throws ColumnException
     * @throws FilterException
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function createFilter(string $class, string $name, array $attributes = []): self
    {
        if (null !== ($filterFactory = $this->getFilterFactory())) {
            $filter = $filterFactory->createFilter($class, $name, $attributes);

            $this->setFilter($filter);
        }

        return $this;
    }

    /**
     * "Syntactic sugar" shorthand for addColumn + set hidden
     *
     * @param string $name
     * @param Column $column
     * @param bool $disableSorting
     * @param bool $createSimpleCell
     * @param bool $createNoneFilter
     *
     * @throws TableException
     *
     * @return Column
     */
    public function addHiddenColumn(
        string $name,
        Column $column = null,
        bool $disableSorting = true,
        bool $createSimpleCell = true,
        bool $createNoneFilter = true
    ): Column {
        $column = $this
            ->addColumn($name, $column)
            ->setHidden(true)
        ;

        if ($disableSorting) {
            $column->disableSorting();
        }

        if ($createNoneFilter) {
            $column->createFilter(NoneFilter::class, $name);
        }

        if ($createSimpleCell) {
            $column->createCell(SimpleField::class);
        }

        return $column;
    }

    /**
     * @param Column $column
     *
     * @throws TableException
     *
     * @return $this
     */
    public function removeColumn(Column $column): self
    {
        if ($this->isLocked()) {
            throw new TableException('Cannot modify table columns on locked table!');
        }

        $this->columns->removeElement($column);
        $this->filters->removeElement($column->getFilter());

        if ($this->isSortingEnabled()) {
            $this->getTableSorter()->removeSortedColumn($column);
        }

        return $this;
    }

    /**
     * @param string $name
     *
     * @throws \Exception
     *
     * @return $this
     */
    public function removeColumnByName(string $name): self
    {
        $column = $this->getColumn($name);

        if (null !== $column) {
            $this->removeColumn($column);
        }

        return $this;
    }

    // ~

    /**
     * @param string $name
     * @param Action|null $action
     *
     * @throws ActionException
     *
     * @return Action
     */
    public function addAction(string $name, Action $action = null): Action
    {
        if (null === $action) {
            $action = new Action($name);
        }

        $action->setName($name);

        //todo if table already has that action

        $this->actions->set($name, $action);

        return $this->actions->get($name);
    }

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function removeAction(Action $action): self
    {
        $this->actions->removeElement($action);

        return $this;
    }

    /**
     * @return $this
     */
    public function removeActions(): self
    {
        $this->actions = new ArrayCollection();

        return $this;
    }

    /**
     * @param string $name
     *
     * @return Action|null
     */
    public function getAction(string $name): ?Action
    {
        return $this->actions->get($name);
    }

    // ~

    /**
     * @throws FilterException
     *
     * @return $this
     */
    private function validateFiltersCriteriaMap(): self
    {
        /** @var FilterAbstract $filter */
        foreach ($this->getFilters() as $filter) {
            if (!$filter->isValidWithCriteriaMap()) {
                throw new FilterException('Criteria map is invalid for filter "%s" %s', [
                    $filter->getName(),
                    $filter->getType(),
                ]);
            }
        }

        return $this;
    }

    /**
     * @param array $additionalCriteria
     *
     * @throws TableException
     *
     * @return $this
     */
    private function validateAdditionalCriteriaMap(array $additionalCriteria): self
    {
        $criteriaMap = $this->getCriteriaMap();

        foreach ($additionalCriteria as $key => $value) {
            if (false === \array_key_exists($key, $criteriaMap)) {
                throw new TableException('Criteria map is invalid for additional criteria "%s" with value "%s"', [
                    $key,
                    $value,
                ]);
            }
        }

        return $this;
    }

    /**
     * @param bool $autoInit
     *
     * @return $this
     */
    public function setAutoInit(bool $autoInit): self
    {
        $this->autoInit = $autoInit;

        return $this;
    }

    /**
     * @throws ColumnException
     * @throws FilterException
     * @throws TableException
     * @throws \Exception
     *
     * @return $this
     */
    public function handlePostData(): self
    {
        $postData = $this->getPostData();

        // ~

        $this->validateFiltersCriteriaMap();

        if ($this->isSortingEnabled()) {
            $orderMap = $this->getOrderMap();
            $this->getTableSorter()->validateSortingOrderMap($orderMap);
        }

        // ~

        $this
            ->handleCriteria($postData)
            ->handleSorting($postData)
            ->handlePagination($postData);

        // ~

        $this->lockTable();

        return $this;
    }

    /**
     * @param TableData $postData
     *
     * @throws FilterException
     *
     * @return $this
     */
    protected function handleCriteria(TableData $postData): self
    {
        $criteria = $postData->getFilters();
        $this->dispatchPreSetCriteriaEvent($criteria);

        // ~

        foreach ($this->filters as $filter) {
            $filter->handleCriteria($criteria);
        }

        // ~

        $this->dispatchPostSetCriteriaEvent($criteria);

        return $this;
    }

    // ~

    /**
     * @param string $eventName
     * @param callable $listener
     * @param int $priority
     *
     * @return $this
     */
    public function addEventListener(string $eventName, callable $listener, int $priority = 0): self
    {
        if (null !== $this->eventDispatcher) {
            $this->eventDispatcher->addListener($eventName, $listener, $priority);
        }

        return $this;
    }

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @throws ExecutorArrayAccessOnNonArrayException
     * @throws ExecutorMethodNotImplementedException
     * @throws TableException
     * @throws \Exception
     *
     * @return array
     */
    public function generate(iterable $list, int $resultsCount = null): array
    {
        $this->dispatchPreSetDataEvent($list);

        list($list, $resultsCount) = $this->normalizeData($list, $resultsCount);

        // ~

        $tableOptions = $this->getOptions();

        $tableData = [
            'table' => [
                'name' => $this->getName(),
                'showHeader' => $this->showHeader,
                'showLabels' => $this->showLabels,
                'showFilters' => $this->showFilters,
                'showSorting' => $this->showSorting,
                'showCheckboxes' => $this->showCheckboxes,
                'showActions' => $this->showActions,
                'showPagination' => $this->showPagination,
                // ~
                'paginationEnabled' => $this->isPaginationEnabled(),
                'sortingEnabled' => $this->isSortingEnabled(),
                // ~
                'dataSourceRoute' => $this->getDataSourceRoute(),
                'dataSourceRouteParameters' => $this->getDataSourceRouteParameters(),
                'checkboxesColumnLabel' => $this->getCheckboxesColumnLabel(),
                'autoInit' => $this->autoInit,
            ],
            'rows' => [],
            'columns' => [],
            'actions' => [],
            'checkboxes' => [],
            'pagination' => $this->getPaginationData(\count($list), $resultsCount),
            'sorting' => $this->getSortingData(),
            'filters' => $this->initializeFilters()->getFiltersArray(),
        ];

        /* Generate column metadata */

        $this->reorderColumnsPositions();
        $this->setColumnsVisibility();

        /** @var Column $column */
        foreach ($this->columns as $name => $column) {
            $tableData['columns'][$name] = $column->generate();
        }

        $eachRowCallback = $this->getEachRowCallback();

        /**
         * @var  int|string $index
         * @var  array|mixed $item
         */
        foreach ($list as $index => $item) {
            $rowData = [];

            $rowIdCallback = $this->getRowIdCallback();

            if (null !== $rowIdCallback) {
                /* Callback given - use callback */
                $rowData['__rowId'] = $rowIdCallback($item);
            } else {
                /* List is an array of entities - use getId() method */
                if (\is_callable([$item, 'getId'])) {
                    $rowData['__rowId'] = $item->getId();
                } elseif (\is_array($item) && \array_key_exists(0, $item) && \is_callable([$item[0], 'getId'])) {
                    /* List is an array with entity under 0 key (e.g. query with additional selects) */
                    $rowData['__rowId'] = $item[0]->getId();
                } elseif (\is_array($item) && \array_key_exists('id', $item)) {
                    /* List is an array with key id */
                    $rowData['__rowId'] = $item['id'];
                } else {
                    $rowData['__rowId'] = null;
                }
            }

            if (null !== $eachRowCallback) {
                $metadata = $eachRowCallback($item, $index);
                $rowData['__rowMetadata'] = \is_array($metadata) ? $metadata : [$metadata];
            }

            /** @var Column $column */
            foreach ($this->columns as $name => $column) {
                $cellData = $column->getData($item, $name);
                $cellData['metadata'] = $rowData['__rowMetadata'] ?? [];
                $cellData['metadata']['rowId'] = $rowData['__rowId'] ?? null;
                $rowData[$name] = $cellData;
            }

            if ($this->isCheckingEnabled()) {
                $this->generateCheckboxes($index, $item, $tableOptions, $tableData['checkboxes']);
            }

            $tableData['rows'][] = $rowData;

            if ($this->showActions !== TableInterface::TABLE_SHOW_ACTIONS_NONE) {
                $actionData = [];
                /** @var Action $action */
                foreach ($this->actions as $name => $action) {
                    $actionData[$name] = $action->generate($item, $index);
                }
                $tableData['actions'][] = $actionData;
            }
        }

        // ~

        $this->dispatchPostSetDataEvent($list);
        $this->dispatchPostGenerateEvent($tableData);

        return $tableData;
    }

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @return array
     */
    protected function normalizeData(iterable $list, int $resultsCount = null): array
    {
        switch (true) {
            case ($list instanceof Paginator):
                $resultsCount = (int)$list->count();
                $list = $list->getQuery()->getResult();
                break;
            case ($list instanceof Collection):
                $list = $list->toArray();
                $resultsCount = $resultsCount ?? \count($list);
                break;
            default:
                $resultsCount = $resultsCount ?? \count($list);
        }

        return [$list, $resultsCount];
    }

    /**
     * @param int|string $rowIndex
     * @param mixed $item
     * @param array $tableOptions
     * @param array $checkboxes
     *
     * @throws TableException
     *
     * @return $this
     */
    protected function generateCheckboxes($rowIndex, $item, array $tableOptions, array &$checkboxes): self
    {
        $checkbox = [
            'checked' => $this->getTableChecker()->handleCheckboxes($item, $tableOptions),
            'active' => $this->getTableChecker()->activateCheckbox($item, $tableOptions),
            'htmlAttributes' => $this->getTableChecker()->decorateCheckbox($item, $tableOptions),
        ];

        $checkboxes[$rowIndex] = $checkbox;

        return $this;
    }

    /**
     * @return $this
     */
    private function setColumnsVisibility(): self
    {
        $columnsVisibility = $this->getColumnsOrder();

        foreach ($columnsVisibility as $name => $visibility) {
            $column = $this->getColumn($name);

            if (null !== $column) {
                $column->setVisible($visibility);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function reorderColumnsPositions(): self
    {
        $columnsOrder = $this->getColumnsOrder();

        if (!empty($columnsOrder)) {
            $columnsKeys = \array_flip(\array_keys($columnsOrder));

            $columns = $this->columns->toArray();

            foreach ($columnsKeys as $key => $value) {
                if (false === isset($columns[$key])) {
                    continue;
                }

                $columnsKeys[$key] = $columns[$key];
            }

            /** @noinspection AdditionOperationOnArraysInspection */
            $columnsKeys += array_diff_key($columns, $columnsKeys);

            $this->columns = new ArrayCollection($columnsKeys);
        }

        return $this;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    protected function setDefaultOptions(OptionsResolver $optionsResolver): self
    {
        /* Default options for all tables */

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaginationEnabled(): bool
    {
        if (null !== $this->getTablePaginator()) {
            return $this->getTablePaginator()->isPaginationEnabled();
        }

        return false;
    }

    /**
     * Returns true if sorting is enabled or forced
     *
     * @return bool
     */
    public function isSortingUsed(): bool
    {
        if (null !== $this->getTableSorter()) {
            $enabled = $this->getTableSorter()->isSortingEnabled();
            $forced = $this->getTableSorter()->isSortingForced();

            return ($enabled || $forced);
        }

        return false;
    }

    /**
     * Returns true if sorting is enabled
     *
     * @return bool
     */
    public function isSortingEnabled(): bool
    {
        if (null !== $this->getTableSorter()) {
            return $this->getTableSorter()->isSortingEnabled();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isCheckingEnabled(): bool
    {
        if (null !== $this->getTableChecker()) {
            return $this->getTableChecker()->isCheckerEnabled();
        }

        return false;
    }

    // ~

    /**
     * Gets pagination data for view
     * Paginator proxy method
     *
     * @param int $currentItemsCount
     * @param int|null $resultsCount
     *
     * @return array
     */
    public function getPaginationData(int $currentItemsCount, int $resultsCount = null): array
    {
        /* Defaults when there is no paginator */
        $paginationData = [
            'limit' => TablePaginator::LIMIT_ALL,
            'possibleLimits' => [],
            'maxTiles' => 5,
            'currentPage' => 1,
            'maxPage' => 1,
            'currentItemsCount' => $currentItemsCount,
            'totalItemsCount' => $resultsCount,
        ];

        if ($this->isPaginationEnabled()) {
            $paginationData = $this->getTablePaginator()->getPaginationData($currentItemsCount, $resultsCount);
        }

        return $paginationData;
    }

    /**
     * @return array
     */
    public function getSortingData(): array
    {
        /* Defaults when there is no table sorter */
        $sortingData = [];

        if ($this->isSortingUsed()) {
            $sortingData = $this->getTableSorter()->getSorting();
        }

        return $sortingData;
    }

    /**
     * @return array
     */
    public function getFiltersArray(): array
    {
        $filters = $this->filters->toArray();

        $callback = function (&$filter) {
            /** @var FilterInterface $filter */
            $filter = $filter->toArray();
        };

        \array_walk($filters, $callback);

        return $filters;
    }

    /**
     * Forces table to be sorted with given order
     * even if table sorter is disabled (but present).
     * If sorting is not disabled it disables it automatically.
     *
     * Returns true if table has table sorter and sorting has been forced
     * returns false otherwise.
     *
     * @param array $sorting
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function forceUseSorting(array $sorting): bool
    {
        if (0 == \count($this->getColumns())) {
            throw new TableException('Hmm..., there is no columns defined in table. Did you use forceUseSorting before adding columns perhaps?');
        }

        if (null !== $this->getTableSorter()) {
            $this->getTableSorter()->forceUseSorting($sorting);

            return true;
        }

        return false;
    }

    /**
     * @return $this
     */
    public function initializeFilters(): self
    {
        foreach ($this->getFilters() as $filter) {
            $filter->initializeFilter();
        }

        return $this;
    }

    /**
     * @param TableData $postData
     *
     * @return $this
     */
    protected function handlePagination(TableData $postData): self
    {
        if ($this->isPaginationEnabled()) {
            $this->dispatchPreSetPaginationEvent(
                $postData->getPage(),
                $postData->getLimit()
            );

            $this->getTablePaginator()->handlePagination($postData);

            $this->dispatchPostSetPaginationEvent(
                $this->getTablePaginator()->getCurrentPage(),
                $this->getTablePaginator()->getLimit()
            );
        }

        return $this;
    }

    /**
     * @param TableData $postData
     *
     * @throws \Exception
     *
     * @return $this
     */
    protected function handleSorting(TableData $postData): self
    {
        if ($this->isSortingEnabled()) {
            //$this->dispatchPreSetSortingEvent(
            //    $postData['column'] ?? null,
            //    $postData['direction'] ?? null
            //);

            $this->getTableSorter()->handleSorting($postData);

            //$this->dispatchPostSetSortingEvent(
            //    $this->getTableSorter()->getSortingColumn(),
            //    $this->getTableSorter()->getSortingDirection()
            //);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getListOffset(): int
    {
        $offset = 0;

        if ($this->isPaginationEnabled()) {
            $offset = $this->getTablePaginator()->getListOffset();
        }

        return $offset;
    }

    /**
     * @return int|null
     */
    public function getListLimit(): ?int
    {
        $limit = null;

        if ($this->isPaginationEnabled()) {
            $limit = $this->getTablePaginator()->getListLimit();
        }

        return $limit ?? 10;
    }

    /**
     * @return array|null
     */
    public function getListOrder(): ?array
    {
        $order = null;

        if ($this->isSortingUsed()) {
            $orderMap = $this->getOrderMap();
            $order = $this->getTableSorter()->getListOrder($orderMap);
        }

        return $order;
    }

    /**
     * @return string
     */
    public function getCheckboxesColumnLabel(): string
    {
        if (
            true == $this->showCheckboxes &&
            null !== $this->getTableChecker()
        ) {
            return $this->getTableChecker()->getColumnLabel();
        }

        return '';
    }

    /**
     * @return array
     */
    public function getCriteriaMap(): array
    {
        return $this->criteriaMap;
    }

    /**
     * @param array $criteriaMap
     *
     * @return $this
     */
    public function setCriteriaMap(array $criteriaMap): self
    {
        $this->criteriaMap = $criteriaMap;

        return $this;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return $this->orderMap;
    }

    /**
     * @param array $orderMap
     *
     * @return $this
     */
    public function setOrderMap(array $orderMap): self
    {
        $this->orderMap = $orderMap;

        return $this;
    }

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @throws ExecutorArrayAccessOnNonArrayException
     * @throws ExecutorMethodNotImplementedException
     * @throws TableException
     * @throws \Exception
     *
     * @return array
     */
    public function createView(iterable $list, int $resultsCount = null): array
    {
        return $this->generate($list, $resultsCount);
    }

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @throws ExecutorArrayAccessOnNonArrayException
     * @throws ExecutorMethodNotImplementedException
     * @throws TableException
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function createListResponse(iterable $list, int $resultsCount = null): JsonResponse
    {
        $tableData = $this->generate($list, $resultsCount);

        return new JsonResponse($tableData);
    }
}
