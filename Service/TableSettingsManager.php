<?php

namespace TableBundle\Service;

use DanlineBundle\Entity\User;
use DanlineBundle\Exception\UserNotPresentInTokenException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use TableBundle\Entity\TableSetting;

/**
 * Class TableSettingsManager
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableSettingsManager
{
    /** @var EntityManager */
    private $entityManager;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * @param TableSetting $settings
     *
     * @return bool
     */
    public function saveTableSettings(TableSetting $settings): bool
    {
        $entityManager = $this->entityManager;

        try {
            $entityManager->persist($settings);
            $entityManager->flush();
        } catch (\Throwable $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $tableName
     * @param array $tableSettings
     *
     * @throws UserNotPresentInTokenException
     *
     * @return bool
     */
    public function saveTableSettingsFromArray(string $tableName, array $tableSettings): bool
    {
        $settings = $this->getCurrentUserSettingsByName($tableName);

        if (isset($tableSettings['page'])) {
            $settings->setPage($tableSettings['page']);
        }

        if (isset($tableSettings['limit'])) {
            $settings->setLimit($tableSettings['limit']);
        }

        if (isset($tableSettings['sorting'])) {
            $settings->setSorting($tableSettings['sorting']);
        }

        if (isset($tableSettings['criteria'])) {
            $settings->setCriteria($tableSettings['criteria']);
        }

        if (isset($tableSettings['columns'])) {
            $settings->setColumns($tableSettings['columns']);
        }

        return $this->saveTableSettings($settings);
    }

    // ~

    /**
     * @param EntityManager $entityManager
     *
     * @return void
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     *
     * @return void
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @throws UserNotPresentInTokenException
     *
     * @return User
     */
    protected function getUserFromTokenStorage(): User
    {
        $token = $this->tokenStorage->getToken();

        if ((null !== $token) && $token->getUser() instanceof User) {
            return $token->getUser();
        }

        throw new UserNotPresentInTokenException('User is not present in token.');
    }

    /**
     * @param string $name
     *
     * @throws UserNotPresentInTokenException
     *
     * @return TableSetting
     */
    public function getCurrentUserSettingsByName(string $name): TableSetting
    {
        $user = $this->getUserFromTokenStorage();

        $repository = $this->entityManager->getRepository(TableSetting::class);
        $settings = null;

        try {
            $settings = $repository->findByUserAndTableName($user, $name);
        } catch (\Throwable $exception) {
            /* Do nothing. */
        }

        if (null === $settings) {
            $settings = new TableSetting();
            $settings
                ->setUser($user)
                ->setName($name);
        }

        return $settings;
    }

    /**
     * @throws UserNotPresentInTokenException
     *
     * @return TableSetting[]
     */
    public function getCurrentUserSettings(): array
    {
        $user = $this->getUserFromTokenStorage();

        $repository = $this->entityManager->getRepository(TableSetting::class);

        $settings = $repository->findBy(['user' => $user]);

        return $settings;
    }
}
