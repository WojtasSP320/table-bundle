<?php

namespace TableBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use TableBundle\Exception\TableException;
use TableBundle\Service\Model\TableData;

/**
 * TableInterface interface.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
interface TableInterface
{
    /** @var string */
    public const TABLE_SHOW_ACTIONS_NONE = 'none';

    /** @var string */
    public const TABLE_SHOW_ACTIONS_RIGHT = 'right';

    /** @var string */
    public const TABLE_SHOW_ACTIONS_LEFT = 'left';

    /** @var string */
    public const TABLE_SHOW_ACTIONS_BOTH = 'both';

    /** @var string */
    public const TABLE_SHOW_CHECKBOXES_NONE = 'none';

    /** @var string */
    public const TABLE_SHOW_CHECKBOXES_RIGHT = 'right';

    /** @var string */
    public const TABLE_SHOW_CHECKBOXES_LEFT = 'left';

    /** @var string */
    public const TABLE_SHOW_CHECKBOXES_BOTH = 'both';

    /** @var string */
    public const TABLE_SHOW_PAGINATION_TOP = 'top';

    /** @var string */
    public const TABLE_SHOW_PAGINATION_BOTTOM = 'bottom';

    /** @var string */
    public const TABLE_SHOW_PAGINATION_BOTH = 'both';

    /** @var string */
    public const TABLE_SHOW_PAGINATION_NONE = 'none';

    /** @var string */
    public const TABLE_SETTINGS_SESSION_KEY = 'tableSettings';

    /** @var string */
    public const TABLE_SETTINGS_DATABASE_KEY = 'tableSettings';

    /** @var string */
    public const PRE_SET_DATA = 'table.pre_set_data';

    /** @var string */
    public const POST_SET_DATA = 'table.post_set_data';

    /** @var string */
    public const PRE_SET_CRITERIA = 'table.pre_set_criteria';

    /** @var string */
    public const PRE_SET_SORTING = 'table.pre_set_sorting';

    /** @var string */
    public const POST_SET_SORTING = 'table.post_set_sorting';

    /** @var string */
    public const PRE_SET_PAGINATION = 'table.pre_set_pagination';

    /** @var string */
    public const POST_SET_PAGINATION = 'table.post_set_pagination';

    /** @var string */
    public const POST_GENERATE = 'table.post_generate';

    // ~

    /**
     * @return mixed
     */
    public function buildTable();

    /**
     * @return $this
     */
    public function handlePostData();

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return ArrayCollection|Column[]
     */
    public function getColumns(): ArrayCollection;

    /**
     * @return array
     */
    public function getCriteriaMap(): array;

    /**
     * @return array
     */
    public function getOrderMap(): array;

    /**
     * @param array $additionalCriteria
     * @param bool $override
     *
     * @return array
     */
    public function getListCriteria(array $additionalCriteria = [], bool $override = false): array;

    /**
     * Gets offset for list function
     * Paginator proxy method
     *
     * @return int|null
     */
    public function getListOffset(): ?int;

    /**
     * @return array|null
     */
    public function getListOrder(): ?array;

    /**
     * Gets limit for list function
     * Paginator proxy method
     *
     * @return int|null
     */
    public function getListLimit(): ?int;

    /**
     * @return ArrayCollection
     */
    public function getFilters(): ArrayCollection;

    /**
     * @param string $eventName
     * @param callable $listener
     * @param int $priority
     */
    public function addEventListener(string $eventName, callable $listener, int $priority = 0);

    /**
     * @param string $name
     * @param Column $column
     *
     * @return Column
     */
    public function addColumn(string $name, Column $column = null): Column;

    /**
     * @param string $name
     *
     * @return Column|null
     */
    public function getColumn(string $name): ?Column;

    /**
     * @param string $name
     * @param Action $action
     *
     * @return Action
     */
    public function addAction(string $name, Action $action = null): Action;

    /**
     * @param string $name
     *
     * @return Action|null
     */
    public function getAction(string $name): ?Action;

    /**
     * @param array $criteriaMap
     *
     * @return $this
     */
    public function setCriteriaMap(array $criteriaMap);

    /**
     * @param array $orderMap
     *
     * @return $this
     */
    public function setOrderMap(array $orderMap);

    /**
     * @throws TableException
     *
     * @return array
     */
    public function getOptions(): array;

    /**
     * @param string $optionName
     *
     * @throws TableException
     *
     * @return mixed
     */
    public function getOption(string $optionName);

    /**
     * "Syntactic sugar" shorthand for addColumn + set hidden
     *
     * @param string $name
     * @param Column $column
     *
     * @throws TableException
     *
     * @return Column
     */
    public function addHiddenColumn(string $name, Column $column = null);

    /**
     * @return $this
     */
    public function showHeader();

    /**
     * @return $this
     */
    public function hideHeader();

    /**
     * @return $this
     */
    public function showLabels();

    /**
     * @return $this
     */
    public function hideLabels();

    /**
     * @return $this
     */
    public function showFilters();

    /**
     * @return $this
     */
    public function hideFilters();

    /**
     * @return $this
     */
    public function showSorting();

    /**
     * @return $this
     */
    public function hideSorting();

    /**
     * @param string $showCheckboxes
     *
     * @return $this
     */
    public function showCheckboxes(string $showCheckboxes = self::TABLE_SHOW_CHECKBOXES_LEFT);

    /**
     * @return $this
     */
    public function hideCheckboxes();

    /**
     * @param string $showActions
     *
     * @throws TableException
     *
     * @return $this
     */
    public function showActions(string $showActions = self::TABLE_SHOW_ACTIONS_RIGHT);

    /**
     * @param string $showPagination
     *
     * @throws TableException
     *
     * @return $this
     */
    public function showPagination(string $showPagination = self::TABLE_SHOW_PAGINATION_BOTH);

    /**
     * @return $this
     */
    public function hideActions();

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher(): EventDispatcherInterface;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return $this
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher);

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options);

    /**
     * @param array $columnsOrder
     *
     * @return $this
     */
    public function setColumnsOrder(array $columnsOrder);

    /**
     * @return array
     */
    public function getColumnsOrder(): array;

    /**
     * @param TableData $postData
     *
     * @return $this
     */
    public function setPostData(TableData $postData);

    /**
     * @param TablePaginator $tablePaginator
     *
     * @return $this
     */
    public function setTablePaginator(TablePaginator $tablePaginator);

    /**
     * @param TableSorter $tableSorter
     *
     * @return $this
     */
    public function setTableSorter(TableSorter $tableSorter);

    /**
     * @return TableSorter|null
     */
    public function getTableSorter(): ?TableSorter;

    /**
     * @param TableChecker $tableChecker
     *
     * @return $this
     */
    public function setTableChecker(TableChecker $tableChecker);

    /**
     * @return FilterFactory|null
     */
    public function getFilterFactory(): ?FilterFactory;

    /**
     * @param FilterFactory $filterFactory
     *
     * @return $this
     */
    public function setFilterFactory(FilterFactory $filterFactory);

    /**
     * @param string $filterName
     *
     * @return FilterInterface|null
     */
    public function getFilter(string $filterName): ?FilterInterface;

    /**
     * @internal
     *
     * @param FilterInterface $filter
     * @param string|null $oldName
     *
     * @throws TableException
     */
    public function setFilter(FilterInterface $filter, string $oldName = null);

    /**
     * @return CellFactory|null
     */
    public function getCellFactory(): ?CellFactory;

    /**
     * @param CellFactory $cellFactory
     *
     * @return TableInterface
     */
    public function setCellFactory(CellFactory $cellFactory);

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @return array
     */
    public function generate(iterable $list, int $resultsCount = null): array;

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @return array
     */
    public function createView(iterable $list, int $resultsCount = null): array;

    /**
     * @param iterable $list
     * @param int|null $resultsCount
     *
     * @return JsonResponse
     */
    public function createListResponse(iterable $list, int $resultsCount = null): JsonResponse;

    /**
     * Forces table to be sorted with given order
     * even if table sorter is disabled (but present).
     * If sorting is not disabled it disables it automatically.
     *
     * Returns true if table has table sorter and sorting has been forced
     * returns false otherwise.
     *
     * @param array $sorting
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function forceUseSorting(array $sorting): bool;

    /**
     * @return bool
     */
    public function isSortingUsed(): bool;
}
