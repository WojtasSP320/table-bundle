<?php

namespace TableBundle\Service;

use TableBundle\Exception\ExecutorException;

/**
 * QuietExecutor class.
 *
 * Executes methods chain for object given later.
 *
 * @example
 *
 *  $executor = new Executor();
 *  $executor->getFoo('bar')->getNames()[0]->getSurname($a, $b, $c);
 *  ...
 *  $result = $executor->execute($item);
 * @example
 *
 *  $result = Executor::getFoo('bar')->getNames()[0]->getSurname($a, $b, $c)->execute($item);
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class QuietExecutor extends Executor implements \ArrayAccess
{
    /**
     * @param mixed $object
     *
     * @throws \Exception
     *
     * @return array|mixed|null
     */
    public function execute($object)
    {
        try {
            return parent::execute($object);
        } catch (ExecutorException $exception) {
            /* Do nothing. */
        }

        // ~

        return null;
    }
}
