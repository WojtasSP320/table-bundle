<?php

namespace TableBundle\Service;

/**
 * TableBuilderInterface class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
interface TableBuilderInterface
{
    /**
     * @param TableInterface $table
     * @param array $options
     *
     * @return TableInterface
     */
    public function buildTable(TableInterface $table, array $options = []);
}
