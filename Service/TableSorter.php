<?php

namespace TableBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use TableBundle\Enums\SortingDirectionEnum;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\Model\TableData;

/**
 * TableSorter class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableSorter
{
    /** @var string */
    public const DEFAULT_SORTING_DIRECTION = SortingDirectionEnum::ASC;

    /**
     * @deprecated use SortingDirectionEnum::ASC
     *
     * @var string
     */
    public const DIRECTION_ASC = SortingDirectionEnum::ASC;

    /**
     * @deprecated use SortingDirectionEnum::DESC
     *
     * @var string
     */
    public const DIRECTION_DESC = SortingDirectionEnum::DESC;

    /** @var bool */
    private $sortingEnabled = true;

    /** @var ArrayCollection|Column[] */
    private $columns;

    /** @var ArrayCollection|Column[] */
    private $sortedColumns;

    /** @var bool */
    private $forceSorting = false;

    /**
     * @param ArrayCollection $columns
     */
    public function __construct(ArrayCollection $columns)
    {
        $this
            ->setSortedColumns(new ArrayCollection())
            ->setColumns($columns)
            ->enableSorting()
        ;
    }

    /**
     * @return bool
     */
    public function isSortingEnabled(): bool
    {
        return $this->sortingEnabled;
    }

    /**
     * @return bool
     */
    public function isSortingForced(): bool
    {
        return $this->forceSorting;
    }

    /**
     * @return $this
     */
    public function enableSorting(): self
    {
        $this->sortingEnabled = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disableSorting(): self
    {
        $this->sortingEnabled = false;

        return $this;
    }

    /**
     * @return ArrayCollection|Column[]
     */
    public function getColumns(): ArrayCollection
    {
        return $this->columns;
    }

    /**
     * @param ArrayCollection $columns
     *
     * @return TableSorter
     */
    public function setColumns(ArrayCollection $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @param ArrayCollection $sortedColumns
     *
     * @return $this
     */
    private function setSortedColumns(ArrayCollection $sortedColumns): self
    {
        $this->sortedColumns = $sortedColumns;

        return $this;
    }

    /**
     * @param Column $column
     *
     * @return $this
     */
    public function addSortedColumn(Column $column): self
    {
        $this->sortedColumns->add($column);

        return $this;
    }

    /**
     * @param Column $column
     *
     * @return $this
     */
    public function removeSortedColumn(Column $column): self
    {
        $this->sortedColumns->removeElement($column);

        return $this;
    }

    /**
     * @param TableData $postData
     *
     * @throws \Exception
     *
     * @return $this
     */
    public function handleSorting(TableData $postData): self
    {
        if (false === $this->isSortingForced()) {
            $columns = \array_keys($postData->getSorting());
            $directions = \array_values($postData->getSorting());

            // ~

            $this->handleMultiSorting($columns, $directions);
        }

        // ~

        return $this;
    }

    /**
     * @param array $columns
     * @param array $directions
     *
     * @throws \Exception
     */
    protected function handleMultiSorting(array $columns, array $directions): void
    {
        if (\count($columns) !== \count($directions)) {
            throw new ColumnException('Columns array has different length than directions array!');
        }

        foreach ($columns as $key => $column) {
            $this->handleSingleSorting($column, $directions[$key]);
        }
    }

    /**
     * @param string $columnName
     * @param string $direction
     *
     * @throws \Exception
     */
    protected function handleSingleSorting(string $columnName, string $direction): void
    {
        $tableColumn = null;

        if ('' === $columnName || '' === $direction) {
            return;
        }

        $tableColumn = $this->validateSortingColumn($columnName);

        if (null !== $tableColumn) {
            $direction = $this->validateSortingDirection($direction);
            $tableColumn->setSortingDirection($direction);
            $this->sortedColumns->add($tableColumn);
        }
    }

    // ~

    /**
     * Get table sorting (multi-sorted)
     *
     * @return array
     */
    public function getSorting(): array
    {
        $sorting = [];

        foreach ($this->sortedColumns as $sortedColumn) {
            $sorting[$sortedColumn->getName()] = $sortedColumn->getSortingDirection();
        }

        return $sorting;
    }

    // ~

    /**
     * @param array $orderMap
     *
     * @return array
     */
    public function getListOrder(array $orderMap): array
    {
        $parsedOrder = [];
        $sorting = $this->getSorting();

        foreach ($sorting as $columnName => $direction) {
            $parsedOrder += $this->parseSort($columnName, $direction, $orderMap);
        }

        return $parsedOrder;
    }

    // ~

    /**
     * Validates if sorting direction value matches valid sorting direction values
     *
     * @param string $direction
     *
     * @throws ColumnException
     *
     * @return string
     */
    protected function validateSortingDirection(string $direction): string
    {
        $direction = \strtolower($direction);

        if (false === \in_array($direction, [self::DIRECTION_ASC, self::DIRECTION_DESC], true)) {
            throw new ColumnException('Sorting direction must be either "asc" or "desc", "%s" given!', [
                $direction,
            ]);
        }

        return $direction;
    }

    /**
     * Validates if column with given post data filter exists in table
     *
     * @param string $columnName
     *
     * @throws ColumnException
     *
     * @return Column
     */
    protected function validateSortingColumn(string $columnName): Column
    {
        $foundColumn = null;

        /** @var Column $column */
        foreach ($this->getColumns() as $column) {
            if ($column->getName() === $columnName) {
                $foundColumn = $column;
                break;
            }
        }

        if (null === $foundColumn) {
            throw new ColumnException('Trying to sort by non-existing columns filter name: "%s"!', [
                $columnName,
            ]);
        }

        return $foundColumn;
    }

    /**
     * @param array $orderMap
     *
     * @throws ColumnException
     *
     * @return $this
     */
    public function validateSortingOrderMap(array $orderMap): self
    {
        foreach ($this->columns as $column) {
            $columnName = $column->getName();
            $columnSortingEnabled = $column->isSortingEnabled();

            $isValid = (
                (!$columnSortingEnabled) ||
                \array_key_exists($columnName, $orderMap)
            );

            if (!$isValid) {
                throw new ColumnException(sprintf('Order map is invalid for column "%s"', $columnName));
            }
        }

        return $this;
    }

    /**
     * Forces to use given sorting order
     * and disables table generic sorting.
     *
     * $sorting = array(
     *     column1 => direction1,
     *     column2 => direction2,
     *     ...
     * );
     *
     * @param array $sorting
     *
     * @throws \Exception
     *
     * @return $this
     */
    public function forceUseSorting(array $sorting): self
    {
        $columns = \array_keys($sorting);
        $directions = \array_values($sorting);

        $this->handleMultiSorting($columns, $directions);

        $this->forceSorting = true;
        $this->disableSorting();

        return $this;
    }

    // ~

    /**
     * If filed value is array, then:
     *
     * key is database tables filed name
     * value is sorting direction with following values:
     *
     *  ASC - fixed ascending
     *  DESC - fixed descending
     *
     * @param string $columnName
     * @param string $direction
     * @param array $map
     *
     * @return array
     */
    protected function parseSort(string $columnName, string $direction, array $map): array
    {
        $parsedSort = [\reset($map) => self::DEFAULT_SORTING_DIRECTION];

        // ~

        if (true === \array_key_exists($columnName, $map)) {
            if (false === \is_array($map[$columnName])) {
                $parsedSort = [$map[$columnName] => $direction];
            } else {
                $parsedSort = [];

                // ~

                foreach ($map[$columnName] as $key => $value) {
                    $parsedSort[$key] = $value;
                }
            }
        }

        // ~

        return $parsedSort;
    }
}
