<?php

namespace TableBundle\Service;

use Closure;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\FilterException;

/**
 * Interface FilterInterface
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
interface FilterInterface
{
    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver);

    /**
     * @param string $name
     *
     * @throws FilterException
     *
     * @return $this
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setValue($value);

    /**
     * @return array
     */
    public function getAttributes(): array;

    /**
     * @param array $attributes
     *
     * @return $this
     */
    public function setAttributes(array $attributes);

    /**
     * @return Column|null
     */
    public function getColumn(): ?Column;

    /**
     * @param Column $column
     *
     * @return $this
     */
    public function setColumn(Column $column);

    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool;

    /**
     * @param Closure|null $modelTransformer
     * @param Closure|null $modelReverseTransformer
     *
     * @return mixed
     */
    public function setModelTransformer(Closure $modelTransformer = null, Closure $modelReverseTransformer = null);

    /**
     * @param Closure|null $viewTransformer
     * @param Closure|null $viewReverseTransformer
     *
     * @return mixed
     */
    public function setViewTransformer(Closure $viewTransformer = null, Closure $viewReverseTransformer = null);

    /**
     * @throws FilterException
     *
     * @return string
     */
    public function getComponentName(): string;

    /**
     * Tries to find filter name in criteria array.
     * If $criteria[filter_name] has been found uses
     * its value for filter value.
     *
     * If $criteria[filter_name] is not present uses
     * default_value from options.
     *
     * @param array $criteria
     *
     * @throws FilterException
     */
    public function handleCriteria(array $criteria);

    /**
     * Filter data transformers works similar to form data transformers.
     * Internally data is stored in norm form and is transformed either to view form or model form.
     *
     * MODEL_DATA =======(modelTransformer)=====> NORM_DATA ======(viewTransformer)======> VIEW_DATA
     *
     * MODEL_DATA <==(modelReverseTransformer)=== NORM_DATA <==(viewReverseTransformer)=== VIEW_DgetRoleATA
     *
     * If no transformer is defined, setModelValue & setViewValue works equal to setValue,
     * and getModelValue & getViewValue works equal to getValue.
     */

    /**
     * Returns norm data transformed to model data
     *
     * @return mixed
     */
    public function getModelValue();

    /**
     * Sets norm data from model data
     *
     * @param mixed $value
     *
     * @return $this|mixed
     */
    public function setModelValue($value);

    /**
     * Sets norm data from view data
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setViewValue($value);

    /**
     * Returns norm data transformed to view data
     *
     * @return mixed
     */
    public function getViewValue();

    /**
     * @return $this
     */
    public function initializeFilter();

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @param TableInterface $table
     *
     * @return $this
     */
    public function setTable(TableInterface $table);
}
