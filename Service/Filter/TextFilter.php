<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TextFilter extends FilterAbstract
{
    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'placeholder' => 'Wpisz wartość',
        ]);

        $optionsResolver->setAllowedTypes('default_value', 'string');

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'TextFilter';
    }
}
