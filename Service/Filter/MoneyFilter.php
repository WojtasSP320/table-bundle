<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class MoneyFilter extends FilterAbstract
{
    /** @var string */
    public $defaultCurrency = '';

    /**
     * MoneyFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);

        $moneySuffix = $this->defaultCurrency;

        // ~

        $modelTransformer = function ($modelValue) {
            return (null === $modelValue) ? null : (float)$modelValue;
        };

        $viewTransformer = function ($normValue) use ($moneySuffix) {
            return (null === $normValue)
                ? null
                : \sprintf('%s %s', \round($normValue, 2), $moneySuffix)
            ;
        };

        $modelReverseTransformer = function ($normValue) {
            return $normValue ?? '';
        };

        $viewReverseTransformer = function ($viewValue) {
            if (\preg_match('/[-\+]{0,1}[\d]+[\d ]*[\.,]{0,1}\d*/i', $viewValue, $matches)) {
                return $matches[0];
            }

            return null;
        };

        // ~

        $this->setModelTransformer($modelTransformer, $modelReverseTransformer);
        $this->setViewTransformer($viewTransformer, $viewReverseTransformer);
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'input_attributes' => [],
            'default_value' => null,
        ]);

        $optionsResolver->setAllowedTypes('default_value', ['integer', 'null']);

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'MoneyFilter';
    }

    /**
     * @param string $defaultCurrency
     *
     * @return $this;
     */
    public function setDefaultCurrency(string $defaultCurrency): self
    {
        $this->defaultCurrency = $defaultCurrency;

        return $this;
    }
}
