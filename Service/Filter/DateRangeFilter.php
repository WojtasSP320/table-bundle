<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class DateRangeFilter extends FilterAbstract
{
    /**
     * DateRangeFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);

        $modelTransformer = function ($modelValue) {
            return $this->modelToNormData($modelValue);
        };

        $modelReverseTransformer = function ($normValue) {
            return $this->normToModelData($normValue);
        };

        // ~

        $viewTransformer = function ($normValue) {
            return $this->normToViewData($normValue);
        };

        $viewReverseTransformer = function ($normValue) {
            return $this->viewToNormData($normValue);
        };

        // ~

        $this->setModelTransformer($modelTransformer, $modelReverseTransformer);
        $this->setViewTransformer($viewTransformer, $viewReverseTransformer);
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'input_attributes' => [],
            'format' => 'Y-m-d',
            'placeholder_from' => 'yyyy-mm-dd',
            'placeholder_to' => 'yyyy-mm-dd',
            'default_value' => [
                'from' => '',
                'to' => '',
            ],
        ]);

        $optionsResolver->setRequired([]);

        return $this;
    }

    /**
     * @param array $criteria
     *
     * @return $this
     */
    public function handleCriteria(array $criteria): FilterAbstract
    {
        $filterValue = $this->getAttributes()['default_value'];

        $filterName = $this->getName();

        $filterValue['from'] = $criteria[$filterName]['from'] ?? '';
        $filterValue['to'] = $criteria[$filterName]['to'] ?? '';

        $this->setModelValue($filterValue);

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool
    {
        $criteriaMap = $this->getColumn()->getTable()->getCriteriaMap();

        $filterName = $this->getName();

        $criteriaMapValid = (
            \array_key_exists($filterName, $criteriaMap) &&
            \array_key_exists('from', $criteriaMap[$filterName]) &&
            \array_key_exists('to', $criteriaMap[$filterName])
        );

        return $criteriaMapValid;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'DateRangeFilter';
    }

    /**
     * @param \DateTime|string|array $date
     * @param string|null $format
     *
     * @throws \Exception
     *
     * @return \DateTime|null
     */
    protected function createValidDate($date, string $format = null): ?\DateTime
    {
        $dateObject = null;

        if ($date instanceof \DateTime) {
            $dateObject = $date;
        } elseif (!empty($date)) {
            if (null === $format) {

                /** todo: ad-hoc */
                if (\is_array($date)) {
                    $parts = \explode(' ', $date['date']);
                    $date = \reset($parts);
                }

                $format = $this->resolveDateFormat($date);
            }

            $dateObject = \DateTime::createFromFormat($format, $date);
            $dateObject = ($dateObject && $dateObject->format($format) === $date)
                ? $dateObject
                : null;
        }

        return $dateObject;
    }

    /**
     * @param string $date
     *
     * @throws \Exception
     *
     * @return string
     */
    protected function resolveDateFormat(string $date): string
    {
        switch (true) {
            case (1 === \preg_match('/\d{4}-\d{2}-\d{2}/', $date)):
                $format = 'Y-m-d';
                break;
            case (1 === \preg_match('/\d{2}\.\d{2}\.\d{4}/', $date)):
                $format = 'd.m.Y';
                break;
            default:
                throw new \Exception('Invalid date format!');
        }

        return $format;
    }

    /**
     * @param array $modelValue
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function modelToNormData(array $modelValue)
    {
        $normValue['from'] = $this->createValidDate($modelValue['from']);
        $normValue['to'] = $this->createValidDate($modelValue['to']);

        return $normValue;
    }

    /**
     * @param array $normValue
     *
     * @return mixed
     */
    public function normToViewData(array $normValue = null)
    {
        /** @var \DateTime[] $normValue */
        $viewValue['from'] = (null === $normValue['from']) ? null : $normValue['from']->format('Y-m-d');
        $viewValue['to'] = (null === $normValue['to']) ? null : $normValue['to']->format('Y-m-d');

        return $viewValue;
    }

    /**
     * @param array $normValue
     *
     * @return mixed
     */
    public function normToModelData(array $normValue = null)
    {
        /** @var \DateTime[] $normValue */
        $modelValue['from'] = (null === $normValue['from']) ? '' : $normValue['from']->format('Y-m-d 00:00:00');
        $modelValue['to'] = (null === $normValue['to']) ? '' : $normValue['to']->format('Y-m-d 23:59:59');

        return $modelValue;
    }

    /**
     * @param array $viewValue
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function viewToNormData(array $viewValue)
    {
        $normValue['from'] = $this->createValidDate($viewValue['from']);
        $normValue['to'] = $this->createValidDate($viewValue['to']);

        return $normValue;
    }
}
