<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PlaceNumberFilter extends FilterAbstract
{
    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool
    {
        $criteriaMap = $this->getColumn()->getTable()->getCriteriaMap();

        return isset(
            $criteriaMap[$this->getName()]['sector'],
            $criteriaMap[$this->getName()]['alley'],
            $criteriaMap[$this->getName()]['rack'],
            $criteriaMap[$this->getName()]['shelf']
        );
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        $optionsResolver->setDefaults([
            'input_attributes' => [],
            'default_value' => ['sector' => null, 'alley' => null, 'rack' => null, 'shelf' => null],
        ]);

        $optionsResolver->setAllowedTypes('default_value', ['array']);

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'PlaceNumberFilter';
    }
}
