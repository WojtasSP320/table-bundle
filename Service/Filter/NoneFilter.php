<?php

namespace TableBundle\Service\Filter;

use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class NoneFilter extends FilterAbstract
{
    /**
     * NoneFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'NoneFilter';
    }
}
