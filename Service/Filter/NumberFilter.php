<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class NumberFilter extends FilterAbstract
{
    /**
     * NumberFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);

        // ~

        $modelTransformer = function ($modelValue) {
            return empty($modelValue) ? null : (int)$modelValue;
        };

        $viewTransformer = function ($normValue) {
            return (null === $normValue) ? '' : (int)$normValue;
        };

        $modelReverseTransformer = function ($normValue) {
            return (null === $normValue) ? '' : (int)$normValue;
        };

        $viewReverseTransformer = function ($viewValue) {
            return ('' === $viewValue) ? null : (int)$viewValue;
        };

        // ~

        $this->setModelTransformer($modelTransformer, $modelReverseTransformer);
        $this->setViewTransformer($viewTransformer, $viewReverseTransformer);
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'input_attributes' => [],
            'default_value' => null,
        ]);

        $optionsResolver->setAllowedTypes('default_value', ['integer', 'null']);

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'NumberFilter';
    }
}
