<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TriboxFilter extends FilterAbstract
{
    /**
     * TriboxFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);

        $transformerCallback = function ($viewValue) {
            if ($viewValue === 'null') {
                return null;
            }

            if ($viewValue === 'true') {
                return true;
            }

            if ($viewValue === 'false') {
                return false;
            }

            $intValue = \filter_var($viewValue, FILTER_VALIDATE_INT);
            if (\is_int($intValue)) {
                return $intValue;
            }

            $floatValue = \filter_var($viewValue, FILTER_VALIDATE_FLOAT);
            if (\is_float($floatValue)) {
                return $floatValue;
            }

            return $viewValue;
        };

        $this->setViewTransformer($transformerCallback, $transformerCallback);
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'positive' => 1,
            'negative' => 0,
            'neutral' => '',
        ]);

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'TriboxFilter';
    }
}
