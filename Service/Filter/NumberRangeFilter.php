<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class NumberRangeFilter extends FilterAbstract
{
    /** @var int */
    private $decimalDigits;

    /** @var string */
    private $thousandSeparator;

    /** @var string */
    private $decimalPoint;

    /**
     * NumberRangeFilter constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        parent::__construct($name, $attributes);

        // ~

        $modelTransformer = function ($modelValue) {
            return $this->modelToNormData($modelValue);
        };

        $modelReverseTransformer = function ($normValue) {
            return $this->normToModelData($normValue);
        };

        // ~

        $viewTransformer = function ($normValue) {
            return $this->normToViewData($normValue);
        };

        $viewReverseTransformer = function ($normValue) {
            return $this->viewToNormData($normValue);
        };

        // ~

        $this->setModelTransformer($modelTransformer, $modelReverseTransformer);
        $this->setViewTransformer($viewTransformer, $viewReverseTransformer);
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'input_attributes' => [],
            'default_value' => [
                'from' => null,
                'to' => null,
            ],
            'placeholder_from' => 'Wartość od',
            'placeholder_to' => 'Wartość do',
        ]);

        return $this;
    }

    /**
     * @param array $criteria
     *
     * @return $this;
     */
    public function handleCriteria(array $criteria): FilterAbstract
    {
        $filterValue = $this->getAttributes()['default_value'];

        $filterName = $this->getName();

        $filterValue['from'] = $criteria[$filterName]['from'] ?? '';
        $filterValue['to'] = $criteria[$filterName]['to'] ?? '';

        $this->setModelValue($filterValue);

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool
    {
        $criteriaMap = $this->getColumn()->getTable()->getCriteriaMap();

        $filterName = $this->getName();

        $criteriaMapValid = (
            \array_key_exists($filterName, $criteriaMap) &&
            \array_key_exists('from', $criteriaMap[$filterName]) &&
            \array_key_exists('to', $criteriaMap[$filterName])
        );

        return $criteriaMapValid;
    }

    /**
     * @param mixed $modelValue
     *
     * @return mixed
     */
    public function modelToNormData($modelValue)
    {
        $modelValue = (false == empty($modelValue)) ? $modelValue : [];

        $callback = function ($value) {
            return '' !== $value
                ? (float)\str_replace([$this->getThousandSeparator(), $this->getDecimalPoint()], ['', '.'], $value)
                : null
            ;
        };

        $normValue = \array_map($callback, $modelValue);

        return $normValue;
    }

    /**
     * @param mixed $normValue
     *
     * @return mixed
     */
    public function normToViewData($normValue)
    {
        $normValue = (false == empty($normValue)) ? $normValue : [];

        $callback = function ($value) {
            return (false == empty($value))
                ? \number_format($value, $this->getDecimalDigits(), $this->getDecimalPoint(), $this->getThousandSeparator())
                : '';
        };

        $viewValue = \array_map($callback, $normValue);

        return $viewValue;
    }

    /**
     * @param mixed $normValue
     *
     * @return mixed
     */
    public function normToModelData($normValue)
    {
        $normValue = (false == empty($normValue)) ? $normValue : [];

        $callback = function ($value) {
            return (false == empty($value))
                ? (string)$value
                : ''
            ;
        };

        $modelValue = \array_map($callback, $normValue);

        return $modelValue;
    }

    /**
     * @param mixed $viewValue
     *
     * @return mixed
     */
    public function viewToNormData($viewValue)
    {
        $callback = function ($value) {
            return '' !== $value
                ? (float)\str_replace([$this->getThousandSeparator(), $this->getDecimalPoint()], ['', '.'], $value)
                : null
            ;
        };

        $normValue = \array_map($callback, $viewValue);

        return $normValue;
    }

    /**
     * @return string
     */
    public function getThousandSeparator(): string
    {
        return $this->thousandSeparator;
    }

    /**
     * @param string $thousandSeparator
     *
     * @return $this
     */
    public function setThousandSeparator(string $thousandSeparator): self
    {
        $this->thousandSeparator = $thousandSeparator;

        return $this;
    }

    /**
     * @return string
     */
    public function getDecimalPoint(): string
    {
        return $this->decimalPoint;
    }

    /**
     * @param string $decimalPoint
     *
     * @return $this
     */
    public function setDecimalPoint(string $decimalPoint): self
    {
        $this->decimalPoint = $decimalPoint;

        return $this;
    }

    /**
     * @return int
     */
    public function getDecimalDigits(): int
    {
        return $this->decimalDigits;
    }

    /**
     * @param int $decimalDigits
     *
     * @return $this
     */
    public function setDecimalDigits(int $decimalDigits): self
    {
        $this->decimalDigits = $decimalDigits;

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'NumberRangeFilter';
    }
}
