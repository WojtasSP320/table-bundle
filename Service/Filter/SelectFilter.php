<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class SelectFilter extends FilterAbstract
{
    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'trans_domain' => null,
            'trans_locale' => null,
            'trans' => false,
            'placeholder' => '',
            'allow_clear' => true,
            'multiple' => false,
            'tags' => false,
        ]);

        $optionsResolver->setRequired([
            'choices',
        ]);

        $optionsResolver->setDefined([
            'trans_prefix',
        ]);

        $optionsResolver
            ->setAllowedTypes('choices', 'array')
            ->setAllowedTypes('trans', 'bool')
            ->setAllowedTypes('allow_clear', 'bool')
            ->setAllowedTypes('trans_prefix', 'string')
            ->setAllowedTypes('trans_domain', ['null', 'string'])
            ->setAllowedTypes('trans_locale', ['string', 'null'])
            ->setAllowedTypes('placeholder', ['string'])
            ->setAllowedTypes('multiple', 'boolean')
            ->setAllowedTypes('tags', 'boolean')
        ;

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'SelectFilter';
    }
}
