<?php

namespace TableBundle\Service\Filter;

use MainBundle\Services\BarcodesParser;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * Class OrderItemBarcodeFilter
 *
 * @author Jakub Ciach <jciach@danhoss.com>
 */
class OrderItemBarcodeFilter extends FilterAbstract
{
    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        $this->setModelTransformer(static function($code) {
            if (empty($code)) {
                return null;
            }

            $barcodeParser = new BarcodesParser();
            return (int)$barcodeParser->extractOrderItemNumber($code);
        });

        $this->setViewTransformer(static function($partyboxOrderItemId) {
            if (empty($partyboxOrderItemId)) {
                return null;
            }

            $barcodeParser = new BarcodesParser();
            return $barcodeParser->generateOrderItemCode($partyboxOrderItemId);
        });

        $optionsResolver->setDefaults([
            'placeholder' => 'Wpisz wartość',
        ]);

        $optionsResolver->setAllowedTypes('default_value', 'string');

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'TextFilter';
    }
}
