<?php

namespace TableBundle\Service\Filter;

use Closure;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\FilterException;
use TableBundle\Service\FilterAbstract;

/**
 * EntityFilter class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class EntityFilter extends FilterAbstract
{
    /** @var Registry|null */
    private $doctrine;

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        $optionsResolver->setRequired([
            'choice_label',
        ]);

        $optionsResolver->setDefaults([
            'class' => null,
            'query_builder' => null,
            'repository_method' => null, /* Repository method must return an array, Traversable, QueryBuilder or Query object */
            'hydration' => AbstractQuery::HYDRATE_OBJECT,
            'choice_value' => 'id',
            'allow_clear' => false,
            'placeholder' => null,
            'trans' => false,
            'trans_prefix' => null,
        ]);

        $optionsResolver
            ->setAllowedTypes('class', ['string', 'null'])
            ->setAllowedTypes('query_builder', ['object', 'null'])
            ->setAllowedTypes('repository_method', ['string', 'null'])
            ->setAllowedTypes('hydration', ['integer'])
            ->setAllowedTypes('choice_label', ['object', 'string'])
            ->setAllowedTypes('choice_value', 'string')
            ->setAllowedTypes('allow_clear', 'boolean')
            ->setAllowedTypes('placeholder', ['null', 'string'])
            ->setAllowedTypes('trans', 'boolean')
            ->setAllowedTypes('trans_prefix', ['string', 'null'])
        ;

        return $this;
    }

    /**
     * @throws FilterException
     * @throws ORMException
     *
     * @return FilterAbstract
     */
    public function initializeFilter(): FilterAbstract
    {
        $list = [];

        $entityClass = $this->getAttributes()['class'];
        $hydration = $this->getAttributes()['hydration'];
        $repositoryMethod = $this->getAttributes()['repository_method'];
        $queryBuilderClosure = $this->getAttributes()['query_builder'];

        // ~

        if (null !== $entityClass) {
            /** @var EntityRepository $repository */
            $repository = $this->doctrine->getRepository($entityClass);

            if (null !== $queryBuilderClosure) {
                $list = $this->fetchFromClosure($queryBuilderClosure, $repository, $hydration);
            } else {
                $list = $this->fetchFromRepository($repository, $repositoryMethod, $hydration);
            }
        }

        // ~ QuickFix - todo: fix quick

        $attributes = $this->getAttributes();
        $valuePropertyName = $this->getAttributes()['choice_value'];
        $textPropertyName = $this->getAttributes()['choice_label'];

        $array = [];
        foreach ($list as $item) {
            $label = null;
            $choiceLabel = $this->getAttributes()['choice_label'];

            if ($choiceLabel instanceof Closure) {
                $label = $choiceLabel($item);
            } elseif (is_string($choiceLabel)) {
                $label = $item->$textPropertyName();
            }

            $array[] = [
                'value' => $item->$valuePropertyName(),
                'text' => $label,
            ];
        }

        $attributes['choices'] = \array_column($array, 'text', 'value');

        // ~

        /* Its important to use here simple property instead of setter method */
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @param Registry $doctrine
     *
     * @return $this
     */
    public function setDoctrine(Registry $doctrine): self
    {
        $this->doctrine = $doctrine;

        return $this;
    }

    /**
     * @param Closure $queryBuilderClosure
     * @param ObjectRepository $repository
     * @param int $hydration
     *
     * @throws FilterException
     *
     * @return iterable
     */
    protected function fetchFromClosure(Closure $queryBuilderClosure, ObjectRepository $repository, int $hydration): iterable
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $queryBuilderClosure($repository);

        if (false === $queryBuilder instanceof QueryBuilder) {
            throw new FilterException('Query builder closure must return a QueryBuilder class object but returned "%s" in "%s" filter!', [
                \is_object($queryBuilder) ? \get_class($queryBuilder) : \gettype($queryBuilder),
                $this->getName(),
            ]);
        }

        $list = $queryBuilder->getQuery()->getResult($hydration);

        return $list;
    }

    /**
     * @param EntityRepository $repository
     * @param string|null $repositoryMethod
     * @param int $hydration
     *
     * @return iterable
     */
    protected function fetchFromRepository(EntityRepository $repository, ?string $repositoryMethod, int $hydration): iterable
    {
        if (null !== $repositoryMethod) {
            $list = $repository->$repositoryMethod();

            if ($list instanceof QueryBuilder) {
                $list = $list->getQuery()->getResult($hydration);
            } elseif ($list instanceof Query) {
                $list = $list->getResult($hydration);
            }
        } else {
            $list = $repository->findAll();
        }

        return $list;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'SelectFilter';
    }
}
