<?php

namespace TableBundle\Service\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\FilterAbstract;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class DoubleTextFilter extends FilterAbstract
{
    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this;
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): FilterAbstract
    {
        parent::configureOptionsResolver($optionsResolver);

        // ~

        $optionsResolver->setDefaults([
            'first_text_attributes' => [],
            'second_text_attributes' => [],
            'default_value' => [
                'x' => '',
                'y' => '',
            ],
        ]);

        $optionsResolver->setAllowedTypes('default_value', 'array');

        return $this;
    }

    /**
     * @param array $criteria
     *
     * @return $this;
     */
    public function handleCriteria(array $criteria): FilterAbstract
    {
        $filterValue = $this->getAttributes()['default_value'];

        $firstInputName = $this->getName() . 'x';
        $secondInputName = $this->getName() . 'y';

        if (array_key_exists($firstInputName, $criteria)) {
            $filterValue['x'] = $criteria[$firstInputName];
        }

        if (array_key_exists($secondInputName, $criteria)) {
            $filterValue['y'] = $criteria[$secondInputName];
        }

        $this->setModelValue($filterValue);

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool
    {
        $criteriaMap = $this->getColumn()->getTable()->getCriteriaMap();

        $criteriaMapValid = (
            \array_key_exists($this->getName() . 'x', $criteriaMap) &&
            \array_key_exists($this->getName() . 'y', $criteriaMap)
        );

        return $criteriaMapValid;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'DoubleTextFilter';
    }
}
