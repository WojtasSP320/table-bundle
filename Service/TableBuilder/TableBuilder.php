<?php

namespace TableBundle\Service\TableBuilder;

use DanlineBundle\Exception\UserNotPresentInTokenException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use TableBundle\Exception\TableException;
use TableBundle\Exception\TablePaginatorException;
use TableBundle\Service\CellFactory;
use TableBundle\Service\FilterFactory;
use TableBundle\Service\Model\TableData;
use TableBundle\Service\TableBuilderInterface;
use TableBundle\Service\TableChecker;
use TableBundle\Service\TableInterface;
use TableBundle\Service\TablePaginator;
use TableBundle\Service\TableSettingsManager;
use TableBundle\Service\TableSorter;

/**
 * TableFactory class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableBuilder implements TableBuilderInterface
{
    /** @var RequestStack */
    private $requestStack;

    /** @var ParameterBagInterface */
    private $parameterBag;

    /** @var SessionInterface */
    private $session;

    /** @var TableSettingsManager */
    private $tableSettingsManager;

    /** @var FilterFactory */
    private $filterFactory;

    /** @var CellFactory */
    private $cellFactory;

    public function __construct(
        RequestStack $requestStack,
        ParameterBagInterface $parameterBag,
        SessionInterface $session,
        TableSettingsManager $tableSettingsManager,
        FilterFactory $filterFactory,
        CellFactory $cellFactory
    ) {
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
        $this->session = $session;
        $this->tableSettingsManager = $tableSettingsManager;
        $this->filterFactory = $filterFactory;
        $this->cellFactory = $cellFactory;
    }

    /**
     * @param TableData $postData
     * @param TableData $settingsData
     *
     * @return TableData
     */
    public function mergeSettingsWithPostData(TableData $postData, TableData $settingsData): TableData
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();

        return $request->isMethod(Request::METHOD_POST)
            ? $postData
            : $settingsData;
    }

    /**
     * Returns columns order and visibility array
     *
     * @param TableInterface $table
     *
     * @return array
     */
    private function getColumnsOrder(TableInterface $table): array
    {
        $sessionData = $this->session->get(TableInterface::TABLE_SETTINGS_SESSION_KEY);

        $tableName = $table->getName();

        if (isset($sessionData[$tableName]['columns'])) {
            return $sessionData[$tableName]['columns'];
        }

        return [];
    }

    /**
     * @return TableData
     */
    private function getPostData(): TableData
    {
        /** @var Request $request */
        $request = $this->requestStack->getCurrentRequest();

        return TableData::createFromRequest($request);
    }

    /**
     * @param string $tableName
     *
     * @throws UserNotPresentInTokenException
     *
     * @return TableData
     */
    private function getSettingsData(string $tableName): TableData
    {
        $setting = $this->tableSettingsManager->getCurrentUserSettingsByName($tableName);

        return TableData::createFromTableSettings($setting);
    }

    /**
     * @param array $tableOptions
     *
     * @return array
     */
    private function createTablePaginatorOptions(array $tableOptions): array
    {
        return [
            'limit' => $this->parameterBag->get('table_bundle.paginator.default_limit'),
            'possibleLimits' => $this->parameterBag->get('table_bundle.paginator.default_possible_limits'),
        ];
    }

    // ~

    /**
     * @param TableInterface $table
     * @param array $options
     *
     * @throws \ReflectionException
     * @throws TableException
     * @throws TablePaginatorException
     * @throws UserNotPresentInTokenException
     *
     * @return TableInterface
     */
    public function buildTable(TableInterface $table, array $options = [])
    {
        $eventDispatcher = new EventDispatcher();

        $table->setOptions($options);

        $columnsOrder = $this->getColumnsOrder($table);

        $postData = $this->getPostData();

        $tableData = $this->mergeSettingsWithPostData(
            $this->getPostData(),
            $this->getSettingsData($table->getName())
        );

        $paginatorOptions = $this->createTablePaginatorOptions($options);

        // ~

        $table
            ->setEventDispatcher($eventDispatcher)
            ->setFilterFactory($this->filterFactory)
            ->setCellFactory($this->cellFactory)
            ->setColumnsOrder($columnsOrder)
            ->setPostData($tableData)
            ->setTablePaginator(new TablePaginator($paginatorOptions))
            ->setTableSorter(new TableSorter($table->getColumns()))
            ->setTableChecker(new TableChecker())
            ->buildTable();

        return $table;
    }
}
