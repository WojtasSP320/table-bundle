<?php

namespace TableBundle\Service;

/**
 * CellInterface interface.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
interface CellInterface
{
    /** @var string */
    public const CELL_TYPE_STRING = 'string';

    /** @var string */
    public const CELL_TYPE_NUMBER = 'number';

    /** @var string */
    public const CELL_TYPE_FLOAT = 'float';

    /** @var string */
    public const CELL_TYPE_DATE_TIME = 'dateTime';

    /** @var string */
    public const CELL_TYPE_MONEY = 'money';

    /** @var string */
    public const CELL_TYPE_BOOL = 'bool';

    /** @var string */
    public const CELL_TYPE_PERCENT = 'percent';

    /** @var string */
    public const CELL_TYPE_CUSTOM = 'custom';

    /**
     * @return string
     */
    public function getComponentName(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return array
     */
    public function getOptions(): array;

    /**
     * @param mixed $rawData
     *
     * @return mixed
     */
    public function getViewData($rawData);
}
