<?php

namespace TableBundle\Service;

use ArrayAccess;
use TableBundle\Exception\ExecutorArrayAccessOnNonArrayException;
use TableBundle\Exception\ExecutorMethodNotImplementedException;

/**
 * Executes methods chain for object given later
 *
 * Class Executor
 *
 * @example
 * $executor = new Executor();
 * $executor->getFoo('bar')->getNames()[0]->getSurname($a, $b, $c);
 * ...
 * $result = $executor->execute($item);
 *
 * @method arrayAccess($offset)
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class Executor implements ArrayAccess
{
    /** @var array */
    private $callStack = [];

    /** @var int $pointer */
    private $pointer;

    /** @var mixed|null */
    private $object;

    // ~

    /**
     * @param Executor|null $executor
     */
    public function __construct(self $executor = null)
    {
        $this->pointer = 0;

        /* Copy shifted call stack */
        if (null !== $executor) {
            $callStack = $executor->getCallStack();
            $shiftedCallStack = array_slice($callStack, $executor->getPointer(), null);
            $this->setCallStack($shiftedCallStack);
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @deprecated
     *
     * @return $this
     */
    public function __call(string $name, array $arguments)
    {
        $call = [];
        $call['function'] = $name;
        $call['arguments'] = $arguments;

        array_push($this->callStack, $call);

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        $class = get_called_class();
        $instance = new $class;

        // ~

        $callback = [$instance, $name];

        // ~

        return call_user_func_array($callback, $arguments);
    }

    // ~

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return $this
     */
    public function addMethodCall(string $name, array $arguments = [])
    {
        $call = [];
        $call['function'] = $name;
        $call['arguments'] = $arguments;

        array_push($this->callStack, $call);

        return $this;
    }

    /**
     * @param int|string $offset
     *
     * @return $this
     */
    public function addArrayAccess($offset)
    {
        $call = [];
        $call['function'] = 'arrayAccess';
        $call['arguments'] = [$offset];

        array_push($this->callStack, $call);

        return $this;
    }

    // ~

    /**
     * @param int $pointer
     */
    private function setPointer($pointer)
    {
        $this->pointer = $pointer;
    }

    /**
     * @return int
     */
    public function getPointer()
    {
        return $this->pointer;
    }

    /**
     * Dummy method
     *
     * @param mixed $offset
     * @param mixed $value
     *
     * @return null
     */
    public function offsetSet($offset, $value)
    {
        return null;
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return (is_array($this->object) && isset($this->object[$offset]));
    }

    /**
     * Dummy method
     *
     * @param mixed $offset
     *
     * @return null
     */
    public function offsetUnset($offset)
    {
        return null;
    }

    /**
     * @param mixed $offset
     *
     * @return $this
     */
    public function offsetGet($offset)
    {
        $this->arrayAccess($offset);

        return $this;
    }

    // ~

    /**
     * @return $this
     */
    public function clear()
    {
        $this->callStack = [];

        return $this;
    }

    /**
     * @param array $callStack
     *
     * @return $this
     */
    public function setCallStack(array $callStack)
    {
        $this->callStack = array_values($callStack);

        return $this;
    }

    /**
     * @return array
     */
    public function getCallStack()
    {
        return array_values($this->callStack);
    }

    /**
     * @return string
     */
    public function printCallStack()
    {
        $callStack = [];

        foreach ($this->callStack as $call) {
            if ($call['function'] === 'arrayAccess') {
                $callStack[] = sprintf('[%s]', $this->argumentsToString($call['arguments']));
            } else {
                $callStack[] = sprintf('%s(%s)', $call['function'], $this->argumentsToString($call['arguments']));
            }
        }

        return implode('->', $callStack);
    }

    /**
     * Parses arguments array to string
     *
     * @param array $arguments
     *
     * @return string
     */
    protected function argumentsToString(array $arguments)
    {
        array_walk($arguments, function (&$elem) {
            if (is_array($elem)) {
                $elem = sprintf('array(%s)', count($elem));
            } else {
                if (is_object($elem)) {
                    $elem = sprintf('object(%s)', get_class($elem));
                } else {
                    $elem = (string)$elem;
                }
            }
        });

        return implode(', ', $arguments);
    }

    /**
     * @param mixed $object
     *
     * @throws ExecutorArrayAccessOnNonArrayException
     * @throws ExecutorMethodNotImplementedException
     * @throws \Exception
     *
     * @return array|mixed|null
     */
    public function execute($object)
    {
        $this->object = $object;

        // ~

        /* Main loop */
        foreach ($this->callStack as $pointer => $call) {
            $this->setPointer($pointer);

            /* Call on array */
            if ($call['function'] === 'arrayAccess') {
                if (is_array($this->object)) {
                    $key = $call['arguments'][0];
                    if (array_key_exists($key, $this->object)) {
                        $this->object = $this->object[$key];
                        continue;
                    }

                    return $this->executeArrayObject();
                }

                throw new ExecutorArrayAccessOnNonArrayException(sprintf(
                    'Trying to get array access on %s with key: "%s"!',
                    (is_object($this->object))
                            ? sprintf('"%s" object', get_class($this->object))
                            : sprintf('%s(%s)', gettype($this->object), (string)$this->object),
                    @$call['arguments'][0]
                ));
            }
            /* Call on object */
            if (method_exists($this->object, $call['function'])) {
                $this->object = call_user_func_array([$this->object, $call['function']], $call['arguments']);
            } else {
                /* Need to split recursively */
                if ($this->object instanceof \Traversable) {
                    return $this->executeArrayObject();
                }

                if (gettype($this->object) === 'object') {
                    throw new ExecutorMethodNotImplementedException(sprintf(
                        'Method "%s" is not implemented in object of class "%s"!',
                        $call['function'],
                        get_class($this->object)
                    ));
                }
                if (false == (null === $this->object)) {
                    throw new ExecutorMethodNotImplementedException(sprintf(
                        'Trying to call method "%s" on "%s" variable!',
                        $call['function'],
                        gettype($this->object)
                    ));
                }
            }
        }

        // ~

        return $this->object;
    }

    /**
     * @throws \Exception
     *
     * @return array
     */
    protected function executeArrayObject()
    {
        $arrayResult = [];

        foreach ($this->object as $collectionItem) {
            $itemExecutor = new self($this);
            $result = $itemExecutor->execute($collectionItem);
            array_push($arrayResult, $result);
        }

        return $arrayResult;
    }
}
