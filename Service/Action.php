<?php

namespace TableBundle\Service;

use Closure;
use Doctrine\Common\Collections\ArrayCollection;
use MainBundle\Interfaces\IconsInterface;
use TableBundle\Exception\ActionException;

/**
 * Action class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class Action implements IconsInterface
{
    /** @var string */
    public const ACTION_TYPE_LINK = 'link';

    /** @var string */
    public const ACTION_TYPE_COMPONENT = 'component';

    /** @var string */
    public const ACTION_TARGET_TOP = '_top';

    /** @var string */
    public const ACTION_TARGET_SELF = '_self';

    /** @var string */
    public const ACTION_TARGET_BLANK = '_blank';

    /** @var string */
    public const ACTION_TARGET_PARENT = '_parent';

    /** @var string */
    public const ACTION_TARGET_DEFAULT = self::ACTION_TARGET_SELF;

    /** @var string */
    private $name;

    /** @var string */
    private $componentName = 'Action';

    /** @var string|null */
    private $label;

    /** @var string|null */
    private $class;

    /** @var string */
    private $icon = IconsInterface::ICON_GEARS;

    /** @var bool */
    private $visible = true;

    /** @var bool */
    private $enabled = true;

    /** @var string|null */
    private $role;

    /** @var string|null */
    private $route;

    /** @var array */
    private $parameters = [];

    /** @var string|null */
    private $target;

    /** @var string */
    private $type = self::ACTION_TYPE_LINK;

    /** @var array */
    private $componentAttributes = [];

    /** @var Action[]|ArrayCollection */
    private $privilegedAlternatives;

    /** @var Action[]|ArrayCollection */
    private $unprivilegedAlternatives;

    /** @var Closure|null */
    private $privilegedCondition;

    /** @var Closure|null */
    private $unprivilegedCondition;

    /** @var Closure|null */
    private $actionCondition;

    /** @var int */
    private $priority = 100;

    /**
     * Constructor
     *
     * @param string $name
     *
     * @throws ActionException
     */
    public function __construct(string $name)
    {
        $this->setName($name);

        $this->privilegedAlternatives = new ArrayCollection();
        $this->unprivilegedAlternatives = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string $target
     *
     * @return $this
     */
    public function setTarget(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     *
     * @return $this
     */
    public function setPriority(int $priority): self
    {
        $this->priority = \max($priority, 0);

        return $this;
    }

    /**
     * Check if action may be alternative in case of granted permission
     * If no check-callback given consider action as valid alternative
     *
     * @param mixed $object
     *
     * @return bool
     */
    public function checkPrivilegedCondition($object): bool
    {
        $privilegedCondition = $this->privilegedCondition;

        if (null !== $privilegedCondition) {
            return (bool)$privilegedCondition($object);
        }

        return true;
    }

    /**
     * @param Closure $privilegedCondition
     *
     * @return $this
     */
    public function setPrivilegedCondition(Closure $privilegedCondition): self
    {
        $this->privilegedCondition = $privilegedCondition;

        return $this;
    }

    /**
     * Check if action may be alternative in case of non-granted permission
     * If no check-callback given consider action as valid alternative
     *
     * @param mixed $object
     *
     * @return bool
     */
    public function checkUnprivilegedCondition($object): bool
    {
        $unprivilegedCondition = $this->unprivilegedCondition;

        if (null !== $unprivilegedCondition) {
            return (bool)$unprivilegedCondition($object);
        }

        return true;
    }

    /**
     * @param Closure $unprivilegedCondition
     *
     * @return $this
     */
    public function setUnprivilegedCondition(Closure $unprivilegedCondition): self
    {
        $this->unprivilegedCondition = $unprivilegedCondition;

        return $this;
    }

    /**
     * Check if action can be used (enabled)
     * If no check-callback given get isEnabled() value
     *
     * @param mixed $object
     * @param int|string $rowIndex
     *
     * @return bool
     */
    public function checkActionCondition($object, $rowIndex): bool
    {
        $actionCondition = $this->actionCondition;

        if (null !== $actionCondition) {
            return (bool)$actionCondition($object, $rowIndex);
        }

        return $this->isEnabled();
    }

    /**
     * @param Closure $actionCondition
     *
     * @return $this
     */
    public function setActionCondition(Closure $actionCondition): self
    {
        $this->actionCondition = $actionCondition;

        return $this;
    }

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function addPrivilegedAlternative(self $action): self
    {
        $this->privilegedAlternatives->set($action->getName(), $action);

        return $this;
    }

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function removePrivilegedAlternative(self $action): self
    {
        $this->privilegedAlternatives->removeElement($action);

        return $this;
    }

    /**
     * @param string $actionName
     *
     * @return $this
     */
    public function removePrivilegedAlternativeByName(string $actionName): self
    {
        $this->privilegedAlternatives->remove($actionName);

        return $this;
    }

    /**
     * @param string|null $actionName
     *
     * @return Action|Action[]|ArrayCollection|null
     */
    public function getPrivilegedAlternatives(string $actionName = null)
    {
        if (null !== $actionName) {
            return $this->privilegedAlternatives->get($actionName);
        }

        return $this->privilegedAlternatives;
    }

    // ~

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function addUnprivilegedAlternative(self $action): self
    {
        $this->unprivilegedAlternatives->set($action->getName(), $action);

        return $this;
    }

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function removeUnprivilegedAlternative(self $action): self
    {
        $this->unprivilegedAlternatives->removeElement($action);

        return $this;
    }

    /**
     * @param string $actionName
     *
     * @return $this
     */
    public function removeUnprivilegedAlternativeByName(string $actionName): self
    {
        $this->unprivilegedAlternatives->remove($actionName);

        return $this;
    }

    /**
     * @param string|null $actionName
     *
     * @return Action|Action[]|ArrayCollection|null
     */
    public function getUnprivilegedAlternatives(string $actionName = null)
    {
        if (null !== $actionName) {
            return $this->unprivilegedAlternatives->get($actionName);
        }

        return $this->unprivilegedAlternatives;
    }

    // ~

    /**
     * @return array
     */
    public function getComponentAttributes(): array
    {
        return $this->componentAttributes;
    }

    /**
     * @param array $componentAttributes
     *
     * @return Action
     */
    public function setComponentAttributes(array $componentAttributes): self
    {
        $this->componentAttributes = $componentAttributes;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string $route #Route
     *
     * @return $this
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @throws ActionException
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        if ('' === $name) {
            throw new ActionException('Action name must be non-empty string!');
        }

        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return $this->componentName;
    }

    /**
     * @param string $componentName
     *
     * @return Action
     */
    public function setComponentName(string $componentName): self
    {
        $this->componentName = $componentName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string $class
     *
     * @return $this
     */
    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @return $this
     */
    public function showAction(): self
    {
        $this->visible = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function hideAction(): self
    {
        $this->visible = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return $this
     */
    public function enableAction(): self
    {
        $this->enabled = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disableAction(): self
    {
        $this->enabled = false;

        return $this;
    }

    /**
     * @param string $name
     * @param Executor|null $executor
     *
     * @return Executor
     */
    public function addParameter(string $name, Executor $executor = null): Executor
    {
        if (null === $executor) {
            $this->parameters[$name] = new Executor();
        }

        return $this->parameters[$name];
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @return $this
     */
    public function addStaticParameter(string $name, $value): self
    {
        $this->parameters[$name] = $value;

        return $this;
    }

    /**
     * @param mixed $item
     *
     * @return Action|null
     */
    protected function resolvePrivilegedAlternative($item): ?self
    {
        foreach ($this->getPrivilegedAlternatives() as $alternative) {
            /** @var Action $alternative */
            if ($alternative->checkPrivilegedCondition($item)) {
                return $alternative;
            }
        }

        return null;
    }

    /**
     * @param mixed $item
     *
     * @return Action|null
     */
    protected function resolveUnprivilegedAlternative($item): ?self
    {
        foreach ($this->getUnprivilegedAlternatives() as $alternative) {
            /** @var Action $alternative */
            if ($alternative->checkUnprivilegedCondition($item)) {
                return $alternative;
            }
        }

        return null;
    }

    /**
     * @param mixed $item
     * @param int|string $rowIndex
     * @param bool $recursive
     *
     * @throws \Exception
     *
     * @return array
     */
    public function generate($item, $rowIndex, bool $recursive = true): array
    {
        $privilegedAlternative = null;
        $unprivilegedAlternative = null;

        if ($recursive) {
            $privilegedAlternative = $this->resolvePrivilegedAlternative($item);
            $unprivilegedAlternative = $this->resolveUnprivilegedAlternative($item);
        }

        $actionData = [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'class' => $this->getClass(),
            'icon' => $this->getIcon(),
            'component' => $this->getComponentName(),
            'enabled' => $this->checkActionCondition($item, $rowIndex),
            'role' => $this->getRole(),
            'visible' => $this->isVisible(),
            'target' => $this->getTarget(),
            'type' => $this->getType(),
            'route' => $this->getRoute(),
            'routeParameters' => [],
            'priority' => $this->getPriority(),
            'componentAttributes' => $this->getComponentAttributes(),
            'privilegedAlternative' => null === $privilegedAlternative
                ? null
                : $privilegedAlternative->generate($item, $rowIndex, false),
            'unprivilegedAlternative' => null === $unprivilegedAlternative
                ? null
                : $unprivilegedAlternative->generate($item, $rowIndex, false),
        ];

        foreach ($this->parameters as $name => $parameter) {
            if ($parameter instanceof Executor) {
                $actionData['routeParameters'][$name] = $parameter->execute($item);
            } else {
                $actionData['routeParameters'][$name] = $parameter;
            }
        }

        return $actionData;
    }
}
