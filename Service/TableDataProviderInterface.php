<?php

declare(strict_types=1);

namespace TableBundle\Service;

use Countable;
use IteratorAggregate;
use TableBundle\Service\Model\Criterion;

/**
 * TableDataProviderInterface interface.
 *
 * @author Kamil Gnidziński <kamil.gnidzinski@gogomedia.pl>
 */
interface TableDataProviderInterface extends Countable, IteratorAggregate
{
    /**
     * @param array|null $order
     *
     * @return $this
     */
    public function setOrder(?array $order): self;

    /**
     * @param Criterion[] $criteria
     *
     * @return $this
     */
    public function setCriteria(array $criteria): self;

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    public function setLimit(?int $limit): self;

    /**
     * @param int|null $offset
     *
     * @return $this
     */
    public function setOffset(?int $offset): self;
}
