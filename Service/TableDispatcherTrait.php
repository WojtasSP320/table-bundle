<?php

namespace TableBundle\Service;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TableBundle\Events\TableEvent\PostGenerateEvent;
use TableBundle\Events\TableEvent\PostSetCriteriaEvent;
use TableBundle\Events\TableEvent\PostSetDataEvent;
use TableBundle\Events\TableEvent\PostSetPaginationEvent;
use TableBundle\Events\TableEvent\PostSetSortingEvent;
use TableBundle\Events\TableEvent\PreSetCriteriaEvent;
use TableBundle\Events\TableEvent\PreSetDataEvent;
use TableBundle\Events\TableEvent\PreSetPaginationEvent;
use TableBundle\Events\TableEvent\PreSetSortingEvent;

/**
 * TableDispatcherTrait class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
trait TableDispatcherTrait
{
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return $this
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): self
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    /**
     * @param array $criteria
     *
     * @return void
     */
    protected function dispatchPreSetCriteriaEvent(array $criteria): void
    {
        //$event = new PreSetCriteriaEvent();
        //$event
        //    ->setTable($this)
        //    ->setCriteria($criteria);
        //
        //$this->eventDispatcher->dispatch$event);
    }

    /**
     * @param array $criteria
     *
     * @return void
     */
    protected function dispatchPostSetCriteriaEvent(array $criteria): void
    {
        $event = new PostSetCriteriaEvent();
        $event
            ->setTable($this)
            ->setCriteria($criteria);

        $this->eventDispatcher->dispatch($event);
    }

    // ~

    /**
     * @param iterable $list
     *
     * @return void
     */
    protected function dispatchPreSetDataEvent(iterable $list): void
    {
        //$event = new PreSetDataEvent();
        //$event
        //    ->setTable($this)
        //    ->setList($list);
        //
        //$this->eventDispatcher->dispatch(TableInterface::PRE_SET_DATA, $event);
    }

    /**
     * @param iterable $list
     *
     * @return void
     */
    protected function dispatchPostSetDataEvent(iterable $list): void
    {
        //    $event = new PostSetDataEvent();
    //    $event
    //        ->setTable($this)
    //        ->setList($list);
    //
    //    $this->eventDispatcher->dispatch(TableInterface::POST_SET_DATA, $event);
    }

    // ~

    /**
     * @param array|string $column
     * @param array|string $direction
     *
     * @return void
     */
    protected function dispatchPreSetSortingEvent($column, $direction): void
    {
        //$event = new PreSetSortingEvent();
        //$event
        //    ->setTable($this)
        //    ->setColumn($column)
        //    ->setDirection($direction);
        //
        //$this->eventDispatcher->dispatch(TableInterface::PRE_SET_SORTING, $event);
    }

    /**
     * @param array|string $column
     * @param array|string $direction
     *
     * @return void
     */
    protected function dispatchPostSetSortingEvent($column, $direction): void
    {
        //$event = new PostSetSortingEvent();
        //$event
        //    ->setTable($this)
        //    ->setColumn($column)
        //    ->setDirection($direction);
        //
        //$this->eventDispatcher->dispatch(TableInterface::POST_SET_SORTING, $event);
    }

    // ~

    /**
     * @param int $page
     * @param int|null $limit
     *
     * @return void
     */
    protected function dispatchPreSetPaginationEvent(int $page, ?int $limit): void
    {
        //$event = new PreSetPaginationEvent();
        //$event
        //    ->setTable($this)
        //    ->setPage($page)
        //    ->setLimit($limit);
        //
        //$this->eventDispatcher->dispatch(TableInterface::PRE_SET_PAGINATION, $event);
    }

    /**
     * @param int|null $page
     * @param int|null $limit
     *
     * @return void
     */
    protected function dispatchPostSetPaginationEvent(?int $page, ?int $limit): void
    {
        //$event = new PostSetPaginationEvent();
        //$event
        //    ->setTable($this)
        //    ->setPage($page)
        //    ->setLimit($limit);
        //
        //$this->eventDispatcher->dispatch(TableInterface::POST_SET_PAGINATION, $event);
    }

    /**
     * @param array $tableData
     *
     * @return void
     */
    protected function dispatchPostGenerateEvent(array $tableData): void
    {
        ///** @var TableInterface $this */
        //$sortingData = $this->getSortingData();
        //
        //$column = $sortingData['column'];
        //$direction = $sortingData['direction'];
        //
        //$page = $this->isPaginationEnabled()
        //    ? $this->getTablePaginator()->getCurrentPage()
        //    : 1
        //;
        //
        //$limit = $this->isPaginationEnabled()
        //    ? $this->getTablePaginator()->getLimit()
        //    : null
        //;
        //
        //$criteria = $this->getFiltersData();
        //
        //// ~
        //
        //$event = new PostGenerateEvent();
        //$event
        //    ->setTable($this)
        //    ->setPage($page)
        //    ->setLimit($limit)
        //    ->setColumn($column)
        //    ->setDirection($direction)
        //    ->setCriteria($criteria);
        //
        //$this->eventDispatcher->dispatch(TableInterface::POST_GENERATE, $event);
    }
}
