<?php

namespace TableBundle\Service;

use DanlineBundle\Services\SettingsManager\SettingsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\Cell\CustomField;
use TableBundle\Service\Cell\PriceField;
use TableBundle\Service\Cell\SimpleTransField;

/**
 * CellFactory class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class CellFactory
{
    /** @var ContainerInterface */
    private $container;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * CellFactory constructor.
     *
     * @param ContainerInterface $container
     * @param TranslatorInterface $translator
     */
    public function __construct(ContainerInterface $container, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->translator = $translator;
    }

    /**
     * @param string $class
     * @param array $options
     * @param array $tableOptions
     *
     * @throws ColumnException
     * @throws \ReflectionException
     *
     * @return CellInterface
     */
    public function createCell(string $class, array $options = [], array $tableOptions = []): CellInterface
    {
        $reflection = new \ReflectionClass($class);

        if ($reflection->implementsInterface(CellInterface::class)) {
            /** @var CellInterface $cell */
            $cell = $reflection->newInstance($options);
        } else {
            throw new ColumnException('CellFactory: given cell is not Cell class name!');
        }

        $this->handleCell($cell, $tableOptions);

        return $cell;
    }

    /**
     * @param CellInterface $cell
     * @param array $tableOptions
     */
    protected function handleCell(CellInterface $cell, array $tableOptions): void
    {
        switch (true) {
            case $cell instanceof PriceField:
                $this->handlePriceField($cell);
                break;
            case $cell instanceof SimpleTransField:
                $this->handleSimpleTransField($cell);
                break;
            case $cell instanceof CustomField:
                $this->handleCustomField($cell);
                break;
            default:
        }
    }

    /**
     * @param PriceField $cell
     *
     * @return $this
     */
    protected function handlePriceField(PriceField $cell): self
    {
        /** @var SettingsManager $settingsManager */
        $settingsManager = $this->container->get(SettingsManager::class);

        $decimalDigits = $settingsManager->getDefaultDecimalDigits();
        $decimalPoint = $settingsManager->getDefaultDecimalPoint();
        $thousandSeparator = $settingsManager->getDefaultThousandSeparator();
        $currency = $settingsManager->getDefaultCurrency();

        $options = [
            'currency' => $currency,
            'thousands_separator' => $thousandSeparator,
            'decimals_point' => $decimalPoint,
            'decimal_digits' => $decimalDigits,
        ];

        $cell->setOptions($options);

        return $this;
    }

    /**
     * @param SimpleTransField $cell
     *
     * @return $this
     */
    protected function handleSimpleTransField(SimpleTransField $cell): self
    {
        $cell->setTranslator($this->translator);

        $options = [
            'trans_domain' => null,
            'locale' => null,
        ];

        $cell->setOptions($options);

        return $this;
    }

    protected function handleCustomField(CustomField $cell): self
    {
        $serializer = $this->container->get('jms_serializer');

        $cell->setSerializer($serializer);

        return $this;
    }
}
