<?php

namespace TableBundle\Service;

use Closure;
use TableBundle\Exception\ColumnException;
use TableBundle\Exception\ExecutorArrayAccessOnNonArrayException;
use TableBundle\Exception\ExecutorCallOnNullException;
use TableBundle\Exception\ExecutorMethodNotImplementedException;
use TableBundle\Exception\FilterException;
use TableBundle\Exception\TableException;
use TableBundle\Service\Cell\CustomField;

/**
 * Column class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class Column
{
    /** @var string */
    public const ALIGN_LEFT = 'column-align-left';

    /** @var string */
    public const ALIGN_CENTER = 'column-align-center';

    /** @var string */
    public const ALIGN_RIGHT = 'column-align-right';

    /** @var string */
    public const ALIGN_JUSTIFY = 'column-align-justify';

    // ~

    /** @var string */
    public const COLUMN_WIDTH_AUTO = 'column-width-auto';

    /** @var string */
    public const COLUMN_WIDTH_ID = 'column-width-id';

    /** @var string */
    public const COLUMN_WIDTH_ORDER_NO = 'column-width-order-no';

    /** @var string */
    public const COLUMN_WIDTH_CHECKBOX = 'column-width-checkbox';

    /** @var string */
    public const COLUMN_WIDTH_40 = 'column-width-40';

    /** @var string */
    public const COLUMN_WIDTH_60 = 'column-width-60';

    /** @var string */
    public const COLUMN_WIDTH_80 = 'column-width-80';

    /** @var string */
    public const COLUMN_WIDTH_100 = 'column-width-100';

    /** @var string */
    public const COLUMN_WIDTH_120 = 'column-width-120';

    /** @var string */
    public const COLUMN_WIDTH_140 = 'column-width-140';

    /** @var string */
    public const COLUMN_WIDTH_160 = 'column-width-160';

    /** @var string */
    public const COLUMN_WIDTH_180 = 'column-width-180';

    /** @var string */
    public const COLUMN_WIDTH_200 = 'column-width-200';

    /** @var string */
    public const COLUMN_WIDTH_220 = 'column-width-220';

    /** @var string */
    public const COLUMN_WIDTH_240 = 'column-width-240';

    /** @var string */
    public const COLUMN_WIDTH_260 = 'column-width-260';

    /** @var string */
    public const COLUMN_WIDTH_280 = 'column-width-280';

    /** @var string */
    public const COLUMN_WIDTH_320 = 'column-width-320';

    // ~

    /** @var string|null */
    private $name;

    /** @var string */
    private $label = '';

    /** @var string|null */
    private $class;

    /** @var string */
    private $align = self::ALIGN_LEFT;

    /** @var bool */
    private $visible = true;

    /** @var string|null */
    private $width;

    /** @var Executor|null */
    private $field;

    /** @var CellInterface|null */
    private $cell;

    /** @var bool */
    private $sortingEnabled = true;

    /** @var string|null */
    private $sortingDirection;

    /** @var FilterAbstract|FilterInterface|null */
    private $filter;

    /** @var TableInterface */
    private $table;

    /** @var bool */
    private $hidden = false;

    /** @var Closure|null */
    private $closureField;

    /**
     * Column public constructor.
     *
     * @param TableInterface $table
     */
    public function __construct(TableInterface $table)
    {
        $this->setTable($table);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Column
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return Column
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string $class
     *
     * @return Column
     */
    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlign(): string
    {
        return $this->align;
    }

    /**
     * @param string $align
     *
     * @return Column
     */
    public function setAlign(string $align): self
    {
        $this->align = $align;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     *
     * @return Column
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWidth(): ?string
    {
        return $this->width;
    }

    /**
     * @param string $width
     *
     * @return Column
     */
    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return CellInterface|null
     */
    public function getCell(): ?CellInterface
    {
        return $this->cell;
    }

    /**
     * @param CellInterface $cell
     *
     * @return Column
     */
    public function setCell(CellInterface $cell): self
    {
        $this->cell = $cell;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSortingEnabled(): bool
    {
        return $this->sortingEnabled;
    }

    /**
     * @return Column
     */
    public function disableSorting(): self
    {
        $this->sortingEnabled = false;

        return $this;
    }

    /**
     * @return Column
     */
    public function enableSorting(): self
    {
        $this->sortingEnabled = true;

        return $this;
    }

    ///**
    // * Returns true if table sorting is enabled and column sorting is enabled
    // *
    // * @return bool
    // */
    //public function isSortingEnabled()
    //{
    //    $tableSortingEnabled = $this->getTable()->isSortingEnabled();
    //    $columnSortingEnabled = $this->sortingEnabled;
    //
    //    return ($tableSortingEnabled && $columnSortingEnabled);
    //}

    /**
     * @param string $sortingDirection
     *
     * @throws ColumnException
     *
     * @return $this
     */
    public function setSortingDirection(string $sortingDirection): self
    {
        if (!\in_array(\strtolower($sortingDirection), [TableSorter::DIRECTION_ASC, TableSorter::DIRECTION_DESC], true)) {
            throw new ColumnException('Sorting direction must be either "asc" or "desc", "%s" given!', [
                $sortingDirection,
            ]);
        }

        if (!$this->isSortingEnabled()) {
            throw new ColumnException('Trying to set sorting direction on column with sorting disabled!');
        }

        $this->sortingDirection = $sortingDirection;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSortingDirection(): ?string
    {
        return $this->sortingDirection;
    }

    /**
     * @return Column
     */
    public function clearSortingDirection(): self
    {
        $this->sortingDirection = null;

        return $this;
    }

    /**
     * @return FilterAbstract|null
     */
    public function getFilter(): ?FilterAbstract
    {
        return $this->filter;
    }

    /**
     * @return TableInterface
     */
    public function getTable(): TableInterface
    {
        return $this->table;
    }

    /**
     * @param TableInterface $table
     *
     * @return Column
     */
    public function setTable(TableInterface $table): self
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     *
     * @return Column
     */
    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * @param mixed $item
     * @param string $columnName
     *
     * @throws ExecutorMethodNotImplementedException
     * @throws ExecutorArrayAccessOnNonArrayException
     *
     * @return array|null
     */
    public function getData($item, string $columnName): ?array
    {
        $rawData = null;

        //todo: move columnName out

        try {
            if ($this->field instanceof Executor) {
                /* Executor item getter */
                $rawData = $this->field->execute($item);
            } elseif (null !== $this->getClosureField()) {
                /* Closure item getter */
                $callback = $this->getClosureField();
                $rawData = $callback($item);
            } else {
                /* No executor neither closure getter was set */
                return null;
            }
        } catch (ExecutorCallOnNullException $exception) {
            /* Do nothing */
        } catch (ExecutorMethodNotImplementedException $exception) {
            $message = sprintf('%s in %s column!', $exception->getMessage(), $columnName);

            throw new ExecutorMethodNotImplementedException($message);
        } catch (ExecutorArrayAccessOnNonArrayException $exception) {
            $message = sprintf('%s in %s column!', $exception->getMessage(), $columnName);

            throw new ExecutorArrayAccessOnNonArrayException($message);
        }

        $viewData = $this->getCell()->getViewData($rawData);

        $data = [
            'viewData' => $viewData,
            'rawData' => $rawData,
        ];

        return $data;
    }

    /**
     * @return array
     */
    public function generate(): array
    {
        return [
            'label' => $this->getLabel(),
            'class' => $this->getClass(),
            'align' => $this->getAlign(),
            'type' => $this->getType(),
            'width' => $this->getWidth(),
            'visible' => $this->isVisible(),
            'hidden' => $this->isHidden(),
            'filter' => $this->getFilter()->toArray(),
            'component' => $this->getComponentName(),
            'componentAttributes' => $this->getComponentAttributes(),
            'sortingEnabled' => $this->isSortingEnabled(),
            'sortingDirection' => $this->getSortingDirection(),
            'isCurrentlySorted' => $this->isCurrentlySorted(),
        ];
    }

    /**
     * @param Closure|null $closure
     *
     * @throws ColumnException
     * @throws \ReflectionException
     */
    protected function validateClosureFieldHandler(Closure $closure = null): void
    {
        if (null !== $closure) {
            $reflectionFunction = new \ReflectionFunction($closure);

            $parametersNumber = $reflectionFunction->getNumberOfParameters();

            if ($parametersNumber !== 1) {
                throw new ColumnException('Closure field callback must have exactly one parameter!');
            }

            $parameter = $reflectionFunction->getParameters()[0];

            if ($parameter->isPassedByReference()) {
                throw new ColumnException('Closure field callback parameter cannot be passed by reference!');
            }
        }
    }

    /**
     * Checks if filter with given name already exists in table
     *
     * @param FilterInterface $filter
     *
     * @return bool
     */
    public function filterNameExists(FilterInterface $filter): bool
    {
        $table = $this->getTable();
        $filterName = $filter->getName();

        return (null !== $table->getFilter($filterName));
    }

    /**
     * Proxy method for retrieving filters name
     *
     * @return string|null
     */
    public function getFilterName(): ?string
    {
        if (null !== $this->getFilter()) {
            return $this->getFilter()->getName();
        }

        return null;
    }

    /**
     * @param string $class
     * @param array $options
     * @param array $tableOptions
     *
     * @throws ColumnException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function createCell(string $class, array $options = [], array $tableOptions = []): self
    {
        if (null !== ($cellFactory = $this->getTable()->getCellFactory())) {
            $cell = $cellFactory->createCell($class, $options, $tableOptions);

            $this->setCell($cell);
        }

        return $this;
    }

    /**
     * @param string $class
     * @param string $name
     * @param array $attributes
     *
     * @throws ColumnException
     * @throws FilterException
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function createFilter(string $class, string $name, array $attributes = []): self
    {
        if (null !== ($filterFactory = $this->getTable()->getFilterFactory())) {
            $filter = $filterFactory->createFilter($class, $name, $attributes);

            $this->setFilter($filter);
        }

        return $this;
    }

    /**
     * @param FilterInterface $filter
     *
     * @throws ColumnException
     * @throws TableException
     *
     * @return $this
     */
    public function setFilter(FilterInterface $filter): self
    {
        $oldName = null;

        if (null !== $this->getFilter()) {
            $oldName = $this->getFilter()->getName();
        }

        if (
            $this->filterNameExists($filter) &&
            $filter->getName() !== $oldName
        ) {
            throw new ColumnException('Filter with name "%s" already exists in table!', [
                $filter->getName(),
            ]);
        }

        $this->filter = $filter;

        $filter->setColumn($this);

        $this->getTable()->setFilter($filter, $oldName);

        return $this;
    }

    /**
     * Returns true if table sorting is enabled or forced and column sorting is enabled
     *
     * @return bool
     */
    public function isSortingUsed(): bool
    {
        $tableSortingUsed = $this->getTable()->isSortingUsed();
        $columnSortingEnabled = $this->sortingEnabled;

        return ($tableSortingUsed && $columnSortingEnabled);
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        if (null !== $this->getCell()) {
            return $this->getCell()->getType();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isCurrentlySorted(): bool
    {
        return (null !== $this->getSortingDirection());
    }

    /**
     * @return string|null
     */
    public function getComponentName(): ?string
    {
        if (null !== $this->getCell()) {
            return $this->getCell()->getComponentName();
        }

        return null;
    }

    /**
     * @return array
     */
    public function getComponentAttributes(): array
    {
        if (null !== $this->getCell()) {
            $options = $this->getCell()->getOptions();

            if ($this->cell instanceof CustomField) {
                $options = $options['componentAttributes'];
            }

            return $options;
        }

        return [];
    }

    /**
     * @param Executor $executor
     *
     * @return Executor
     */
    public function setField(Executor $executor = null): Executor
    {
        if (null === $executor) {
            $this->field = new Executor();
        }

        return $this->field;
    }

    /**
     * @param Closure|null $closureField
     *
     * @throws ColumnException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function setClosureField(Closure $closureField = null): self
    {
        $this->validateClosureFieldHandler($closureField);

        $this->closureField = $closureField;

        return $this;
    }

    /**
     * @return callable|null
     */
    public function getClosureField(): ?callable
    {
        return $this->closureField;
    }

    /**
     * @param string $componentName
     *
     * @throws ColumnException
     *
     * @return $this
     */
    public function setComponentName(string $componentName): self
    {
        if (null === $this->getCell()) {
            $message = \sprintf('You must create cell before setting component name in column "%s"!', $this->name);

            throw new ColumnException($message);
        }

        if (false === $this->getCell() instanceof CustomField) {
            $message = \sprintf('Specifying components name is allowed only in CustomField in column "%s"!', $this->name);

            throw new ColumnException($message);
        }
        
        $this->getCell()->setComponentName($componentName);

        return $this;
    }
}
