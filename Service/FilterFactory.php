<?php

namespace TableBundle\Service;

use DanlineBundle\Services\SettingsManager\SettingsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TableBundle\Exception\FilterException;
use TableBundle\Service\Filter\EntityFilter;
use TableBundle\Service\Filter\MoneyFilter;
use TableBundle\Service\Filter\NumberRangeFilter;

/**
 * FilterFactory class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class FilterFactory
{
    /** @var ContainerInterface */
    private $container;

    /**
     * FilterFactory constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $class
     * @param string $name
     * @param array $attributes
     *
     * @throws FilterException
     * @throws \ReflectionException
     *
     * @return FilterInterface
     */
    public function createFilter(string $class, string $name, array $attributes = []): FilterInterface
    {
        $reflection = new \ReflectionClass($class);

        if ($reflection->implementsInterface(FilterInterface::class)) {
            /** @var FilterInterface $filter */
            $filter = $reflection->newInstance($name, $attributes);
        } else {
            throw new FilterException('FilterFactory: given filter is not Filter class name!', []);
        }

        $this->handleFilter($filter);

        return $filter;
    }

    /**
     * @param FilterInterface $filter
     */
    protected function handleFilter(FilterInterface $filter): void
    {
        if ($filter instanceof EntityFilter) {
            $this->handleEntityFilter($filter);
        }

        if ($filter instanceof NumberRangeFilter) {
            $this->handleNumberRangeFilter($filter);
        }
    }

    /**
     * @param EntityFilter $filter
     */
    protected function handleEntityFilter(EntityFilter $filter): void
    {
        $doctrine = $this->container->get('doctrine');

        $filter->setDoctrine($doctrine);
    }

    /**
     * @param NumberRangeFilter $filter
     */
    protected function handleNumberRangeFilter(NumberRangeFilter $filter): void
    {
        $settingsManager = $this->container->get(SettingsManager::class);

        $decimalDigits = $settingsManager->getDefaultDecimalDigits();
        $decimalPoint = $settingsManager->getDefaultDecimalPoint();
        $thousandSeparator = $settingsManager->getDefaultThousandSeparator();

        $filter
            ->setDecimalDigits($decimalDigits)
            ->setDecimalPoint($decimalPoint)
            ->setThousandSeparator($thousandSeparator)
        ;
    }

    /**
     * @param MoneyFilter $filter
     */
    protected function handleMoneyFilter(MoneyFilter $filter): void
    {
        $settingsManager = $this->container->get(SettingsManager::class);

        $currency = $settingsManager->getDefaultCurrency();

        $filter->setDefaultCurrency($currency);
    }
}
