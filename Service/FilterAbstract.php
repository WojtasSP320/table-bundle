<?php

namespace TableBundle\Service;

use Closure;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\FilterException;
use TableBundle\Service\Filter\NoneFilter;

/**
 * FilterAbstract class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
abstract class FilterAbstract implements FilterInterface
{
    /** @var string */
    protected $name;

    /** @var mixed */
    protected $value;

    /** @var array */
    protected $attributes = [];

    /** @var TableInterface */
    protected $table;

    /** @var Column|null */
    protected $column;

    /** @var Closure|null */
    protected $modelTransformer;

    /** @var Closure|null */
    protected $modelReverseTransformer;

    /** @var Closure|null */
    protected $viewTransformer;

    /** @var Closure|null */
    protected $viewReverseTransformer;

    /** @var OptionsResolver|null */
    protected $optionsResolver;

    /** @var bool */
    protected $filterArrayTypeData = false;

    /**
     * FilterAbstract public constructor.
     *
     * @param string $name
     * @param array $attributes
     */
    public function __construct(string $name, array $attributes = [])
    {
        $optionsResolver = new OptionsResolver();

        $this->name = $name;

        $this->configureOptionsResolver($optionsResolver);
        $this->setOptionsResolver($optionsResolver);

        $this->setAttributes($attributes);
    }

    /**
     * @param TableInterface $table
     *
     * @return $this
     */
    public function setTable(TableInterface $table): self
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFilterArrayTypeData(): bool
    {
        return $this->filterArrayTypeData;
    }

    /**
     * @param bool $filterArrayTypeData
     *
     * @return $this
     */
    public function setFilterArrayTypeData(bool $filterArrayTypeData): self
    {
        $this->filterArrayTypeData = $filterArrayTypeData;

        return $this;
    }

    /**
     * @return OptionsResolver|null
     */
    public function getOptionsResolver(): ?OptionsResolver
    {
        return $this->optionsResolver;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function setOptionsResolver(OptionsResolver $optionsResolver): self
    {
        $this->optionsResolver = $optionsResolver;

        return $this;
    }

    /**
     * @param Closure|null $modelTransformer
     * @param Closure|null $modelReverseTransformer
     *
     * @return $this
     */
    public function setModelTransformer(Closure $modelTransformer = null, Closure $modelReverseTransformer = null): self
    {
        $this->modelTransformer = $modelTransformer;
        $this->modelReverseTransformer = $modelReverseTransformer;

        return $this;
    }

    /**
     * @param Closure|null $viewTransformer
     * @param Closure|null $viewReverseTransformer
     *
     * @return $this
     */
    public function setViewTransformer(Closure $viewTransformer = null, Closure $viewReverseTransformer = null): self
    {
        $this->viewTransformer = $viewTransformer;
        $this->viewReverseTransformer = $viewReverseTransformer;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return \get_called_class();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @throws FilterException
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        if (empty($name)) {
            throw new FilterException('Column filter name cannot be empty!');
        }

        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return $this
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     *
     * @return $this
     */
    public function setAttributes(array $attributes): self
    {
        try {
            $mergedAttributes = \array_merge($this->getAttributes(), $attributes);
            $attributes = $this->getOptionsResolver()->resolve($mergedAttributes);
            $this->attributes = $attributes;
        } catch (InvalidOptionsException $exception) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            $messageSuffix = sprintf("\n In \"%s\" filter options.", $this->getName());

            throw new InvalidOptionsException($message . $messageSuffix, $code, $exception);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearAttributes(): self
    {
        $this->setAttributes([]);

        return $this;
    }

    /**
     * @return Column|null
     */
    public function getColumn(): ?Column
    {
        return $this->column;
    }

    /**
     * @param Column $column
     *
     * @return $this
     */
    public function setColumn(Column $column): self
    {
        $this->column = $column;

        return $this;
    }

    /**
     * @return string
     */
    abstract public function getComponentName(): string;

    /**
     * Tries to find filter name in criteria array.
     * If $criteria[filter_name] has been found uses
     * its value for filter value.
     *
     * If $criteria[filter_name] is not present uses
     * default_value from options.
     *
     * @param array $criteria
     *
     * @throws FilterException
     *
     * @return $this
     */
    public function handleCriteria(array $criteria): self
    {
        if (!$this->isValidWithCriteriaMap()) {
            throw new FilterException('Criteria map is invalid for filter "%s"', [
                $this->getName(),
            ]);
        }

        $filterValue = $this->getAttributes()['default_value'];

        if (\array_key_exists($this->getName(), $criteria)) {
            $filterValue = $criteria[$this->getName()];
        }

        $this->setModelValue($filterValue);

        return $this;
    }

    /**
     * @return bool
     */
    public function isValidWithCriteriaMap(): bool
    {
        if ($this->getType() !== NoneFilter::class) {
            $criteriaMap = $this->table->getCriteriaMap();

            return \array_key_exists($this->getName(), $criteriaMap);
        }

        return true;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): self
    {
        $optionsResolver->setDefaults([
            'label' => '',
            'default_value' => '',
            'input_attributes' => [],
        ]);

        $optionsResolver->setDefined([
            'class',
        ]);

        $optionsResolver->setAllowedTypes('label', 'string');
        $optionsResolver->setAllowedTypes('input_attributes', 'array');

        return $this;
    }

    // ~

    /**
     * Filter data transformers works similar to form data transformers.
     * Internally data is stored in norm form and is transformed either to view form or model form.
     *
     * MODEL_DATA =======(modelTransformer)=====> NORM_DATA ======(viewTransformer)======> VIEW_DATA
     *
     * MODEL_DATA <==(modelReverseTransformer)=== NORM_DATA <==(viewReverseTransformer)=== VIEW_DATA
     *
     * If no transformer is defined, setModelValue & setViewValue works equal to setValue,
     * and getModelValue & getViewValue works equal to getValue.
     */

    /**
     * Returns norm data transformed to model data
     *
     * @return mixed
     */
    public function getModelValue()
    {
        $transformer = $this->modelReverseTransformer;

        if (null !== $transformer) {
            return $transformer($this->getValue());
        }

        return $this->getValue();
    }

    /**
     * Returns norm data transformed to view data
     *
     * @return mixed
     */
    public function getViewValue()
    {
        $transformer = $this->viewTransformer;

        if (null !== $transformer) {
            return $transformer($this->getValue());
        }

        return $this->getValue();
    }

    /**
     * Sets norm data from model data
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setModelValue($value): self
    {
        $transformer = $this->modelTransformer;

        if (null !== $transformer) {
            $value = $transformer($value);
        }

        $this->setValue($value);

        return $this;
    }

    /**
     * Sets norm data from view data
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setViewValue($value): self
    {
        $transformer = $this->viewReverseTransformer;

        if (null !== $transformer) {
            $value = $transformer($value);
        }

        $this->setValue($value);

        return $this;
    }

    /**
     * Method can be overridden in inheriting class
     *
     * @return $this
     */
    public function initializeFilter(): self
    {
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $label = $this->getAttributes()['label'] ?? '';

        if (empty($label)) {
            $label = $this->getColumn()->getLabel();
            // todo: fix
            // $this->setAttributes(['label' => $label]);
        }

        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'component' => $this->getComponentName(),
            'componentAttributes' => $this->getAttributes(),
            'value' => $this->getValue(),
            'modelValue' => $this->getModelValue(),
            'viewValue' => $this->getViewValue(),
            'columnName' => null !== $this->column ? $this->column->getName() : null,
        ];
    }
}
