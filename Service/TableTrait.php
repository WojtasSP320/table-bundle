<?php

namespace TableBundle\Service;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\TableException;

/**
 * TableTrait class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
trait TableTrait
{
    /** @var string */
    private $dataSourceRoute = '';

    /** @var array */
    private $dataSourceRouteParameters = [];

    /** @var bool */
    private $showHeader = true;

    /** @var bool */
    private $showLabels = true;

    /** @var bool */
    private $showFilters = true;

    /** @var bool */
    private $showSorting = true;

    /** @var string */
    private $showCheckboxes = TableInterface::TABLE_SHOW_CHECKBOXES_NONE;

    /** @var int */
    private $showActions = TableInterface::TABLE_SHOW_ACTIONS_RIGHT;

    /** @var int */
    private $showPagination = TableInterface::TABLE_SHOW_PAGINATION_BOTH;

    /** @var bool */
    private $lock = false;

    /** @var OptionsResolver|null */
    private $optionsResolver;

    /** @var FilterFactory|null */
    private $filterFactory;

    /** @var CellFactory|null */
    private $cellFactory;

    /** @var array */
    private $columnsOrder = [];

    /**
     * @return FilterFactory|null
     */
    public function getFilterFactory(): ?FilterFactory
    {
        return $this->filterFactory;
    }

    /**
     * @param FilterFactory $filterFactory
     *
     * @return $this
     */
    public function setFilterFactory(FilterFactory $filterFactory): self
    {
        $this->filterFactory = $filterFactory;

        return $this;
    }

    /**
     * @return CellFactory|null
     */
    public function getCellFactory(): ?CellFactory
    {
        return $this->cellFactory;
    }

    /**
     * @param CellFactory $cellFactory
     *
     * @return $this
     */
    public function setCellFactory(CellFactory $cellFactory): self
    {
        $this->cellFactory = $cellFactory;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumnsOrder(): array
    {
        return $this->columnsOrder;
    }

    /**
     * @param array $columnsOrder
     *
     * @return $this
     */
    public function setColumnsOrder(array $columnsOrder): self
    {
        $this->columnsOrder = $columnsOrder;

        return $this;
    }

    /**
     * @return $this
     */
    public function showHeader(): self
    {
        $this->showHeader = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function hideHeader(): self
    {
        $this->showHeader = false;

        return $this;
    }

    /**
     * @return $this
     */
    public function showLabels(): self
    {
        $this->showLabels = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function hideLabels(): self
    {
        $this->showLabels = false;

        return $this;
    }

    /**
     * @return $this
     */
    public function showFilters(): self
    {
        $this->showFilters = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function hideFilters(): self
    {
        $this->showFilters = false;

        return $this;
    }

    /**
     * @return $this
     */
    public function showSorting(): self
    {
        $this->showSorting = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function hideSorting(): self
    {
        $this->showSorting = false;

        return $this;
    }

    /**
     * @param string $showCheckboxes
     *
     * @throws TableException
     *
     * @return $this
     */
    public function showCheckboxes(string $showCheckboxes = TableInterface::TABLE_SHOW_CHECKBOXES_RIGHT): self
    {
        $showCheckboxesMap = [
            TableInterface::TABLE_SHOW_CHECKBOXES_NONE,
            TableInterface::TABLE_SHOW_CHECKBOXES_RIGHT,
            TableInterface::TABLE_SHOW_CHECKBOXES_LEFT,
            TableInterface::TABLE_SHOW_CHECKBOXES_BOTH,
        ];

        if (false === \in_array($showCheckboxes, $showCheckboxesMap, true)) {
            throw new TableException('Invalid show checkboxes parameter: "%s"', [$showCheckboxes]);
        }

        $this->showCheckboxes = $showCheckboxes;

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showCheckboxesLeft(): self
    {
        $this->showCheckboxes(TableInterface::TABLE_SHOW_CHECKBOXES_LEFT);

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showCheckboxesRight(): self
    {
        $this->showCheckboxes(TableInterface::TABLE_SHOW_CHECKBOXES_RIGHT);

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showCheckboxesBoth(): self
    {
        $this->showCheckboxes(TableInterface::TABLE_SHOW_CHECKBOXES_BOTH);

        return $this;
    }

    /**
     * @return $this
     */
    public function hideCheckboxes(): self
    {
        $this->showCheckboxes = TableInterface::TABLE_SHOW_CHECKBOXES_NONE;

        return $this;
    }

    /**
     * @param string $showActions
     *
     * @throws TableException
     *
     * @return $this
     */
    public function showActions(string $showActions = TableInterface::TABLE_SHOW_ACTIONS_RIGHT): self
    {
        $showActionsMap = [
            TableInterface::TABLE_SHOW_ACTIONS_NONE,
            TableInterface::TABLE_SHOW_ACTIONS_RIGHT,
            TableInterface::TABLE_SHOW_ACTIONS_LEFT,
            TableInterface::TABLE_SHOW_ACTIONS_BOTH,
        ];

        if (false === \in_array($showActions, $showActionsMap, true)) {
            throw new TableException('Invalid show actions parameter: "%s"', [$showActions]);
        }

        $this->showActions = $showActions;

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showActionsLeft(): self
    {
        $this->showActions(TableInterface::TABLE_SHOW_ACTIONS_LEFT);

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showActionsRight(): self
    {
        $this->showActions(TableInterface::TABLE_SHOW_ACTIONS_RIGHT);

        return $this;
    }

    /**
     * @throws TableException
     *
     * @return $this
     */
    public function showActionsBoth(): self
    {
        $this->showActions(TableInterface::TABLE_SHOW_ACTIONS_BOTH);

        return $this;
    }

    /**
     * @return $this
     */
    public function hideActions(): self
    {
        $this->showActions = TableInterface::TABLE_SHOW_ACTIONS_NONE;

        return $this;
    }

    /**
     * @param string $showPagination
     *
     * @return $this
     */
    public function showPagination(string $showPagination = TableInterface::TABLE_SHOW_PAGINATION_BOTH): self
    {
        $this->showPagination = $showPagination;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->lock;
    }

    /**
     * @return $this
     */
    private function lockTable(): self
    {
        $this->lock = true;

        return $this;
    }

    /**
     * @return $this
     */
    private function unlockTable(): self
    {
        $this->lock = false;

        return $this;
    }

    /**
     * @return OptionsResolver|null
     */
    public function getOptionsResolver(): ?OptionsResolver
    {
        return $this->optionsResolver;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function setOptionsResolver(OptionsResolver $optionsResolver): self
    {
        $this->optionsResolver = $optionsResolver;

        return $this;
    }

    /**
     * @deprecated
     *
     * @param string $dataSourceRoute
     *
     * @return $this
     */
    public function setDataSourceRoute(string $dataSourceRoute): self
    {
        $this->dataSourceRoute = $dataSourceRoute;

        return $this;
    }

    /**
     * @return string
     */
    public function getDataSourceRoute(): string
    {
        return $this->dataSourceRoute;
    }

    /**
     * @return array
     */
    public function getDataSourceRouteParameters(): array
    {
        return $this->dataSourceRouteParameters;
    }

    /**
     * @param array $dataSourceRouteParameters
     *
     * @return $this
     */
    public function setDataSourceRouteParameters(array $dataSourceRouteParameters): self
    {
        $this->dataSourceRouteParameters = $dataSourceRouteParameters;

        return $this;
    }

    /**
     * @param string $dataSourceRoute #Route
     * @param array $dataSourceRouteParameters
     *
     * @return $this
     */
    public function setDataSourceAction(string $dataSourceRoute, array $dataSourceRouteParameters = []): self
    {
        $this->setDataSourceRoute($dataSourceRoute);
        $this->setDataSourceRouteParameters($dataSourceRouteParameters);

        return $this;
    }
}
