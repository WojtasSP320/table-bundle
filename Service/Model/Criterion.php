<?php

namespace TableBundle\Service\Model;

/**
 * Class Criterion
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class Criterion
{
    /** @var string */
    public const OPERATOR_EQUAL = '=';

    /** @var string */
    public const OPERATOR_NOT_EQUAL = '!=';

    /** @var string */
    public const OPERATOR_LESS_THAN = '<';

    /** @var string */
    public const OPERATOR_LESS_THAN_OR_EQUAL = '<=';

    /** @var string */
    public const OPERATOR_GREATER_THAN = '>';

    /** @var string */
    public const OPERATOR_GREATER_THAN_OR_EQUAL = '>=';

    /** @var string */
    public const OPERATOR_LIKE = 'LIKE';

    /** @var string */
    public const OPERATOR_NOT_LIKE = 'NOT LIKE';

    /** @var string */
    public const OPERATOR_IS_NULL = 'IS NULL';

    /** @var string */
    public const OPERATOR_IS_NOT_NULL = 'IS NOT NULL';

    /** @var string */
    public const OPERATOR_IN = 'IN';

    /** @var string */
    public const CLAUSE_WHERE = 'where';

    /** @var string */
    public const CLAUSE_HAVING = 'having';

    /** @var string|null */
    private $name;

    /** @var string */
    private $field;

    /** @var string */
    private $operator;

    /** @var string */
    private $clauseType;

    /** @var mixed */
    private $value;

    /**
     * Criterion constructor.
     *
     * @param string $field
     * @param string $operator
     * @param string $clauseType
     */
    public function __construct(string $field, string $operator, string $clauseType = self::CLAUSE_WHERE)
    {
        $this->field = $field;
        $this->operator = $operator;
        $this->clauseType = $clauseType;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return string
     */
    public function getClauseType(): string
    {
        return $this->clauseType;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     *
     * @return Criterion
     */
    public function setValue($value): self
    {
        if (
            \is_string($value) &&
            (
                $this->operator === self::OPERATOR_LIKE ||
                $this->operator === self::OPERATOR_NOT_LIKE
            )
        ) {
            //$value = $this->escapeWildcards($value);
        }

        $this->value = $value;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    private function escapeWildcards(string $value): string
    {
        $map = [
            '_' => '\_',
            '%' => '\%',
        ];

        return \str_replace(\array_keys($map), \array_values($map), $value);
    }
}
