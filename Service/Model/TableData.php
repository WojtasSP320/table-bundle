<?php

namespace TableBundle\Service\Model;

use Symfony\Component\HttpFoundation\Request;
use TableBundle\Entity\TableSetting;

/**
 * Class TableData
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableData
{
    /** @var int */
    private $page = 1;

    /** @var int|null */
    private $limit;

    /** @var array */
    private $filters = [];

    /** @var array */
    private $sorting = [];

    /**
     * @param Request $request
     *
     * @return $this
     */
    public static function createFromRequest(Request $request): self
    {
        $postData = new self();

        $pagination = $request->request->get('pagination', []);

        $postData->page = $pagination['page'] ?? 1;
        $postData->limit = $pagination['limit'] ?? null;

        $postData->filters = $request->request->get('filters', []);
        $postData->sorting = $request->request->get('sorting', []);

        return $postData;
    }

    /**
     * @param TableSetting $settings
     *
     * @return $this
     */
    public static function createFromTableSettings(TableSetting $settings): self
    {
        $settingsData = new self();

        $settingsData->page = $settings->getPage() ?? 1;
        $settingsData->limit = $settings->getLimit();

        $settingsData->sorting = $settings->getSorting();
        $settingsData->filters = $settings->getCriteria();

        return $settingsData;
    }

    ///**
    // * @param TableData $tableData
    // *
    // * @return $this
    // */
    //public function overrideWith(TableData $tableData): self
    //{
    //    $this->page = $tableData->getPage();
    //    $this->limit = $tableData->getLimit();
    //
    //    $this->sorting = $tableData->getSorting();
    //    $this->filters = $tableData->getCriteria();
    //
    //    return $settingsData;
    //}

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     *
     * @return $this
     */
    public function setFilters(array $filters): self
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return array
     */
    public function getSorting(): array
    {
        return $this->sorting;
    }

    /**
     * @param array $sorting
     *
     * @return $this
     */
    public function setSorting(array $sorting): self
    {
        $this->sorting = $sorting;

        return $this;
    }
}
