<?php

namespace TableBundle\Service;

use Closure;
use DanlineBundle\Entity\EntityAbstract;
use TableBundle\Exception\TableException;

/**
 * TableChecker class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableChecker
{
    /** @var bool */
    private $checkerEnabled = false;

    /** @var array */
    private $checkboxes = [];

    /** @var string */
    private $columnLabel = '';

    /** @var Closure */
    private $checkboxHandler;

    /** @var Closure|null */
    private $checkboxDecorator;

    /** @var Closure|null */
    private $checkboxActivator;

    /** @var bool $defaultActiveValue */
    private $defaultActiveValue = true;

    /**
     * TableChecker constructor.
     *
     * @throws TableException
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->setDefaultCheckboxHandler();
    }

    /**
     * @return bool
     */
    public function isCheckerEnabled(): bool
    {
        return $this->checkerEnabled;
    }

    /**
     * @return $this
     */
    public function enableChecker(): self
    {
        $this->checkerEnabled = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disableChecker(): self
    {
        $this->checkerEnabled = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDefaultActiveValue(): bool
    {
        return $this->defaultActiveValue;
    }

    /**
     * @param bool $defaultActiveValue
     *
     * @return TableChecker
     */
    public function setDefaultActiveValue(bool $defaultActiveValue): self
    {
        $this->defaultActiveValue = $defaultActiveValue;

        return $this;
    }

    /**
     * @return string
     */
    public function getColumnLabel(): string
    {
        return $this->columnLabel;
    }

    /**
     * @param string $columnLabel
     *
     * @return $this
     */
    public function setColumnLabel(string $columnLabel): self
    {
        $this->columnLabel = $columnLabel;

        return $this;
    }

    /**
     * @param Closure $checkboxHandler
     *
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function setCheckboxHandler(Closure $checkboxHandler): self
    {
        $this->validateCheckboxHandler($checkboxHandler);

        $this->checkboxHandler = $checkboxHandler;

        return $this;
    }

    /**
     * @return array
     */
    protected function getCheckboxes(): array
    {
        return $this->checkboxes;
    }

    /**
     * @param array $checkboxes
     *
     * @return $this
     */
    public function setCheckboxes(array $checkboxes): self
    {
        $this->checkboxes = $checkboxes;

        return $this;
    }

    /**
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function setDefaultCheckboxHandler(): self
    {
        $handler = function ($object, array $checked) {
            $entity = null;

            if ($object instanceof EntityAbstract) {
                $entity = $object;
            } elseif (
                \is_array($object) &&
                isset($object[0]) &&
                ($object[0] instanceof EntityAbstract)
            ) {
                $entity = $object[0];
            }

            if (\is_callable([$entity, 'getId'])) {
                $id = $entity->getId();

                return \in_array($id, $checked, true);
            }

            return false;
        };

        $this->setCheckboxHandler($handler);

        return $this;
    }

    /**
     * @param Closure $closure
     *
     * @throws TableException
     * @throws \ReflectionException
     */
    protected function validateCheckboxHandler(Closure $closure): void
    {
        $reflectionFunction = new \ReflectionFunction($closure);

        $parametersNumber = $reflectionFunction->getNumberOfParameters();

        if ($parametersNumber !== 2) {
            throw new TableException('Checkbox handling callback must have exactly two parameters!');
        }

        $firstParameter = $reflectionFunction->getParameters()[0];
        $secondParameter = $reflectionFunction->getParameters()[1];

        if (!$secondParameter->isArray()) {
            throw new TableException('Checkbox handling callbacks second parameter must be type array!');
        }

        if (
            $firstParameter->isPassedByReference() ||
            $secondParameter->isPassedByReference()
        ) {
            throw new TableException('Checkbox handling callbacks parameters cannot be passed by reference!');
        }
    }

    /**
     * @param mixed $object
     * @param array $tableOptions
     *
     * @throws TableException
     *
     * @return bool
     */
    public function handleCheckboxes($object, array $tableOptions): bool
    {
        $checkboxHandler = $this->checkboxHandler;

        /* todo: implement $tableOptions usage when needed */

        if (null === $checkboxHandler) {
            throw new TableException('You must provide valid checkbox handler or use default.');
        }

        $isChecked = $checkboxHandler($object, $this->getCheckboxes());

        return (bool)$isChecked;
    }

    /**
     * @param Closure $decorator
     *
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function setCheckboxDecorator(Closure $decorator): self
    {
        $this->validateCheckboxDecorator($decorator);

        $this->checkboxDecorator = $decorator;

        return $this;
    }

    /**
     * @param Closure $closure
     *
     * @throws TableException
     * @throws \ReflectionException
     */
    protected function validateCheckboxDecorator(Closure $closure): void
    {
        $reflectionFunction = new \ReflectionFunction($closure);

        $parametersNumber = $reflectionFunction->getNumberOfParameters();

        if ($parametersNumber > 3) {
            throw new TableException('Checkbox decorator callback must have fewer than 4 parameters!');
        }

        $firstParameter = $reflectionFunction->getParameters()[0] ?? null;
        $secondParameter = $reflectionFunction->getParameters()[1] ?? null;

        if ((null !== $secondParameter) && (false == $secondParameter->isArray())) {
            throw new TableException('Checkbox decorator callbacks second parameter must be type array!');
        }

        if (
            (null !== $firstParameter && $firstParameter->isPassedByReference()) ||
            (null !== $secondParameter && $secondParameter->isPassedByReference())
        ) {
            throw new TableException('Checkbox decorator callbacks parameters cannot be passed by reference!');
        }
    }

    /**
     * @param mixed $object
     * @param array $tableOptions
     *
     * @throws TableException
     *
     * @return array
     */
    public function decorateCheckbox($object, array $tableOptions): array
    {
        $decoration = [];

        if ($this->hasDecorator()) {
            $checkboxDecorator = $this->checkboxDecorator;
            $decoration = $checkboxDecorator($object, $this->getCheckboxes(), $tableOptions);

            if (false == \is_array($decoration)) {
                throw new TableException('Checkbox decorator must return array but "%s" returned!', [
                    \is_object($decoration) ? \get_class($decoration) : \gettype($decoration),
                ]);
            }
        }

        return $decoration;
    }

    /**
     * @return bool
     */
    public function hasDecorator(): bool
    {
        return (null !== $this->checkboxDecorator);
    }

    // ~

    /**
     * @param Closure $activator
     *
     * @throws TableException
     * @throws \ReflectionException
     *
     * @return $this
     */
    public function setCheckboxActivator(Closure $activator): self
    {
        $this->validateCheckboxActivator($activator);

        $this->checkboxActivator = $activator;

        return $this;
    }

    /**
     * @param Closure $closure
     *
     * @throws TableException
     * @throws \ReflectionException
     */
    protected function validateCheckboxActivator(Closure $closure): void
    {
        $reflectionFunction = new \ReflectionFunction($closure);

        $parametersNumber = $reflectionFunction->getNumberOfParameters();

        if ($parametersNumber > 3) {
            throw new TableException('Checkbox activator callback must have less than 4 parameters!');
        }

        $firstParameter = $reflectionFunction->getParameters()[0] ?? null;
        $secondParameter = $reflectionFunction->getParameters()[1] ?? null;

        if ((null !== $secondParameter) && (false == $secondParameter->isArray())) {
            throw new TableException('Checkbox activator callbacks second parameter must be type array!');
        }

        if (
            (null !== $firstParameter && $firstParameter->isPassedByReference()) ||
            (null !== $secondParameter && $secondParameter->isPassedByReference())
        ) {
            throw new TableException('Checkbox activator callbacks parameters cannot be passed by reference!');
        }
    }

    /**
     * @param mixed $object
     * @param array $tableOptions
     *
     * @return bool
     */
    public function activateCheckbox($object, array $tableOptions): bool
    {
        if ($this->hasActivator()) {
            $checkboxActivator = $this->checkboxActivator;

            $active = (bool)$checkboxActivator($object, $this->getCheckboxes(), $tableOptions);
        } else {
            $active = $this->getDefaultActiveValue();
        }

        return $active;
    }

    /**
     * @return bool
     */
    public function hasActivator(): bool
    {
        return (null !== $this->checkboxActivator);
    }
}
