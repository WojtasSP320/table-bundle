<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * DateTimeIntervalField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class DateTimeIntervalField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'DateTimeIntervalCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_NUMBER;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'empty_value' => '---',
            'css_class' => '',
        ]);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return $rawData;
    }
}
