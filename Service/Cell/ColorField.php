<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * ColorField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class ColorField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'ColorCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_STRING;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'format' => 'hex',
        ]);

        $optionsResolver->setAllowedTypes('format', ['string']);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return (string)$rawData;
    }
}
