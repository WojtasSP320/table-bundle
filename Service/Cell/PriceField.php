<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\CellException;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\CellAbstract;

/**
 * PriceField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PriceField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'PriceCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_MONEY;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'currency' => 'PLN',
            'decimal_digits' => 2,
            'decimals_point' => ',',
            'thousands_separator' => ' ',
            'empty_value' => '---',
            'buttons' => false,
        ]);

        $optionsResolver->setAllowedTypes('currency', ['null', 'string']);
        $optionsResolver->setAllowedTypes('decimal_digits', 'integer');
        $optionsResolver->setAllowedTypes('decimals_point', 'string');
        $optionsResolver->setAllowedTypes('thousands_separator', 'string');
        $optionsResolver->setAllowedTypes('empty_value', 'string');
        $optionsResolver->setAllowedTypes('buttons', ['bool', 'array']);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @throws CellException
     * @throws ColumnException
     *
     * @return array|mixed
     */
    public function getViewData($rawData)
    {
        if (\is_array($rawData)) {
            if (false == \array_key_exists('value', $rawData) || false == \array_key_exists('currency', $rawData)) {
                throw new CellException('Invalid data type in column "%s": Must be an array with keys ("value", "currency") or float value!', [
                    '???',
                ]);
            }
        } else {
            $rawData = [
                'value' => (null !== $rawData) ? (float)$rawData : null,
                'currency' => $this->getOption('currency'),
            ];
        }

        return $rawData;
    }

    /**
     * @param mixed $rawData
     *
     * @throws CellException
     * @throws ColumnException
     *
     * @return array
     */
    public function getRawData($rawData)
    {
        if (\is_array($rawData)) {
            if (false == \array_key_exists('value', $rawData) || false == \array_key_exists('currency', $rawData)) {
                throw new CellException('Invalid data type in column "%s": Must be an array with keys ("value", "currency") or float value!', [
                    '???',
                ]);
            }
        } else {
            $rawData = [
                'value' => (null !== $rawData) ? (float)$rawData : null,
                'currency' => $this->getOption('currency'),
            ];
        }

        return $rawData;
    }
}
