<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * UserAgentField class.
 *
 * @author Tomi <tosmialowski@danhoss.com>
 */
class UserAgentField extends CellAbstract
{
    /** @var int */
    public const ICON_SIZE_1x = 1;

    /** @var int */
    public const ICON_SIZE_2x = 2;

    /** @var int */
    public const ICON_SIZE_3x = 3;

    /** @var int */
    public const ICON_SIZE_4x = 4;

    /** @var int */
    public const ICON_SIZE_5x = 5;

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'UserAgentCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_CUSTOM;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'show_browser' => true,
            'show_operating_system' => true,
            'show_device' => true,
            'show_labels' => true,
            'show_icons' => true,
            'short_labels' => true,
            'icon_size' => self::ICON_SIZE_2x,
        ]);

        $optionsResolver
            ->setAllowedTypes('show_browser', 'bool')
            ->setAllowedTypes('show_operating_system', 'bool')
            ->setAllowedTypes('show_device', 'bool')
            ->setAllowedTypes('show_labels', 'bool')
            ->setAllowedTypes('short_labels', 'bool')
            ->setAllowedTypes('icon_size', 'integer')
            ->setAllowedValues('icon_size', [
                self::ICON_SIZE_1x,
                self::ICON_SIZE_2x,
                self::ICON_SIZE_3x,
                self::ICON_SIZE_4x,
                self::ICON_SIZE_5x,
            ])
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return array|mixed
     */
    public function getViewData($rawData)
    {
        return $rawData;
    }
}
