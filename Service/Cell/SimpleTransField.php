<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\CellAbstract;

/**
 * SimpleTransField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class SimpleTransField extends CellAbstract
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     *
     * @return $this
     */
    public function setTranslator($translator): self
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'SimpleTransCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_STRING;
    }

    // ~

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'trans_prefix' => '',
            'trans_domain' => null,
            'trans_parameters' => [],
            'locale' => null,
            'raw_output' => false,
            'empty_value' => '---',
            'placeholder' => '',
        ]);

        $optionsResolver
            ->setAllowedTypes('trans_prefix', 'string')
            ->setAllowedTypes('raw_output', 'bool')
            ->setAllowedTypes('empty_value', 'string')
            ->setAllowedTypes('trans_domain', ['null', 'string'])
            ->setAllowedTypes('trans_parameters', 'array')
            ->setAllowedTypes('locale', ['null', 'string'])
            ->setAllowedTypes('placeholder', ['string'])
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @throws ColumnException
     *
     * @return mixed|string|null
     */
    public function getViewData($rawData)
    {
        if ('' !== $rawData) {
            return $this->translator->trans(
                $this->getOption('trans_prefix') . $rawData,
                $this->getOption('trans_parameters'),
                $this->getOption('trans_domain'),
                $this->getOption('locale')
            );
        }

        return null;
    }
}
