<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\CellException;
use TableBundle\Service\CellAbstract;

/**
 * DateField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class DateField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'DateCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_DATE_TIME;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'format' => 'Y-m-d',
            'empty_value' => '---',
        ]);

        $optionsResolver->setAllowedTypes('format', 'string');
        $optionsResolver->setAllowedTypes('empty_value', 'string');

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @throws CellException
     *
     * @return mixed
     */
    public function getViewData($rawData)
    {
        if (false == ($rawData instanceof \DateTime) && (null !== $rawData)) {
            throw new CellException('Invalid data type in column "%s": DateTime or null expected, but "%s" given!', [
                '???',
                \is_object($rawData)
                    ? \get_class($rawData)
                    : \gettype($rawData),
            ]);
        }

        return $rawData;
    }
}
