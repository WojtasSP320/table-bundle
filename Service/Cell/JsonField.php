<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * JsonField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class JsonField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'JsonCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_CUSTOM;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        /* todo: add some options */

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return (null !== $rawData) ? json_encode($rawData) : null;
    }
}
