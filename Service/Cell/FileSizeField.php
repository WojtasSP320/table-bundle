<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * FileSizeField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class FileSizeField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'FileSizeCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_NUMBER;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'decimal_digits' => 2,
            'decimals_point' => ',',
            'thousands_separator' => ' ',
            'compact' => false,
            'empty_value' => '---',
        ]);

        $optionsResolver
            ->setAllowedTypes('decimal_digits', 'integer')
            ->setAllowedTypes('decimals_point', 'string')
            ->setAllowedTypes('thousands_separator', 'string')
            ->setAllowedTypes('compact', 'bool')
            ->setAllowedTypes('empty_value', 'string')
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return (string)$rawData;
    }
}
