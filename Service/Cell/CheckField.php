<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\CellAbstract;

/**
 * CheckField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class CheckField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'CheckCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_BOOL;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'marked' => ['1', 1, true],
            'theme' => '',
        ]);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @throws ColumnException
     *
     * @return bool|mixed
     */
    public function getViewData($rawData)
    {
        return \in_array($rawData, $this->getOption('marked'), true);
    }
}
