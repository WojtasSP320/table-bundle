<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * EditableField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class EditableField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'EditableCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_STRING;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'raw_output' => false,
            'escape_output' => false,
            'striptags' => false,
            'empty_value' => '---',
            'css_class' => '',
            'class' => null,
            'field' => null,
        ]);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return $rawData;
    }
}
