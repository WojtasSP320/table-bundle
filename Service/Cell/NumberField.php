<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * NumberField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class NumberField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'NumberCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_NUMBER;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'thousands_separator' => ' ',
            'empty_value' => '---',
        ]);

        $optionsResolver
            ->setAllowedTypes('thousands_separator', 'string')
            ->setAllowedTypes('empty_value', 'string')
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return (null !== $rawData) ? (int)$rawData : null;
    }
}
