<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * PercentField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PercentField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'PercentCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_FLOAT;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        /** todo: fetch from system settings */
        $optionsResolver->setDefaults([
            'empty_value' => '---',
            'decimal_digits' => 2,
            'format' => 'fraction',
        ]);

        $optionsResolver
            ->setAllowedTypes('empty_value', 'string')
            ->setAllowedTypes('decimal_digits', 'integer')
            ->setAllowedTypes('format', 'string')
        ;

        $optionsResolver
            ->setAllowedValues('format', ['fraction', 'integer'])
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        if (null === $rawData) {
            return null;
        }

        return ('fraction' === $this->getOption('format'))
            ? (float)$rawData * 100.0
            : (float)$rawData
        ;
    }
}
