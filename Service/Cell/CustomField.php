<?php

namespace TableBundle\Service\Cell;

use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\ColumnException;
use TableBundle\Service\CellAbstract;

/**
 * CustomField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class CustomField extends CellAbstract
{
    /** @var string */
    private $componentName = 'SimpleCell';

    /** @var ArrayTransformerInterface */
    private $serializer;

    /**
     * @param ArrayTransformerInterface $serializer
     *
     * @return void
     */
    public function setSerializer(ArrayTransformerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return $this->componentName;
    }

    /**
     * @param string $componentName
     *
     * @return CustomField
     */
    public function setComponentName(string $componentName): self
    {
        $this->componentName = $componentName;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_CUSTOM;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver
            ->setDefault('componentAttributes', [])
            ->setDefault('serialization_groups', ['public'])
            ->setDefault('serialize_raw_data', false)
            ->setDefault('serialization_context', null)
            ->setDefault('serialize_null', true);

        $optionsResolver
            ->setAllowedTypes('serialize_raw_data', 'bool')
            ->setAllowedTypes('serialization_groups', 'array')
            ->setAllowedTypes('serialize_null', 'bool')
            ->setAllowedTypes('serialization_context', ['null', SerializationContext::class]);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @throws ColumnException
     *
     * @return mixed
     */
    public function getViewData($rawData)
    {
        if ($this->getOption('serialize_raw_data')) {
            if (\is_object($rawData) || \is_array($rawData)) {
                $context = $this->prepareSerializationContext();

                return $this->serializer->toArray($rawData, $context);
            }
        }

        return $rawData;
    }

    /**
     * @throws ColumnException
     *
     * @return SerializationContext
     */
    private function prepareSerializationContext(): SerializationContext
    {
        if (null === $this->getOption('serialization_context')) {
            $context = new SerializationContext();

            $serializeNull = $this->getOption('serialize_null');
            $serializationGroups = $this->getOption('serialization_groups');

            $context
                ->setSerializeNull($serializeNull)
                ->setGroups($serializationGroups);
        } else {
            $context = $this->getOption('serialization_context');
        }

        return $context;
    }
}
