<?php

namespace TableBundle\Service\Cell;

use DanlineBundle\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * UserField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class UserField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'UserCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_CUSTOM;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'empty_value' => '---',
        ]);

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return array
     */
    public function getViewData($rawData)
    {
        /** @var User $rawData */
        if (null === $rawData) {
            return null;
        }

        if (null !== $rawData->getAvatar()) {
            $avatarId = $rawData->getAvatar()->getId();
        }

        return [
            'id' => $rawData->getId(),
            'name' => $rawData->getName(),
            'skype' => $rawData->getSkype(),
            'isUserCurrentlyInWork' => $rawData->isUserCurrentlyInWork(),
            'avatar' => [
                'id' => $avatarId ?? null,
            ],
        ];
    }
}
