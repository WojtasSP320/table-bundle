<?php

namespace TableBundle\Service\Cell;

use DanlineBundle\Entity\Order;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * Class ProductField
 */
class ProductField extends CellAbstract
{
    /** @var string */
    private $componentName = 'ProductsCell';

    /**
     * @inheritDoc
     */
    public function getComponentName(): string
    {
        return $this->componentName;
    }

    /**
     * @param string $componentName
     *
     * @return ProductField
     */
    public function setComponentName(string $componentName): self
    {
        $this->componentName = $componentName;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        $optionsResolver->setDefaults([
            'empty_value' => '---',
        ]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return self::CELL_TYPE_CUSTOM;
    }

    /**
     * @param mixed $rawData
     *
     * @return array|mixed|null
     */
    public function getViewData($rawData)
    {
        if (null === $rawData) {
            return null;
        }

        /** @var Order $order */
        $order = $rawData;
        $orderItems = $order->getOrderItems();
        $productsWithQuantity = [];
        foreach ($orderItems as $item) {
            $productsWithQuantity[] = [
              'productSymbol' => $item->getProduct()->getSymbol(),
              'quantity' => $item->getQuantity(),
            ];
        }

        return $productsWithQuantity;
    }
}
