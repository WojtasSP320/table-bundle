<?php

namespace TableBundle\Service\Cell;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Service\CellAbstract;

/**
 * FloatField class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class FloatField extends CellAbstract
{
    /**
     * @return string
     */
    public function getComponentName(): string
    {
        return 'FloatCell';
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::CELL_TYPE_FLOAT;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function configureOptionsResolver(OptionsResolver $optionsResolver): CellAbstract
    {
        /** todo: fetch from system settings */
        $optionsResolver->setDefaults([
            'decimal_digits' => 2,
            'decimals_point' => ',',
            'thousands_separator' => ' ',
            'empty_value' => '---',
        ]);

        $optionsResolver
            ->setAllowedTypes('decimal_digits', 'integer')
            ->setAllowedTypes('decimals_point', 'string')
            ->setAllowedTypes('thousands_separator', 'string')
            ->setAllowedTypes('empty_value', 'string')
        ;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return string
     */
    public function getViewData($rawData)
    {
        return (null !== $rawData) ? (float)$rawData : null;
    }
}
