<?php

namespace TableBundle\Service;

use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\ColumnException;

/**
 * CellAbstract class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
abstract class CellAbstract implements CellInterface
{
    /** @var mixed */
    protected $rawData;

    /** @var mixed */
    protected $viewData;

    /** @var array */
    protected $options = [];

    /** @var OptionsResolver|null */
    protected $optionsResolver;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $optionsResolver = new OptionsResolver();

        $this
            ->configureOptionsResolver($optionsResolver)
            ->setOptionsResolver($optionsResolver)
            ->setOptions($options)
        ;
    }

    /**
     * @return OptionsResolver|null
     */
    public function getOptionsResolver(): ?OptionsResolver
    {
        return $this->optionsResolver;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    public function setOptionsResolver(OptionsResolver $optionsResolver): self
    {
        $this->optionsResolver = $optionsResolver;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string $optionName
     *
     * @throws ColumnException
     *
     * @return mixed
     */
    public function getOption(string $optionName)
    {
        $options = $this->getOptions();

        if (\array_key_exists($optionName, $options)) {
            return $options[$optionName];
        }

        throw new ColumnException('Unknown option "%s" in "%s" cell.', [
            $optionName,
            $this->getComponentName(),
        ]);
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options = []): self
    {
        $mergedOptions = \array_merge($this->getOptions(), $options);
        $options = $this->getOptionsResolver()->resolve($mergedOptions);
        $this->options = $options;

        return $this;
    }

    /**
     * @param mixed $rawData
     *
     * @return mixed
     */
    public function getViewData($rawData)
    {
        return $rawData;
    }

    /**
     * @return string
     */
    abstract public function getComponentName(): string;

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    abstract public function configureOptionsResolver(OptionsResolver $optionsResolver): self;
}
