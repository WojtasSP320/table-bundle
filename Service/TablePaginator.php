<?php

namespace TableBundle\Service;

use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TableBundle\Exception\TablePaginatorException;
use TableBundle\Service\Model\TableData;

/**
 * TablePaginator class.
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TablePaginator
{
    /** @var int */
    public const LIMIT_ALL = 0;

    /** @var int */
    private $currentPage = 1;

    /** @var int|null */
    private $limit;

    /** @var int */
    private $maxPage;

    /** @var array|int[] */
    private $possibleLimits;

    /** @var bool */
    private $paginationEnabled = true;

    /** @var bool */
    private $pageValidation = true;

    /** @var array */
    private $options;

    /** @var OptionsResolver */
    private $optionsResolver;

    /** @var int */
    private $currentItemsCount;

    /** @var int */
    private $totalItemsCount;

    /** @var int */
    private $maxTiles = 5;

    /**
     * TablePaginator constructor.
     *
     * @param array $options
     *
     * @throws TablePaginatorException
     */
    public function __construct(array $options = [])
    {
        $this
            ->setOptionsResolver(new OptionsResolver())
            ->setDefaultOptions($this->getOptionsResolver())
            ->setOptions($options)
        ;

        // ~

        $this
            ->setLimit($this->getOptions()['limit'])
            ->setCurrentPage($this->getOptions()['currentPage'])
            ->setPossibleLimits($this->getOptions()['possibleLimits'])
        ;
    }

    /**
     * @return $this
     */
    public function enablePagination(): self
    {
        $this->paginationEnabled = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disablePagination(): self
    {
        $this->paginationEnabled = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaginationEnabled(): bool
    {
        return $this->paginationEnabled;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        $currentPage = $this->currentPage;

        if ($this->pageValidation && null !== $this->getMaxPage()) {
            $pageLimit = $this->getMaxPage();
            $currentPage = ($currentPage > $pageLimit)
                ? $pageLimit
                : $currentPage;
        }

        return $currentPage;
    }

    /**
     * @param int $currentPage
     *
     * @return $this
     */
    private function setCurrentPage(int $currentPage): self
    {
        $currentPage = ($currentPage > 0) ? $currentPage : 1;
        $this->currentPage = $currentPage;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    private function setLimit(int $limit = null): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return array
     */
    public function getPossibleLimits(): array
    {
        return $this->possibleLimits;
    }

    /**
     * @param array $possibleLimits
     *
     * @throws TablePaginatorException
     *
     * @return $this
     */
    public function setPossibleLimits(array $possibleLimits = []): self
    {
        foreach ($possibleLimits as $possibleLimit) {
            if (false === \is_int($possibleLimit)) {
                throw new TablePaginatorException('Invalid paginator possible limit value: "%s"!', [
                    $possibleLimit,
                ]);
            }
        }

        $this->possibleLimits = $possibleLimits;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxTiles(): int
    {
        return $this->maxTiles;
    }

    /**
     * @param int $maxTiles
     *
     * @return TablePaginator
     */
    public function setMaxTiles(int $maxTiles): self
    {
        $this->maxTiles = \max($maxTiles, 1);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxPage(): ?int
    {
        return $this->maxPage;
    }

    /**
     * When page validation is enabled and
     * page value is greater than max page,
     * max page value is returned.
     *
     * @return $this
     */
    public function enablePageValidation(): self
    {
        $this->pageValidation = true;

        return $this;
    }

    /**
     * Disables page validation
     *
     * @see enablePageValidation
     *
     * @return $this
     */
    public function disablePageValidation(): self
    {
        $this->pageValidation = false;

        return $this;
    }

    /**
     * @return OptionsResolver
     */
    protected function getOptionsResolver(): OptionsResolver
    {
        return $this->optionsResolver;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    protected function setOptionsResolver(OptionsResolver $optionsResolver): self
    {
        $this->optionsResolver = $optionsResolver;

        return $this;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options = []): self
    {
        try {
            $options = $this->getOptionsResolver()->resolve($options);
            $this->options = $options;
        } catch (InvalidOptionsException $exception) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            $messageSuffix = "\n In table paginator options.";

            throw new InvalidOptionsException($message . $messageSuffix, $code, $exception);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param int $currentItemsCount
     * @param int|null $resultsCount
     *
     * @return array
     */
    public function getPaginationData(int $currentItemsCount, int $resultsCount = null): array
    {
        $this->resolveMaxPage($resultsCount);

        $paginationData = [
            'limit' => $this->getLimit(),
            'possibleLimits' => $this->getPossibleLimits(),
            'currentPage' => $this->getCurrentPage(),
            'maxPage' => $this->getMaxPage(),
            'currentItemsCount' => $currentItemsCount,
            'totalItemsCount' => $resultsCount,
            'maxTiles' => $this->getMaxTiles(),
        ];

        return $paginationData;
    }

    // ~

    /**
     * @return int
     */
    public function getListOffset(): int
    {
        $limit = $this->getLimit();
        $page = $this->getCurrentPage();

        return ($page - 1) * (int)$limit;
    }

    /**
     * @return int|null
     */
    public function getListLimit(): ?int
    {
        $limit = $this->getLimit();

        return ($limit > 0) ? $limit : null;
    }

    // ~

    /**
     * @param TableData $postData
     */
    public function handlePagination(TableData $postData): void
    {
        $page = $postData->getPage() ?? 1;
        $limit = $postData->getLimit();

        $this->setCurrentPage($page);
        $this->setLimit($limit);
    }

    /**
     * @param int|null $resultsCount
     *
     * @return $this
     */
    private function resolveMaxPage(int $resultsCount = null): self
    {
        $limit = $this->getLimit();

        $maxPage = ($limit > 0)
            ? (int)\ceil($resultsCount / $limit)
            : null
        ;

        $this->maxPage = $maxPage;

        return $this;
    }

    /**
     * @param OptionsResolver $optionsResolver
     *
     * @return $this
     */
    protected function setDefaultOptions(OptionsResolver $optionsResolver): self
    {
        $optionsResolver->setDefaults([
            'limit' => 20,
            'currentPage' => 1,
            'possibleLimits' => [20, 50, 100],
        ]);

        $optionsResolver
            ->setAllowedTypes('limit', 'integer')
            ->setAllowedTypes('currentPage', 'integer')
            ->setAllowedTypes('possibleLimits', 'array');


        return $this;
    }
}
