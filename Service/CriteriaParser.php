<?php

namespace TableBundle\Service;

use TableBundle\Service\Model\Criterion;

/**
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class CriteriaParser
{
    /**
     * Parse post data to criteria in database.
     *
     * @param array $criteriaMap
     * @param array $postData
     *
     * @return array
     */
    public function parse(array $criteriaMap, array $postData): array
    {
        $results = [];

        foreach ($postData as $columnName => $value) {
            /** @var array|Criterion $criterion */
            $criterion = $criteriaMap[$columnName] ?? null;

            if (null === $criterion) {
                /* todo: prevent sending NoneFilters */
                continue;
            }

            $isNestedTypeCriteria = $this->isNestedTypeCriteria($criterion);
            $criterionIsNotEmpty = !\in_array($value, ['', null, []], true);
            $criterionIsDefinedInMap = \array_key_exists($columnName, $criteriaMap);

            if ($isNestedTypeCriteria) {
                $results[] = $this->handleNestedCriteria($columnName, $value, $criteriaMap);
            } elseif ($criterionIsNotEmpty && $criterionIsDefinedInMap) {
                $results[] = $this->handleSimpleCriteria($criterion, $columnName, $value);
            }
        }

        $results = \array_merge([], ...$results);

        $criteria = [];

        \array_walk($results, static function (Criterion $criterion) use (&$criteria) {
            $criteria[$criterion->getName()] = $criterion;
        });

        return $criteria;
    }

    /**
     * @param array|Criterion|Criterion[] $criteria
     *
     * @return bool
     */
    private function isNestedTypeCriteria($criteria): bool
    {
        if ($criteria instanceof Criterion) {
            return false;
        }

        if (\is_array($criteria)) {
            foreach ($criteria as $criterion) {
                if (false === $criterion instanceof Criterion) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param Criterion $criterion
     * @param string $columnName
     * @param mixed $value
     *
     * @return array
     */
    private function handleSimpleCriteria(Criterion $criterion, string $columnName, $value): array
    {
        $criterion
            ->setName($columnName)
            ->setValue($value)
        ;

        return [$criterion];
    }

    /**
     * @param string $columnName
     * @param array $subCriteria
     * @param array $criteriaMap
     *
     * @return array
     */
    private function handleNestedCriteria(string $columnName, array $subCriteria, array $criteriaMap): array
    {
        $result = [];

        foreach ($subCriteria as $key => $value) {
            $criterionIsNotEmpty = !\in_array($value, ['', null, []], true);
            $criterionIsDefinedInMap = \array_key_exists($key, $criteriaMap[$columnName]);

            if ($criterionIsNotEmpty && $criterionIsDefinedInMap) {
                /** @var Criterion $criterion */
                $criterion = $criteriaMap[$columnName][$key];
                $name = \sprintf('%s.%s', $columnName, $key);
                $criterion
                    ->setName($name)
                    ->setValue($value)
                ;

                $result[] = $criterion;
            }
        }

        return $result;
    }
}
