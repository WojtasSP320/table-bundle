<?php

namespace TableBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class TableBundle
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class TableBundle extends Bundle
{
    /* ... */
}
