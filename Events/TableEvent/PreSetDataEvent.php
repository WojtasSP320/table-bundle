<?php

namespace TableBundle\Events\TableEvent;

use Doctrine\ORM\Tools\Pagination\Paginator;
use TableBundle\Events\TableEventAbstract;

/**
 * Class PreSetDataEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PreSetDataEvent extends TableEventAbstract
{
    /** @var array|Paginator $list */
    private $list;

    /**
     * @return array|Paginator
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param array|Paginator $list
     *
     * @return $this
     */
    public function setList($list): self
    {
        $this->list = $list;

        return $this;
    }
}
