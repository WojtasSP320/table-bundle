<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PostGenerateEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PostGenerateEvent extends TableEventAbstract
{
    /** @var array|string $column */
    private $column;

    /** @var array|string $direction */
    private $direction;

    /** @var int $page */
    private $page;

    /** @var int $limit */
    private $limit;

    /** @var array $criteria */
    private $criteria;

    /**
     * @return array|string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param array|string $column
     *
     * @return $this
     */
    public function setColumn($column): self
    {
        $this->column = $column;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param array|string $direction
     *
     * @return $this
     */
    public function setDirection($direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage($page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     *
     * @return $this
     */
    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }
}
