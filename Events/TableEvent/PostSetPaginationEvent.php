<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PostSetPaginationEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PostSetPaginationEvent extends TableEventAbstract
{
    /** @var int $page */
    private $page;

    /** @var int $limit */
    private $limit;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return $this
     */
    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
}
