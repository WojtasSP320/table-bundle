<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PreSetCriteriaEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PreSetSortingEvent extends TableEventAbstract
{
    /** @var array|string $column */
    private $column;

    /** @var array|string $direction */
    private $direction;

    /**
     * @return array|string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param array|string $column
     *
     * @return $this
     */
    public function setColumn($column): self
    {
        $this->column = $column;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param array|string $direction
     *
     * @return $this
     */
    public function setDirection($direction): self
    {
        $this->direction = $direction;

        return $this;
    }
}
