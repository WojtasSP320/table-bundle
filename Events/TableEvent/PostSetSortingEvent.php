<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PostSetSortingEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PostSetSortingEvent extends TableEventAbstract
{
    /** @var array|string $column */
    private $column;

    /** @var array|string $direction */
    private $direction;

    /**
     * @return array|string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param array|string $column
     *
     * @return PostSetSortingEvent
     */
    public function setColumn($column): self
    {
        $this->column = $column;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param array|string $direction
     *
     * @return PostSetSortingEvent
     */
    public function setDirection($direction): self
    {
        $this->direction = $direction;

        return $this;
    }
}
