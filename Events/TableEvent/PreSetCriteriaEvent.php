<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PreSetCriteriaEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PreSetCriteriaEvent extends TableEventAbstract
{
    /** @var array $criteria */
    private $criteria;

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return $this->criteria;
    }

    /**
     * @param array$criteria
     *
     * @return $this
     */
    public function setCriteria(array &$criteria): self
    {
        $this->criteria = &$criteria;

        return $this;
    }
}
