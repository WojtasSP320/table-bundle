<?php

namespace TableBundle\Events\TableEvent;

use TableBundle\Events\TableEventAbstract;

/**
 * Class PostSetCriteriaEvent
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class PostSetCriteriaEvent extends TableEventAbstract
{
    /** @var array */
    private $criteria;

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     *
     * @return $this
     */
    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }
}
