<?php

namespace TableBundle\Events;

use Symfony\Contracts\EventDispatcher\Event;
use TableBundle\Service\TableInterface;

/**
 * Class TableEventAbstract
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
abstract class TableEventAbstract extends Event
{
    /** @var TableInterface $table */
    private $table;

    /**
     * @return TableInterface
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param TableInterface $table
     *
     * @return $this
     */
    public function setTable(TableInterface $table): self
    {
        $this->table = $table;

        return $this;
    }
}
