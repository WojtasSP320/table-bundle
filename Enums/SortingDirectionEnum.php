<?php

declare(strict_types=1);

namespace TableBundle\Enums;

use Danhoss\SplEnum\SplEnum;

/**
 * Class SortingDirectionEnum
 *
 * @author Wojciech Uniejewski <wuniejewski@danhoss.com>
 */
class SortingDirectionEnum extends SplEnum
{
    /** @var string */
    public const ASC = 'asc';

    /** @var string */
    public const DESC = 'desc';
}
