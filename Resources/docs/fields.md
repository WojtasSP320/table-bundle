### Column fields

#### Overview

Each column consists of cells implementing `CellAbstract` class. By default, columns uses `SimpleField` cell.

Available built-in cells are:
- CheckField
- ColorField
- CustomField
- DateField
- DateTimeField
- DateTimeInterval
- FileSizeField
- FloatField
- JsonField
- NumberField
- PercentField
- PriceField
- ProductField
- ShopField
- SimpleField
- SimpleTransField
- TimeField
- UserAgentField
- UserField

#### Fields

##### CheckField
For displaying yes/no data. Renders as empty space or check mark glyph.
###### Options:
- `marked`: array  
   An array of values considered as "checked".  
   default: `['1', 1, true]`
- `theme`: string  
   Css class name to be used as theme.  
   default: `''`

##### ColorField
For displaying color value. Renders as circle representing color value.
###### Options:
- `format`: string  
   Color format.  
   default: `'hex'`
   
##### CustomField
todo: add description later

##### DateField
For displaying date value.
###### Options:
- `format`: string  
   Date print format.  
   default: `'Y-m-d'`
- `empty_value`: string  
   Value used if source data equals null.  
   default: `'---'`
   
##### DateTimeField
For displaying date and time value.
###### Options:
- `format`: string  
   Date time print format.  
   default: `'Y-m-d H:i'`
- `empty_value`: string  
   Value used if source data equals null.  
   default: `'---'`
   
##### DateTimeInterval
todo: add description later

##### FileSizeField
todo: add description later

##### FloatField
For displaying floating-point numbers.
todo: add description later

##### JsonField
For displaying json data. Renders as expandable/collapsable json viewer (similar to Symfony dump function).
###### Options:
- no options

##### NumberField
For displaying integer numbers.
###### Options:
- `thousands_separator`: string  
   Thousands separator character.   
   default: `' '`
- `empty_value`: string  
   Value used if source data equals null.  
   default: `'---'`
   
##### PercentField
For displaying percent values. Automaticaly turns floating-point numbers to percent value with "%" suffix.
###### Options:
- `decimal_digits`: int  
   Number of decimal digits to show.   
   default: `2`
- `format`: string  
   Number of decimal digits to show.
   allowed values: `fraction` `integer`   
   default: `'fraction'`
- `empty_value`: string  
   Value used if source data equals null.  
   default: `'---'`
   
##### PriceField
For displaying currency value. Supports number formatting and currency symbol.
###### Options:
- `currency`: string|null
   default: `'PLN'` 
- `decimal_digits`: int  
   Number of decimal digits to show.   
   default: `2`
- `decimals_point`: string  
   Decimal point character.   
   default: `','`
- `thousands_separator`: string  
   Thousands separator character.   
   default: `' '`
- `empty_value`: string  
   Value used if source data equals null.  
   default: `'---'`
   
##### ProductField (Danline context) 
For displaying `Product` entity.
todo: add description later

##### ShopField (Danline context)
For displaying shop value. Renders as shop name and icon depending on value.
todo: add description later

##### SimpleField
General use cell field. Renders as text.
###### Options
- `raw_output`: bool  
   Whether to render given data as raw html or strip html tags.  
   default: `false`
- `empty_value`: string  
   Value used if source data equals null  
   default: `'---'`
   
##### SimpleTransField
For displaying translated values. Applies `Translator::trans()` before rendering.
todo: add description later
   
##### TimeField
For displaying time value.
todo: add description later

##### UserAgentField
For displaying user agent string
todo: add description later

##### UserField
For displaying User entity. Renders as user avatar and full name. Also small indicator is rendered if user us currently at work.
If user has Skype number defined, skype call me icon with link is also rendered.

###### Options
- `empty_value`: string  
   Value used if source data equals null  
   default: `'---'`
   
   
#### Creating cell in table builder

Cell can be defined for a column using `createCell` method from `Column` class. First argument is cell (aka field) class and second (optional) argument is array of options.

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;
use TableBundle\Service\Cell\NumberField;
use TableBundle\Service\Cell\SimpleField;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('id')
            ->createCell(NumberField::class, ['empty_value' => 'empty'])
            ->setField()
                ->addMethodCall('getId')
        ;

        $this
            ->addColumn('firstName')
            ->createCell(SimpleField::class)
            ->setField()
                ->addMethodCall('getFirstName')
        ;

        $this
            ->addColumn('lastName')
            ->createCell(SimpleField::class, ['empty_value' => '---'])
            ->setField()
                ->addMethodCall('getLastName')
        ;

        return $this;
    }

    //...
}
```