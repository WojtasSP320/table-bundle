### Column filters

#### Overview

Each column can have defined filter for allowing user to filter out result data (even must have at least defined `NoneFilter`).

There are some built-in filters:
- DateRangeFilter
- NoneFilter (default if column shouldn't have any filter)
- NumberRangeFilter
- SelectFilter
- TextFilter
- TriboxFilter

Filter for a column can be defined using `createFilter` method from `Column` class. First argument is filter class,
second is filter/column name, and third (optional) is array of filter options.

Example:

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\Filter\DateRangeFilter;use TableBundle\Service\Filter\TextFilter;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;
use TableBundle\Service\Filter\NoneFilter;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('id')
            ->createFilter(NoneFilter::class, 'id')
            ->setField()
                ->addMethodCall('getId')
        ;

        $this
            ->addColumn('firstName')
            ->createFilter(TextFilter::class, 'firstName', ['placeholder' => 'Wpisz wartość'])
            ->setField()
                ->addMethodCall('getFirstName')
        ;

        $this
            ->addColumn('lastLoginDate')
            ->createFilter(DateRangeFilter::class, 'lastLoginDate')
            ->setField()
                ->addMethodCall('getLastName')
        ;

        return $this;
    }

    //...
}

```