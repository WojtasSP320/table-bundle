### The Table

#### Basic table definition

In order to create concrete table you need to create class extending `TableAbstract` and implementing `buildTable` and `getName` methods:

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class ExampleList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        /* columns definitions goes here... */        

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'app_example_list';
    }
}
```

#### Sorting

When any column uses sorting you must define sorting criteria in `getOrderCriteriaMap` method:

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('id')
            ->setField()
                ->addMethodCall('getId')
        ;

        $this
            ->addColumn('firstName')
            ->setField()
                ->addMethodCall('getFirstName')
        ;

        $this
            ->addColumn('lastName')
            ->setField()
                ->addMethodCall('getLastName')
        ;

        return $this;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'user.id',
            'firstName' => 'user.first_name',
            'lastName' => 'user.last_name',
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'app_users_list';
    }
}
```

where keys of `getOrderCriteriaMap` corresponds with table column names and values corresponds with data source column names (usually with database fields or entity properties when fetching data from a repository).  

#### Filtering

In order to use filtering on table columns Table class must implement getCriteriaMap() method:

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\Filter\DateRangeFilter;
use TableBundle\Service\Filter\NumberFilter;
use TableBundle\Service\Filter\TextFilter;
use TableBundle\Service\TableAbstract;
use TableBundle\Service\Model\Criterion;
use TableBundle\Exception\TableException;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('id')
            ->createFilter(NumberFilter::class, 'id')
            ->setField()
                ->addMethodCall('getId')
        ;

        $this
            ->addColumn('firstName')
            ->createFilter(TextFilter::class, 'firstName')
            ->setField()
                ->addMethodCall('getFirstName')
        ;

        $this
            ->addColumn('lastName')
            ->createFilter(TextFilter::class. 'lastName')
            ->setField()
                ->addMethodCall('getLastName')
        ;

        $this
            ->addColumn('lastLoginDate')
            ->createFilter(DateRangeFilter::class, 'lastLoginDate')
            ->setField()
                ->addMethodCall('getLastLogin')
                ->addMethodCall('format', 'Y-m-d H:i:s')
        ;

        return $this;
    }

    /**
     * @return array
     */
    public function getCriteriaMap(): array
    {
        return [
            'id' => new Criterion('user.id', Criterion::OPERATOR_EQUAL),
            'firstName' => new Criterion('user.first_name', Criterion::OPERATOR_LIKE),
            'lastName' => new Criterion('user.last_name', Criterion::OPERATOR_LIKE),
            'lastLoginDate' => [
                'from' => new Criterion('user.lastLogin', Criterion::OPERATOR_GREATER_THAN_OR_EQUAL),
                'to' => new Criterion('user.lastLogin', Criterion::OPERATOR_LESS_THAN_OR_EQUAL),
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'app_users_list';
    }
}
```

`getriteriaMap` method must return array of `Criteria` objects indexed by keys corresponding to column names.

#### Basic usage

When basic table is created you can use it:

```php
<?php

declare(strict_types=1);

namespace App\Controller;

use App\Table\UsersList;
use DanlineBundle\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use TableBundle\Service\TableBuilder\TableBuilder;

class UsersTableAction
{
    /** @var UserRepository */
    private $userRepository;

    /** @var TableBuilder */
    private $tableBuilder;

    /**
     * @param UserRepository $userRepository
     * @param TableBuilder $tableBuilder
     */
    public function __construct(
        UserRepository $userRepository,
        TableBuilder $tableBuilder
    ) {
        $this->userRepository = $userRepository;
        $this->tableBuilder = $tableBuilder;
    }

    /**
     * Action Controller
     * 
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $table = $this->tableBuilder->buildTable(new UsersList());

        $table->handlePostData();

        $results = $this->userRepository->getPaginatedList(
            $table->getListOrder(),
            $table->getListCriteria(),
            $table->getListLimit(),
            $table->getListOffset()
        );

        return $table->createListResponse($results);
    }
}
```

- First you need to build table instance using `TableBuilder->buildTable(new UsersList())`.
- Next step is to call handlePostData method on table instance to fetch table settings (orting, filtering, pagintaion, etc) from current request and saved in data base.
- Then you need to prepare some date to hydrate table instance with. This can be done using repository (usually, but any other data source implementing `countable` and `iterable` will be good).
  Each item in array represents one row in table. Row can be represented as array or object.
- Finally you have to generate table output for frontend using `createListResponse` method.   

#### Frontend

Table frontend is written in Vue framework, in order to render table on page just use table component:

```vue

<template>
    <vue-table data-source-url="http://localhost/example/users-list" />
</template>

<script>
    import VueTable from "TableBundle/Resources/public/js/vue/Table";

    export default {
        name: "UsersListPage",
        components: { VueTable },
    }
</script>
```

where data-source-url points to your `UsersTableAction` action controller.