### Columns

#### Overview

Each table is built using `Column` objects representing columns. Columns are added to table using `addColumn` method that takes string argument column name and optionally `Column` object if you want to add previously created column instance.

```php
<?php

namespace App\Table;

use ReflectionException;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('id')
            ->setField()
                ->addMethodCall('getId')
        ;

        $this
            ->addColumn('firstName')
            ->setField()
                ->addMethodCall('getFirstName')
        ;

        $this
            ->addColumn('lastName')
            ->setField()
                ->addMethodCall('getLastName')
        ;

        return $this;
    }

    //...
}
```

#### Column properties

Column object has several properties that can be used:

```php
use TableBundle\Service\Column;
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class UsersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $this
            ->addColumn('name')
            ->setLabel('Column name')
            ->setAlign(Column::ALIGN_CENTER)
            ->setWidth(Column::COLUMN_WIDTH_200)
            ->setVisible(true)
            ->setHidden(false)
            ->setClass('css-class')        
        ;    
        
        return $this;
    }

    //...
}
```
- `setColumnName()` - sets column name. Column name is used to refer column in table context.
- `setLabel()` - sets column label. Label is visible for user as column header.
- `setAlign()` - sets column alignment (left|right|center|justified)
- `setWidth()` - sets column width in pixels
- `setVisible()` - depending on this property column can be visible or invisible in frontend
- `setHidden()` - hidden columns are omitted in generated output (todo: precise definition later) 
- `setClass()` - sets css class that can be used in frontend

#### Fetching data from data source

When table is being hydrated with source data each column generates its rows data. To tell column which object properties or array values from data source should it use you have to call `setField` method and then method chain as if you would use data source item: 

```php
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class OrdersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $surnameFirst = true;

        $this
            ->addColumn('firstOrderItemCreatedBy')
            ->setField()
                ->addMethodCall('getOrderItems')
                ->addArrayAccess(0)
                ->addMethodCall('getProducedBy')
                ->addMethodCall('getUserFullName', $surnameFirst)
        ;
    
        $this
            ->addColumn('mainThumbnailCreatedAt')
            ->setField()
                ->addMethodCall('getProductsThumbnails')
                ->addArrayAccess('main_thumbnail')
                ->addMethodCall('getCreatedAt')
                ->addMethodCall('format', 'Y-m-d H:i:s')
        ;  

        return $this;
    }

    //...
}
```

`addMethodCall('methodName')` implies usage of `methodName` on data source object  
`addArrayAccess('index')` implies getting element at `index` offset from source array

So the example above is equivalent to calling  
``Order->getOrderItems()[0]->getProducedBy()->getUserFullName($surnameFirst)`` for `firstOrderItemCreatedBy` column, and  
``Order->getProductsThumbnails()['main_thumbnail']->getCreatedAt()->format('Y-m-d H:i:s'')`` for `mainThumbnailCreatedAt`

#### Retrieving data from complex method chain

If retrieving data from source via `setField` and method chain is impossible or difficult you can use `setClosureField` method. `setClosureField` takes one parameter - callback function to execute on each row. 
Callback function takes one argument - data item from source and must return some value. 

Example:

```php
use TableBundle\Service\TableAbstract;
use TableBundle\Exception\TableException;

class OrdersList extends TableAbstract
{
    /**
     * @throws ReflectionException
     * @throws TableException
     *
     * @return $this
     */
    public function buildTable(): self
    {
        $noDateFallback = 'N/A';

        $this
            ->addColumn('purchasedAt')
            ->setClosureField(static function(Order $order) use ($noDateFallback) {
                return $order->getPurchasedAt() !== null
                    ? $order->getPurchasedAt()->format('Y-m-d H:i:s')
                    : $noDateFallback
                ; 
            })
        ;
    
        $this
            ->addColumn('minimalAmountProduct')
            ->setClosureField(static function(Order $order) {
                $orderItems = $order->getOrderItems();
                
                $minimalAmount = min(array_map($orderItems, function($item) {return $item->getAmount();}));
                
                return 'Minimal amount is: ' . $minimalAmount;
            })
        ;  

        return $this;
    }

    //...
}
```