const Encore = require('@symfony/webpack-encore');
const webpackUtils = require('webpack.utils.js');

const assetsBundle = webpackUtils.assetsBundle;

Encore
    .addEntry('vue-table', [
        assetsBundle.TableBundle + 'js/main.js',
    ])
;