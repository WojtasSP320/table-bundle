import Vue from 'MainBundle/Resources/public/js/vue/vue-core';
import Table from './vue/Table';

const app = new Vue({
    render(createElement) {
        return createElement(Table);
    }
});

app.$mount('#vue-table');