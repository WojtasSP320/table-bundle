import HttpClient from "MainBundle/Resources/public/js/services/http/HttpClient";

export default class TableRequestClient extends HttpClient {
    /**
     * @param {string} url
     * @return {Promise<void>}
     */
    async fetchList(url) {
        const { body } = await this.request.get(url);

        return body;
    }

    /**
     * @param {string} url
     * @param {object} filterData
     * @return {Promise<void>}
     */
    async fetchFilterList(url, filterData) {
        const { body } = await this.request.post(url, filterData);

        return body;
    }

    /**
     * @param {object} filterData
     * @return {Promise<void>}
     */
    async saveTableSettings(filterData) {
        const url = Routing.generate('table_save_table_settings_async');

        await this.request.post(url, filterData);
    }

    /**
     * @param {string} tableName
     * @param {object} columns
     * @return {Promise<void>}
     */
    async saveColumnsLayout(tableName, columns) {
        const postData = { tableName, columns };

        const url = Routing.generate('table_save_columns_layout_async');

        await this.request.post(url, postData);
    }
}
