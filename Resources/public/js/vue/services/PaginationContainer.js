export default class PaginationContainer {
    /**
     * @param {TableStore} tableStore
     */
    constructor(tableStore) {
        this.store = tableStore;
    }

    /**
     * @return {state.pagination|{limit, possibleLimits, currentPage, maxPage}|getters.pagination|*|pagination|default.computed.pagination}
     */
    get state() {
        return this.store.state.pagination;
    }

    /**
     * @param {int} limit
     */
    changeLimit(limit) {
        this.store.commit('changeLimit', {limit})
    }

    /**
     * @param {int} pageNumber
     */
    changePage(pageNumber) {
        this.store.commit('changePage', {pageNumber});
    }
}