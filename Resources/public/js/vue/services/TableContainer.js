import TableStore from "./TableStore";
import TableRequestClient from "./TableRequestClient";
import PaginationContainer from "./PaginationContainer";

export default class TableContainer {
    constructor() {
        this.store = new TableStore();
        this.tableRequest = new TableRequestClient();

        this.paginationContainer = new PaginationContainer(this.store);

        this.isLoaderDisabled = false;
    }

    /**
     * @param {string} dataSourceUrl
     */
    async initialize(dataSourceUrl) {
        this.store.state.loading = true;

        this.store.state.dataSourceUrl = dataSourceUrl;

        const results = await this.tableRequest.fetchList(dataSourceUrl);
        this.store.commit('table', results);

        this.store.state.loading = false;
    }

    /**
     * @return {Promise<void>}
     */
    async reloadList() {
        this.showLoader();

        const filterData = this.createFilterData();
        const dataSourceUrl = this.store.state.dataSourceUrl;

        const results = await this.tableRequest.fetchFilterList(dataSourceUrl, filterData);
        this.store.commit('table', results);

        this.tableRequest.saveTableSettings(filterData);

        this.hideLoader();
    }

    /**
     * @returns {Array}
     */
    getCheckedRows() {
        const checkedIds = this.store.state.checkedRowsIds
        return this.store.state.rows.filter((row) => {
            return checkedIds.includes(row.__rowId);
        });
    }

    setCheckedRows(checkedRowsIds) {
        const checkedIds = this.store.state.checkedRowsIds
        return this.store.state.rows.filter((row) => {
            return checkedIds.includes(row.__rowId);
        });
    }



    clearCheckedRows() {
        this.store.commit('checkboxesUncheckAll');
    }

    showLoader() {
        if (this.isLoaderDisabled) {
            return;
        }
        this.store.state.reloading = true;
    }

    hideLoader() {
        this.store.state.reloading = false;
    }

    async reloadListInBackground() {
        this.disableLoader();
        await this.reloadList();
        this.enableLoader();
    }

    /**
     * @return {TableContainer}
     */
    disableLoader() {
        this.isLoaderDisabled = true;

        return this;
    }

    /**
     * @return {TableContainer}
     */
    enableLoader() {
        this.isLoaderDisabled = false;

        return this;
    }

    clearFilters() {
        this.store.commit('clearFilters');
    }

    checkRow(rowId) {
        this.store.commit('checkRow', rowId);
    }

    uncheckRow(rowId) {
        this.store.commit('uncheckRow', rowId);
    }

    checkboxesCheckAll() {
        this.store.commit('checkboxesCheckAll');
    }

    /**
     * @return {{pagination: {limit: null, page: null}, sorting: (*|state.sorting|{}|getters.sorting|default.computed.sorting|sorting), filters, table_name: string | null}}
     */
    createFilterData() {
        const filters = {};
        const state = this.store.state;

        Object.keys(state.filters).forEach((filterName) => {
            filters[filterName] = state.filters[filterName].value;
        });

        return {
            table_name: state.table.name,
            sorting: state.sorting,
            pagination: {
                page: state.pagination.currentPage,
                limit: state.pagination.limit,
            },
            filters: filters,
        };
    }
}