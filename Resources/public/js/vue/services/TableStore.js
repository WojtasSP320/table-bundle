import Vue from 'vue';

/**
 * @return {Vue}
 */
function createState() {
    return new Vue({
        data() {
            return {
                loading: false,
                reloading: false,
                dataSourceUrl: null,
                columns: null,
                rows: null,
                actions: null,
                filters: null,
                checkboxes: [],
                sorting: {},
                pagination: {
                    currentPage: null,
                    limit: null,
                    maxPage: null,
                    possibleLimits: [],
                },
                table: {
                    name: null,
                    showHeader: null,
                    showLabels: null,
                    showFilters: null,
                    showSorting: null,
                    showCheckboxes: null,
                    showActions: null,
                    paginationEnabled: null,
                    sortingEnabled: null,
                    showPagination: null,
                }
            };
        },
        computed: {
            checkedRowsIds() {
                return this.checkboxes
                    .map((value, index) => {
                        return value.checked
                            ? this.rows[index].__rowId
                            : null
                        ;
                    }).filter((value) => {
                        return value !== null;
                    })
                ;
            }
        },
    })
}

/**
 * @param {Vue} state
 * @return {Object}
 */
function createMutations(state) {
    return {
        table: (table) => {
            state.columns = table.columns;
            state.rows = table.rows;
            state.pagination = table.pagination;
            state.sorting = {...table.sorting};
            state.actions = table.actions;
            state.table = table.table;
            state.filters = table.filters;
            state.checkboxes = table.checkboxes;

            let layout = {};
            Object.keys(table.columns).forEach(function (columnName) {
                layout[columnName] = table.columns[columnName].visible;
            });
            state.layout = layout;
        },
        changePage: ({pageNumber}) => {
            state.pagination.currentPage = pageNumber;
        },
        changeLimit: ({limit}) => {
            state.pagination.limit = limit;
        },
        changeColumnVisibility: ({columnName, isVisible}) => {
            state.columns[columnName].visible = isVisible;
        },
        changeFilterValue: ({filterName, value, viewValue}) => {
            state.filters[filterName].value = value;
            if (viewValue) {
                state.filters[filterName].viewValue = viewValue;
            }
        },
        changeSortingDirection: ({column, direction}) => {
            state.sorting[column] = direction;

            // state.sorting = {column, direction};
            state.columns[column].isCurrentlySorted = true;
            state.columns[column].sortingDirection = direction;
        },
        removeSorting: ({column}) => {
            delete state.sorting[column];

            let sorting = state.sorting;

            state.sorting = {...sorting};

            state.columns[column].isCurrentlySorted = false;
            state.columns[column].sortingDirection = null;
        },
        moveColumnLeft: ({columnName}) => {
            let columns = Object.keys(state.columns);

            let index = columns.indexOf(columnName);
            let predecessorColumnName = columns[index-1];

            if (index > 0) {
                columns.splice(index-1, 2, columnName, predecessorColumnName);
            }

            const newLayout = {};

            columns.forEach(function (columnName) {
                newLayout[columnName] = state.columns[columnName];
            });

            state.columns = newLayout;
        },
        moveColumnRight: ({columnName}) => {
            let columns = Object.keys(state.columns);

            let index = columns.indexOf(columnName);
            let successorColumnName = columns[index+1];

            if (index < columns.length - 1) {
                columns.splice(index, 2, successorColumnName, columnName);
            }

            const newLayout = {};

            columns.forEach(function (columnName) {
                newLayout[columnName] = state.columns[columnName];
            });

            state.columns = newLayout;
        },
        hideAction: ({actionName, index}) => {
            state.actions[index][actionName].visible = false;
        },

        clearFilters: () => {
            for (const filter in state.filters) {
                let filterValue = state.filters[filter].componentAttributes.default_value;
                filterValue = filterValue === "" ? null : filterValue;

                state.filters[filter].value = filterValue;
            }

            state.pagination.currentPage = 1;
        },

        changeCheckbox: ({value, isCheckedNow}) => {
            Vue.set(state.checkboxes[value], 'checked', isCheckedNow);
        },

        checkRow: (rowId) => {
            const index = state.rows.findIndex(row => row.__rowId === rowId);

            if (-1 !== index && state.checkboxes[index].active === true) {
                Vue.set(state.checkboxes[index], 'checked', true);
            }
        },

        uncheckRow: (rowId) => {
            const index = state.rows.findIndex(row => row.__rowId === rowId);

            if (-1 !== index && state.checkboxes[index].active === true) {
                Vue.set(state.checkboxes[index], 'checked', false);
            }
        },

        checkboxesCheckAll: () => {
            state.checkboxes = state.checkboxes.map((value, index) => {
                if (value.active) {
                    value.checked = true;
                }

                return value;
            });
        },

        checkboxesUncheckAll: () => {
            state.checkboxes = state.checkboxes.map((value, index) => {
                if (value.active) {
                    value.checked = false;
                }

                return value;
            });
        },
    }
}

export default class TableStore {
    constructor() {
        this.state = createState();
        this.mutations = createMutations(this.state);
    }

    /**
     * @param {string} mutationType
     * @param {*} payload
     */
    commit(mutationType, payload = null) {
        const mutation = this.mutations[mutationType];

        mutation(payload);
    }
}
