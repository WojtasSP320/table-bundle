let ConfirmDeleteModal = {
    /** @param {string} */
    confirmPath: null,

    registerEvents: function() {
        $('body').on('click', '#confirm-delete-modal #confirm-delete', function() {
            window.location.href = ConfirmDeleteModal.getConfirmPath();
        });
    },

    /**
     * @param {string} confirmPath
     */
    setConfirmPath: function(confirmPath) {
        ConfirmDeleteModal.confirmPath = confirmPath;
    },

    /**
     * @return {null|string}
     */
    getConfirmPath: function () {
        return ConfirmDeleteModal.confirmPath;
    },

    render: function() {
        let template = `
            <div id="confirm-delete-modal" class="modal fade">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Usuwanie</h4>
                        </div>
                        <div class="modal-body">
                            <p>Czy na pewno usunąć wpis? Operacja może być nieodwracalna.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                            <button id="confirm-delete" type="button" class="btn btn-danger" data-dismiss="modal">Usuń</button>
                        </div>
                    </div>
                </div>
            </div>
        `;

        $('#confirm-delete-modal').remove();
        $('body').append(template);
        $('#confirm-delete-modal').modal({show: true})
    }
};

ConfirmDeleteModal.registerEvents();