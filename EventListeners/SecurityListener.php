<?php

namespace TableBundle\EventListeners;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use TableBundle\Entity\TableSetting;
use TableBundle\Service\TableInterface;
use TableBundle\Service\TableSettingsManager;

class SecurityListener
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var TableSettingsManager
     */
    private $tableSettingsManager;

    /**
     * SecurityListener constructor.
     *
     * @param Session $session
     * @param TableSettingsManager $tableSettingsManager
     */
    public function __construct(Session $session, TableSettingsManager $tableSettingsManager)
    {
        $this->session = $session;
        $this->tableSettingsManager = $tableSettingsManager;
    }

    /**
     * @param InteractiveLoginEvent $event
     *
     * @throws \Exception
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $session = $this->session;

        $tableSettings = $session->get(TableInterface::TABLE_SETTINGS_SESSION_KEY, null);

        if (null === $tableSettings) {
            $tableSettings = $this->flattenTableSettings($this->tableSettingsManager->getCurrentUserSettings());
            $session->set(TableInterface::TABLE_SETTINGS_SESSION_KEY, $tableSettings);
        }
    }

    /**
     * @param TableSetting[] $tableSettings
     *
     * @return array
     */
    protected function flattenTableSettings(array $tableSettings): array
    {
        $settings = [];

        foreach ($tableSettings as $tableSetting) {
            $sorting = $tableSetting->getSorting();

            $setting = [
                'page' => $tableSetting->getPage(),
                'limit' => $tableSetting->getLimit(),
                'column' => $sorting['column'] ?? null,
                'direction' => $sorting['direction'] ?? null,
                'columns' => $tableSetting->getColumns(),
            ];

            $settings[$tableSetting->getName()] = $setting;
        }

        return $settings;
    }
}
